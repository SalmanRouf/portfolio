import csv

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = itemNumber
        self.itemDescription = itemDescription
        self.itemValue = itemValue


class Warehouse:
    def __init__(self, name):
        self.items = []

        
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)
           # print(item)

        
def readingCSV(file, name):
    with open(file, encoding = 'utf-8', errors = 'ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse
    

#Asking the use to enter the item number
itemNumber = int(input('Please enter the item number: ')) 

#Getting the item descrpition
itemDescription = str(input('Please enter the item description: '))

#Item value entered & checked the value to make sure it doesn't exceed the limit.
itemValue = int(input('Please enter the item value:£ '))

while itemValue > 2000000000:
    print('The item value has been rejected as the value has exceeded the limit of £2 billion')
    itemValue = int(input('Please enter the item value:£ '))
    
#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')

#Asking the user to pick a warehouse to place the item within
decision =("Select a warehouse to place the item within A, B, C or D")
print(decision)
while True:
    choice=input("Please Select: ")
    if choice=='A':
        for item in A.items:
            print(item.itemNumber,item.itemDescription,item.itemValue)
    elif choice == 'B':
        for item in B.items:
            print(item.itemNumber,item.itemDescription,item.itemValue)
    elif choice =='C':
        for item in C.items:
            print(item.itemNumber,item.itemDescription,item.itemValue)
    elif choice =='D':
        for item in D.items:
            print(item.itemNumber,item.itemDescription,item.itemValue)
    else:
        print("error: Unknown Variable Selected")
        print(decision)



