//setting up test environment
process.env.NODE_ENV = "test";
var test = require('unit.js');
//imports jsdom
var jsdom = require('jsdom');
var assert = require('assert');
//creates a constructor
var { JSDOM } = jsdom;
// importing server code for testing
var server = require('../server');

// test suite for options
describe("options suite", function () {

    it('T1', function () {
        //test the options value is an object and has following properties 
        test.object(server.options)
            .hasProperty("host", "bristol.api.urbanthings.io")
            .hasProperty("headers")
            .hasProperty("accept", "application/json");

    });



    it('T2', function () {
        var p = "/api/2.0/rti/resources/status?stopIDs=UK_BRIS_EV:ccae1e20674a45329a83a304a52f873d"
        var q = server.path(p);
        test.object(q)
            .hasProperty("host", "bristol.api.urbanthings.io")
            .hasProperty("headers")
            .hasProperty("accept", "application/json")
            .hasProperty("path", p);
    });
});