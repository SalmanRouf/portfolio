/*
This document provides the scores for a game. 
 */
package tt_association;

/**
 *
 *
 * @author CP2/02 Team 3
 */
public class Game { // start of class Game

    private int homeScore, awayScore;

    public Game() {
    }

    public void setScores(int homeScore, int awayScore) {
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    public int getGameHomeScore() {
        return homeScore;
    }

    public int getGameAwayScore() {
        return awayScore;
    }

    public String getGameScore() {
        String score = homeScore + ":" + awayScore;
        return score;
    }

    @Override
    public String toString() {
        String score = homeScore + ":" + awayScore;
        return score;
    }

} //end of class Game
