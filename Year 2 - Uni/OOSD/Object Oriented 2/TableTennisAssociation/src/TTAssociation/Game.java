/*
This document provides the scores for a game and can return it 
with the toString method. 
*/
package TTAssociation;

public class Game {

	private int homeScore, awayScore;
        
	public Game() {
	}
        
        public void setScores (int homeScore, int awayScore) {
            this.homeScore = homeScore;
            this.awayScore = awayScore;
        }
        
        public int getGameHomeScore () {
            return homeScore;
        }
                
        public int getGameAwayScore () {
            return awayScore;
        }
        
        public String getGameScore() {
            String score = homeScore + ":" + awayScore; 
            return score;
        }
        
        @Override
        public String toString() {
            String score = homeScore + ":" + awayScore; 
            return score;
        }

}
