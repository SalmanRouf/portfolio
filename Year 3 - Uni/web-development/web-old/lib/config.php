<?php
@date_default_timezone_set('Europe/London'); 

define('RATES_URL', 'http://data.fixer.io/api/latest?access_key=52bfa01ac9a87b47781c86fc17aeaf43');
define('PARAM', array('from', 'to', 'amnt', 'format'));
define('FORMAT', array('xml', 'json'));
define('PARAM2', array('cur', 'action'));
define('ACTION', array('post', 'put', 'del'));
define('VERSION', '1.0');
define('ENCODING', 'UTF-8');
define('ISO', 'http://www.currency-iso.org/dam/downloads/lists/list_one.xml');

#storing the different file path to the currencies file
define('CURRENCIES', 'data/currencies.xml');  
define('CURRENCIES1', '../data/currencies.xml');  
define('CURRENCIES2', '../../data/currencies.xml');  

#storing the error numbers with the messages
define('ERROR_MESSAGE', array(
    1000 => 'Required parameter is missing',
    1100 => 'Parameter not recognized',
    1200 => 'Currency type not recognized',
    1300 => 'Currency amount must be a decimal number',
    1400 => 'Format must be xml or json',
    1500 => 'Error in service',
    2000 => 'Action not recognized or is missing',
    2100 => 'Currency code in wrong format or is missing',
    2200 => 'Currency code not found for update',
    2300 => 'No rate listed for this currency',
    2400 => 'Cannot update base currency',
    2500 => 'Error in service'
));
?>