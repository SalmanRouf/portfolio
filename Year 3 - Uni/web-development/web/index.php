<?php
#requiring the config file and function file to be included else the file will not run
require('lib/config.php');
require_once('lib/functions.php');
#running the live function from the function file
live($up=0);

#setting the format to default of xml, if there no format or if the format is empty
if (!isset($_GET['format']) || empty($_GET['format'])) {
	$_GET['format'] = 'xml';
}
#checking if core data files are missing 
if (!file_exists(CURRENCIES)) {
	update();
}
#getting every $_GET params into PHP variables
extract($_GET);
#creating variables from the params within the config file and pulling data from the url
$get = array_intersect(PARAM, array_keys($_GET));
#checking if we have more than 4 params, if we do an error message is printed
if (count($get) < 4) {
    error_validation(1000, $format);
	exit;
}
#checking if we have less than the 4 params required, if we do an error message is printed
if (count($_GET) > 4) {
	error_validation(1100, $format);
	exit;
}

#making the entered from and to codes into uppercases
$from = strtoupper($from);
$to = strtoupper($to);
#making the entered format into lowercases 
$format = strtolower($format);

#checking that $to and $from are recognized currencies from the currency codes that we are holding within the currencies file, if not an error message is returned
if (!in_array($to, LIVE) || !in_array($from, LIVE)) {
	error_validation(1200, $format);
	exit;
}
#checking for allowed format values using the format declared within the config file, if not an error message is returned
if (!in_array($format, FORMAT)) {
	error_validation(1400, $format);
	exit;
}
#checking if $amnt is a two decimal place value, if not an error message is returned
if (!preg_match('/^[+-]?(\d*\.\d{2,2})$/', $amnt)) {
	error_validation(1300, $format);
	exit;
}
#getting the timestamp from the currencies file and adding two hours onto the time
$hours = date('j F Y H:i',strtotime('+2 hours',strtotime(DATE)));
#getting the current time
$current_time = date("j F Y H:i");
#checking if the current time is greater than the timestamp with 2 hours added, if so the currencies file is updated with the update function within the function file
if ($current_time > $hours){
    update();
} 
#using the print format function from the function file to print the data with the correct format, sending the variables of from, to, amnt and format to the function
print_format($from, $to, $amnt, $format);

// https://daveismyname.blog/quick-way-to-add-hours-and-minutes-with-php
//print_r($xml);
//$explode = explode(' ',trim($mydate));
//$value = ($explode[3]); 
//echo $xml->code->attributes()->AUD;
//https://alexwebdevelop.com/xml-from-scratch/
?>