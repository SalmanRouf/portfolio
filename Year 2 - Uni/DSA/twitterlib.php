<?php

return array("Liverpool" => array("SearchTerms" => array("Liverpool",
                                                         "#Liverpool",
                                                         "#Merseyside"),
                                "Coordinates" => "53.412095,-2.987820"),

            "Anfield" => array("SearchTerms" => array("#Anfield",
                                                      "Anfield",
                                                      "Liverpool%20FC",
                                                      "#LiverpoolFC",
                                                      "#LFC",
                                                      "LFC"),
                                "Coordinates" => "53.412095,-2.987820"),

            "World Museum" => array("SearchTerms" => array("#WorldMuseum",
                                                           "World%20Museum"),
                                "Coordinates" => "53.412095,-2.987820"),
                
            "Liverpool Cathedral" => array("SearchTerms" => array("#LiverpoolCathedral",
                                                                   "Liverpool%20Cathedral"),
                                "Coordinates" => "53.412095,-2.987820"),

            "Sefton Park" => array("SearchTerms" => array("#SeftonPark",
                                                          "Sefton%20Park"),
                                 "Coordinates" => "53.412095,-2.987820"),

            "Walker Art Gallery" => array("SearchTerms" => array("#WalkerArtGallery",
                                                                 "Walker%20Art%20Gallery"),
                                "Coordinates" => "53.412095,-2.987820"),

            "The Beatles Story" => array("SearchTerms" => array("#TheBeatlesStory",
                                                                 "The%20Beatles%20Story"),
                                "Coordinates" => "53.412095,-2.987820"),

            "University of Liverpool" => array("SearchTerms" => array("#UniversityOfLiverpool",
                                                                      "University%20of%20Liverpool",
                                                                      "UOL",
                                                                      "#UOL"),
                                "Coordinates" => "53.412095,-2.987820"),

            "Liverpool Empire Theatre" => array("SearchTerms" => array("#LiverpoolEmpireTheatre",
                                                                       "Liverpool%20Empire%20Theatre"),
                                "Coordinates" => "53.412095,-2.987820"),
             
                            
            "Cologne" => array("SearchTerms" => array("Cologne",
                                                      "#Cologne",
                                                      "#Koln",
                                                      "Koln"),
                                "Coordinates" => "50.939991,6.958390"),

            "Cologne Cathedral" => array("SearchTerms" => array("#CologneCathedral",
                                                                "Cologne%20Cathedral",
                                                                "KolnCathedral",
                                                                "#KolnCathedral"),
                                "Coordinates" => "50.939991,6.958390"),

            "The Wallraf-Richartz Museums" => array("SearchTerms" => array("#WallrafRichartzMuseums",
																		   "#Wallraf-RichartzMuseums",
                                                                           "Wallraf-Richartz%20Museums"),
                                "Coordinates" => "50.939991,6.958390"),

            "Cologne Zoological Gardens" => array("SearchTerms" => array("#CologneZoologicalGradens",
                                                                          "Cologne%20Zoological%20Gradens",
                                                                          "Cologne%20Zoo",
                                                                          "#CologneZoo",
                                                                          "Koln%20Zoo",
                                                                          "#KolnZoo"  ),
                                "Coordinates" => "50.939991,6.958390"),

            "Cologne Chocolate Museum" => array("SearchTerms" => array("#ChocolateMuseum",
                                                               "Chocolate%20Museum",
                                                               "#CologneChocolateMuseum",
                                                               "Cologne%20Chocolate%20Museum",
                                                               "#KolnChocolateMuseum",
                                                               "Koln%20Chocolate%20Museum"),
                                "Coordinates" => "50.939991,6.958390"),

            "Flora and Botanical Garden" => array("SearchTerms" => array("#FloraandBotanicalGarden",
                                                              "Flora%20and%20Botanical%20Garden"),
                                "Coordinates" => "50.939991,6.958390"),

            "The Roman-Germanic Museum" => array("SearchTerms" => array("#RomanGermanicMuseum",
																		"#Roman-GermanicMuseum",
                                                                         "Roman-Germanic%20Museum"),
                                "Coordinates" => "50.939991,6.958390"),
                            
            "Augustusburg and Falkenlust Palace" => array("SearchTerms" => array("#AugustusburgandFalkenlustPalace",
                                                                                "Augustusburg%20and%20Falkenlust%20Palace"),
                                "Coordinates" => "50.939991,6.958390"),

            "Müngersdorfer Stadion" => array("SearchTerms" => array("#MungersdorferStadion",
																	"#M%FCngersdorferStadion",
                                                                    "M%FCngersdorfer%20Stadion",
                                                                    "Munngersdorfer%20Stadion",
                                                                    "#MuengersdorferStadion",
                                                                    "Muengersdorfer%20Stadion"),
                                "Coordinates" => "50.939991,6.958390"),




            



);

?>