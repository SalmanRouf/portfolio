import csv 
import sys #allows me to exit the program without having to ask the user twice

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = int (itemNumber)
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)


class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    #add items function
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)

    #appending the items i'm adding into one of the warehouses
    def addSomething(self, item):
        self.items.append(item)
        
    #works out the total value of one warehouse with the items we might be adding.
    def totalValue(self):
        totalValue = 0
        for i in self.items:
            totalValue = totalValue + i.itemValue
        return totalValue

    #remove function
    def removeItem(self, item):
        self.items.remove(item)


class Function:
    def addFunction(functions):
        # Asking the user to enter the item number
        itemNumber = int(input("Please Enter The Item Number\n"))

        # Getting the item descrpition
        itemDescription = str(input("Please Enter The Item Description\n"))

        # Item value entered & checked the value to make sure it doesn't exceed the limit.
        itemValue = int(input("Please Enter The Item Value(£)\n"))
        # Performing the check
        while itemValue > capacity:
            print("Error: The Item Value Has Been Rejected As The Value Has Exceeded The Limit Of (£)", capacity)
            sys.exit() #directing exits the program
            break

        #Declaring variables within a new variable to callback all the variables in one callback 
        newItem = Items(itemNumber, itemDescription, itemValue)

        # Asking the user to pick a warehouse to place the item within
        while True: #Keeps user in a loop until they pick an option of A, B, C or D
            warehouseSelection = str(input("Please Select A Warehouse To Place The Item Within A, B, C or D\n"))
            if warehouseSelection == 'A' or warehouseSelection == 'a':
                A.addSomething(newItem)#Uses variable newItem to save the new information within warehouse A
                print("Item No., Description, Value in £")
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue) #Keeps looping until it has printed all the items within warehouse A
                print("The Total Value For Warehouse A is", A.totalValue()) #Works out the total amount of value within warehouse A
                if Warehouse.totalValue(A) > capacity: #Uses the total value function and the capacity variable to work out if the item being added would go over the limit of the £2 billion.
                    print("Error: Warehouse Limit is £2000000000\nError: The Item Cannot Be Added To The Warehouse\nError: The Item Would Make The Warehouse Exceed The Limit By (£)", Warehouse.totalValue(A) - capacity) #Works out the amount it has gone over by
                    Warehouse.removeItem(A, newItem)#Removes the item from A if it makes the warehouse go over the limit of £2 billion
                break
            elif warehouseSelection == 'B' or warehouseSelection == 'b':
                B.addSomething(newItem) #Uses variable newItem to save the new information within warehouse B
                print("Item No., Description, Value in £")
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse B
                print("The Total Value For Warehouse B is", B.totalValue())#Works out the total amount of value within warehouse B
                if Warehouse.totalValue(B) > capacity: #Uses the total value function and the capacity variable to work out if the item being added would go over the limit of the £2 billion.
                    print("Error: Warehouse Limit is £2000000000\nError: The Item Cannot Be Added To The Warehouse\nError: The Item Would Make The Warehouse Exceed The Limit By (£)", Warehouse.totalValue(B) - capacity)#Works out the amount it has gone over by
                    Warehouse.removeItem(B, newItem) #Removes the item from B if it makes the warehouse go over the limit of £2 billion
                break
            elif warehouseSelection == 'C' or warehouseSelection == 'c':
                C.addSomething(newItem)#Uses variable newItem to save the new information within warehouse C
                print("Item No., Description, Value in £")
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse C
                print("The Total Value For Warehouse C is", C.totalValue())#Works out the total amount of value within warehouse C
                if Warehouse.totalValue(C) > capacity:#Uses the total value function and the capacity variable to work out if the item being added would go over the limit of the £2 billion.
                    print("Error: Warehouse Limit is £2000000000\nError: The Item Cannot Be Added To The Warehouse\nError: The Item Would Make The Warehouse Exceed The Limit By (£)", Warehouse.totalValue(C) - capacity)#Works out the amount it has gone over by
                    Warehouse.removeItem(C, newItem)#Removes the item from C if it makes the warehouse go over the limit of £2 billion
                break
            elif warehouseSelection == 'D' or warehouseSelection == 'd':
                D.addSomething(newItem)
                print("Item No., Description, Value in £")
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse D
                print("The Total Value For Warehouse D is", D.totalValue())#Works out the total amount of value within warehouse D
                if Warehouse.totalValue(D) > capacity: #Uses the total value function and the capacity variable to work out if the item being added would go over the limit of the £2 billion.
                    print("Error: Warehouse Limit is £2000000000\nError: The Item Cannot Be Added To The Warehouse\nError: The Item Would Make The Warehouse Exceed The Limit By (£)", Warehouse.totalValue(D) - capacity)#Works out the amount it has gone over by
                    Warehouse.removeItem(D, newItem)#Removes the item from D if it makes the warehouse go over the limit of £2 billion
                break
            else:
                print("Error: Unknown Variable Selected") #Error message appear if the user doesn't select A, B, C or D


class Function2(Function):
    def viewSummaryFunction(functions):
        # Asking the user to pick a warehouse to place the item within
        while True:#Keeps user in a loop until they pick an option of A, B, C or D
            choice = input("Select To View Warehouse A, B, C or D\n")
            if choice == 'A' or choice == 'a':
                print("Item No., Description, Value in £")
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse A
                print("The Total Value For Warehouse A is", A.totalValue()) #Works out the total amount of value within warehouse A
                break
            elif choice == 'B' or choice == 'b':
                print("Item No., Description, Value in £")
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse B
                print("The Total Value For Warehouse B is", B.totalValue()) #Works out the total amount of value within warehouse B
                break
            elif choice == 'C' or choice == 'c':
                print("Item No., Description, Value in £")
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse C
                print("The Total Value For Warehouse C is", C.totalValue()) #Works out the total amount of value within warehouse C
                break
            elif choice == 'D' or choice == 'd':
                print("Item No., Description, Value in £")
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)#Keeps looping until it has printed all the items within warehouse D
                print("The Total Value For Warehouse D is", D.totalValue()) #Works out the total amount of value within warehouse D
                break
            else:
                print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D") #Error message appear if the user doesn't select A, B, C or D


def movingItems():
    itemNumber = None #Defining the item number within the move function
    movingOption = str(input("Please Enter The Warehouse You Want To Take An Item From [A,B,C or D]\n"))
    if movingOption == "A" or movingOption == "a":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))#redefining the item number
        for item in A.items:
            if itemNumber == item.itemNumber:#loops through to find the item that is going to be removed
                A.removeItem(item)#removes item by finding it within the warehouse
    elif movingOption == "B" or movingOption == "b":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))#redefining the item number
        for item in B.items:
            if itemNumber == item.itemNumber: #loops through to find the item that is going to be removed
                B.removeItem(item)#removes item by finding it within the warehouse
    elif movingOption == "C" or movingOption == "c":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))#redefining the item number
        for item in C.items:
            if  itemNumber == item.itemNumber:#loops through to find the item that is going to be removed
                C.removeItem(item)#removes item by finding it within the warehouse
    elif movingOption == "D" or movingOption == "d":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))#redefining the item number
        for item in D.items:
            if itemNumber == item.itemNumber:#loops through to find the item that is going to be removed
                D.removeItem(item)#removes item by finding it within the warehouse
    else:
        print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D") #Error message appear if the user doesn't select A, B, C or D
        sys.exit()

    itemDescription = str(input("Please Enter The Item Description For The Item You Are Moving\n"))#Getting item description for the item that going to be removed

    itemValue = int(input("Please Enter The Item Value For The Item You Are Moving (£)\n"))#Getting item value for the item that is going to be removed

    nextWarehouse = str(input("Please Enter The Warehouse That You Would Like To Move This Item To [A,B,C or D]\n"))

    newItem = Items(itemNumber, itemDescription, itemValue) #Declaring variables within a new variable to callback all the variables in one callback

    if nextWarehouse == 'A' or nextWarehouse == 'a':
        if(itemValue > capacity - Warehouse.totalValue(A)): #See if the item being moved is too big to fit within the warehouse and returns an error if so
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(A)): #See if the item being moved can fit within the warehouse, if so it will add the item to this warehouse
            print("The Item Has Been Added to Warehouse A")
            A.addSomething(newItem) #Adding the item to this warehouse 
    elif nextWarehouse == 'B' or nextWarehouse == 'b':
        if (itemValue > capacity - Warehouse.totalValue(B)): #See if the item being moved is too big to fit within the warehouse and returns an error if so
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(B)): #See if the item being moved can fit within the warehouse, if so it will add the item to this warehouse
            print("The Item Has Been Added to Warehouse B")
            B.addSomething(newItem) #Adding the item to this warehouse 
    elif nextWarehouse == 'C' or nextWarehouse == 'c':
        if (itemValue > capacity - Warehouse.totalValue(C)): #See if the item being moved is too big to fit within the warehouse and returns an error if so
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(C)): #See if the item being moved can fit within the warehouse, if so it will add the item to this warehouse
            print("The Item Has Been Added to Warehouse C")
            C.addSomething(newItem) #Adding the item to this warehouse 
    elif nextWarehouse == 'D' or nextWarehouse == 'd':
        if (itemValue > capacity - Warehouse.totalValue(D)): #See if the item being moved is too big to fit within the warehouse and returns an error if so
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(D)): #See if the item being moved can fit within the warehouse, if so it will add the item to this warehouse
            print("The Item Has Been Added to Warehouse D")
            D.addSomething(newItem) #Adding the item to this warehouse 
    else:
        print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D") #Error message appear if the user doesn't select A, B, C or D


def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse


#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')


#Declaring objects and variables
first = Function()
second = Function2()
capacity = 2000000000

#Asking the user to pick a warehouse to place the item within
while True: #Keeps user in a loop until they pick an option of A, B, C or D
    print("Warehouse Menu\n1.Add Item\n2.View Warehouse\n3.Move Items\n4.Exit")
    selection = input("Please Select 1, 2, 3 or 4: ")
    if selection == '1':
        print("You Can Now Add The Item")
        first.addFunction() #Uses the object first to enter the class Function to callback the add function within the class Function
    elif selection == '2':
        print("You Can Now View A Warehouse")
        second.viewSummaryFunction() #Uses the object second to enter the class Function2 to callback the view summary function within the class Function2
    elif selection == '3':
        print("You Can Now Move An Item")
        movingItems() #Callbacks the move items function & sends the user to this function
    elif selection == '4':
        print("Thank You\nGoodbye")
        sys.exit() #directing exits the program
    else:
        print("Error: Unknown Option Selected(Must Select 1, 2 3 or 4)") #Error message appear if the user doesn't select 1, 2, 3, 4
