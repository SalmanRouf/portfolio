/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;

/**
 *
 * @author s2-rouf
 */
import java.util.ArrayList;

public class Team {

	private String teamName;
        
        private ArrayList<Player> teamPlayers;
        
        private ArrayList<String> playerNames;
        
        public int matchesPlayed, matchesWon, setsWon;


	public Team(String teamName) {
            this.teamName = teamName;
            teamPlayers = new ArrayList<Player>();
	}

	public String getName() {
		return teamName;
	}
        
        public void addPlayer (Player player){
            teamPlayers.add(player);
        }
        
        public ArrayList<Player> getTeamPlayers() {
            return teamPlayers;           
        }
        
        public ArrayList<String> getPlayerNames() {
            playerNames = new ArrayList<>();
            for (int i = 0; i < teamPlayers.size(); i++) {
                playerNames.add(teamPlayers.get(i).getName());
            }
            return playerNames;
        }
}