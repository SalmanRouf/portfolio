
<?php
header('Content-Type: text/html; charset="ISO-8859-15"'); 
require("connect.php");

if(isset($_GET['name'])) {
    $name = $_GET['name'];
}

$results = $db->query("SELECT * FROM POI WHERE name = '$name'");
$poi = $results->fetch_object();
$lib = require_once('twitterlib.php');
$hashtag = $lib[$name]['SearchTerms'][0];

?>

<!DOCTYPE html>
<html>
    
    <h3>Place of Interest</h3>
    <div id="contentframe" style="position:relative; top: -400px; left: 0px;">
      <table border="1" cellpadding="4" cellspacing="0" bordercolor="#cccccc" width="400" style="position:relative">
         <tr>
            <td> Name</td>
            <td> <?php echo$poi->name ?></td>
         </tr>
         <tr>
            <td> Address </td>
            <td><?php echo $poi->address ?></td>
        </tr>
        <tr>
            <td> Description </td>
            <td> <?php echo $poi->description ?></td>
        </tr>
        <tr>
            <td> URI </td>
            <td><a href="<?php echo $poi->uri ?>">Website</a></td>
        </tr>
        <tr>
            <td> Construction </td>
            <td><?php echo $poi->construction ?> </td>
        </tr>
        <tr>
            <td> Type </td>
            <td><?php echo $poi->type ?></td>
        </tr>
        <tr>
            <td> Contact </td>
            <td><?php echo $poi->contact ?></td>
        </tr>
        <tr>
            <td> Altitude </td>
            <td><?php echo $poi->altitude ?></td>
        </tr>
        <tr>
            <td> City </td>
            <td> <?php echo $poi->city_name ?></td>
        </tr>
        <tr>
            <td> Latitude </td>
            <td><?php echo $poi->lat ?></td>
        </tr>
        <tr>
            <td> Longitude </td>
            <td> <?php echo $poi->long ?></td>
        </tr>
        <tr>
            <td> Image </td>
            <td><image src="http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/images/<?php echo $poi->image ?>" style="width:250px;height:250px"></td>
        </tr>
</div>
<div id="contentframe" style="position:relative; top: 800px; left: 410px;">
    <iframe src="TwitterWidget.php?tag=<?php echo $name ?>" width="500px" height="400px" style="position:relative"></iframe><br>
    <a href="https://twitter.com/intent/tweet?button_hashtag=<?php echo $hashtag ?>&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-show-count="false" style="float:right">Tweet #<?php echo $hashtag ?></a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div> 
<div id="contentframe" style="position:absolute; top: 432px; left: 410px;">    
    <image src="http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/images/<?php echo $poi->image ?>" style="width:500px;height:350px">
</div>

</html>




    

 