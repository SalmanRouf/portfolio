/*
 This document controls the fixtures.fxml file and 
sets fixtures for the games and implements initializable 
 */
package TTAssociation;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author andrew
 */
public class FixturesController implements Initializable {
    
    @FXML
    private AnchorPane main;
   
    @FXML
    private TextArea fixturesOutput;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
//        fixtures = manager.getFixtresMatrix;

    }
    public void setFixtures (String fixtures, int numberOfTeams) {
        //size window and show matrix
        int x = (numberOfTeams +1) * 75;
        int y = (numberOfTeams +1) * 25;
        main.setPrefWidth(x);
        main.setPrefHeight(y);
        fixturesOutput.setPrefWidth(x-2);
        fixturesOutput.setPrefHeight(y-2);
        fixturesOutput.setText(fixtures);
    }
        
        
    }    
    

