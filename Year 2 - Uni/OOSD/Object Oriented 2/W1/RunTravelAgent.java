
package UFCFB6302.PracticalWeek1.Test3;

public class RunTravelAgent 
{
    public static void main(String[] args)
    {
        Holiday h1 = new Holiday("Bermuda", 2, 800);
        Holiday h2 = new Holiday("Hull", 14, 8);
        Holiday h3 = new Holiday("Los Angeles", 12, 2100);

        TravelAgent t1 = new TravelAgent("CheapAsChips", "MA99 1CU");
        t1.addHoliday(h1);
        t1.addHoliday(h2);
        t1.addHoliday(h3);
        
        TravelAgent t2 = new TravelAgent("Shoe String Tours", "CO33 2DX");
        
        System.out.println(t1);
        
        System.out.printf("h3 Duration=%s days & Cost=$%s\n", h3.getDuration(), h3.getCost());
        System.out.printf("t2 %s %s\n", t2.getName(), t2.getPostcode());
    }
}
