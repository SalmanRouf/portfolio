// // This sample uses the Apache HTTP client from HTTP Components (http://hc.apache.org/httpcomponents-client-ga/)
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class JavaSample 
{
    public static void main(String[] args) 
    {
        HttpClient httpclient = HttpClients.createDefault();

        try
        {
            URIBuilder builder = new URIBuilder("https://api.videoindexer.ai/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos/7a1abb93b4/Captions");

            builder.setParameter("format", "Vtt");
            builder.setParameter("accessToken", "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJWaWRlb0lkIjoiNmY4ZGE1ZDZjNSIsIkFsbG93RWRpdCI6IlRydWUiLCJFeHRlcm5hbFVzZXJJZCI6ImEyMzdlNDIxYjdiNDVhMjIiLCJVc2VyVHlwZSI6Ik1pY3Jvc29mdCIsIm5iZiI6MTU4MjkwMzE4MSwiZXhwIjoxNTgyOTA3MDgxLCJpc3MiOiJodHRwczovL3d3dy52aWRlb2luZGV4ZXIuYWkvIiwiYXVkIjoiaHR0cHM6Ly93d3cudmlkZW9pbmRleGVyLmFpLyJ9.DHGiSKbZA6JG3xZV86NUzWcgFzHX_XYuREBw14_dYxY"
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);
            request.setHeader("x-ms-client-request-id", "");
            request.setHeader("Content-Type", "multipart/form-data");
            request.setHeader("Ocp-Apim-Subscription-Key", "79634650c39241a8af5030c89c325bfd");


            // Request body
            StringEntity reqEntity = new StringEntity("{body}");
            request.setEntity(reqEntity);

            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null) 
            {
                System.out.println(EntityUtils.toString(entity));
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}

