<?php
    #adding the header path
    require "header.php";
?>
<!DOCTYPE html>
<head>
    <!-- path to css file -->
    <link rel="stylesheet" href="css/placement.css" type="text/css"/>
</head>
<body>
    <div id="table-container">
        <div class="container-placement">
            <div class="square">
                <div class="layer"></div>
                    <div class="content">
                        <h1>Money Heist</h1>
                        <div class="image">
                            <img src="images/money-heist.jpeg">
                        </div>
                        <div class="details">
                            <h2>Creator: Álex Pina</h2>
                            <h3>Stars: Úrsula Corberó, Álvaro Morte, Itziar Ituño</h3>
                        </div>
                    </div>
            </div>
            <div class="square">
                <div class="layer"></div>
                    <div class="content">
                        <h1>Westworld</h1>
                        <div class="image">
                            <img src="images/westworld.jpeg">
                        </div>
                        <div class="details">
                            <h2>Creators: Lisa Joy, Jonathan Nolan</h2>
                            <h3>Stars: Evan Rachel Wood, Jeffrey Wright, Ed Harris</h3>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</body>