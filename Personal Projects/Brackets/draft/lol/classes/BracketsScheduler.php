<?php

class BracketsScheduler
{

    private $names;
    private $teams;
    private $rounds;
    private $byes;
    private $brackets;

    public function __construct($names, $teams, $rounds, $byes)
    {

        $this->names = $names;
        $this->teams = $teams;
        $this->rounds = $rounds;
        $this->byes = $byes;

        $this->generateBrackets();

    }

    private function generateBrackets()
    {

        //If no names have been entered, then use numbers

        if ($this->names != '') {

            $teams = array_filter(array_map('htmlspecialchars', array_unique(array_map('trim', explode("\n", $this->names)))));

        } else {

            $noTeams = ((is_numeric($this->teams)) && ($this->teams > 0)) ? $this->teams : 5;

            $teams = range(1, $noTeams);

        }

        $noRounds = ((is_numeric($this->rounds)) && ($this->rounds > 0)) ? $this->rounds : 1;

        //If there's an odd number of teams then add a null

        if (count($teams) % 2) $teams[] = null;

        $away = array_splice($teams, (count($teams) / 2));
        $home = $teams;

        $brackets = array();

        for ($round = 1; $round <= $noRounds; $round++) {

            for ($i = 1; $i < count($home) + count($away); $i++) {

                for ($j = 0; $j < count($home); $j++) {

                    //If byes should be included or not

                    if ($this->byes || (!$this->byes && !is_null($home[$j]) && !is_null($away[$j]))) {

                        $brackets[$round][$i][$j] = array($home[$j], $away[$j]);

                        //Reverse the brackets for each even round

                        if ($round % 2 == 0) $brackets[$round][$i][$j] = array_reverse($brackets[$round][$i][$j]);

                    }

                }

                //Move the brackets around for a unique pairing

                if (count($home) + count($away) - 1 > 2) {

                    $splice = array_splice($home, 1, 1);
                    $shift = array_shift($splice);
                    array_unshift($away, $shift);
                    array_push($home, array_pop($away));

                }

            }

        }

        $this->brackets = $brackets;

    }

    public function printBrackets()
    {

        foreach ($this->brackets as $roundNumber => $round) {

            echo '<h2>Round ' . $roundNumber . '</h2>';

            foreach ($round as $matchNumber => $match) {

                echo '<table><tr><th colspan="3">Match ' . $matchNumber . '</th></tr>';

                foreach ($match as $teams) {

                    $bye = '<span class="bye">BYE</span>';

                    $teams[0] = is_null($teams[0]) ? $bye : $teams[0];
                    $teams[1] = is_null($teams[1]) ? $bye : $teams[1];

                    echo '<tr><td>' . $teams[0] . '</td><td>vs</td><td>' . $teams[1] . '</td></tr>';

                }

                echo '</table>';

            }

        }

    }

}