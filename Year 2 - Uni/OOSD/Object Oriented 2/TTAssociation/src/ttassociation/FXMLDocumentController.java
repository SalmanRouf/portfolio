/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;


/**
 *
 * @author s2-rouf
 */
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Window;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


/**
 *
 * @author andrew
 */
public class FXMLDocumentController implements Initializable {
    
    private TableTennisManager manager = new TableTennisManager();
    
    ObservableList<String> teamNames, homePlayerNames, awayPlayerNames;
    
    @FXML
    private ComboBox<String> selectAddTeam, selectHomeTeam, selectAwayTeam, 
            awayPlayer1, awayPlayer2, homePlayer1, homePlayer2;
    
    @FXML
    private Label label;
    
    @FXML
    private Button addTeamButton, addPlayerButton;
    
    @FXML
    private TextField teamNameField, playerNameField, set1game1, set1game2,
            set1game3, set2game1,set2game2, set2game3, set3game1, set3game2,
            set3game3, set4game1,set4game2, set4game3, dsetgame1, dsetgame2,
            dsetgame3;
    
    @FXML
    private TextArea statisticsOutput;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        manager.addTeam("Filton");
        manager.addTeam("UWE");
        manager.addTeam("KCC");
        manager.addTeam("Page");
        manager.addPlayer("Alex", "Filton");
        manager.addPlayer("Brian", "Filton");
        manager.addPlayer("Jin", "UWE");
        manager.addPlayer("Julia", "UWE");
        manager.addPlayer("Stewart", "UWE");
        manager.addPlayer("Chris", "KCC");
        manager.addPlayer("Ryan", "KCC");
        manager.addPlayer("Peter", "Page");
        manager.addPlayer("Phil", "Page");
        
        
        teamNames = FXCollections.observableArrayList(manager.getTeamNames());
        selectAddTeam.setItems(teamNames);
        selectHomeTeam.setItems(teamNames);
        selectAwayTeam.setItems(teamNames);
        
        manager.generateFixtures();
        ArrayList<Team> teams = manager.getTeams();
        Team filton = teams.get(0);
        Team uwe = teams.get(1);
        ArrayList<Player> filtonPlayers = filton.getTeamPlayers();
        Player alex = filtonPlayers.get(0);
        Player brian = filtonPlayers.get(1);

        ArrayList<Player> uwePlayers = uwe.getTeamPlayers();
        Player jin = uwePlayers.get(0);
        Player julia = uwePlayers.get(1);
        Match match1 = new Match(filton, uwe);
        match1.setHomePlayers(alex, brian);
        match1.setAwayPlayers(jin, julia);
        
        
        int [][] scores = new int[][]{
          {11, 2, 3, 11, 11, 5},
          {1, 11, 5, 11, 11, 6},
          {11, 9, 11, 1, 11, 1},
          {11, 2, 3, 11, 11, 5}, 
          {0, 11, 1, 11, 2, 11}
        };  
    
        
        match1.setScores(scores);
        manager.addMatch(match1);
        
//        manager.fixtures.get(13);

    }
    

    @FXML
    protected void addTeamButtonAction (ActionEvent event) {       
        Window owner = addTeamButton.getScene().getWindow();
        if(teamNameField.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please enter a team name");
        }
        
        AlertHelper.showAlert(Alert.AlertType.CONFIRMATION, owner, "Success!", 
                "Welcome " + teamNameField.getText());
        manager.addTeam(teamNameField.getText());
        teamNames = FXCollections.observableArrayList(manager.getTeamNames());
        selectAddTeam.setItems(teamNames);
        selectHomeTeam.setItems(teamNames);
        selectAwayTeam.setItems(teamNames);
        teamNameField.clear();
        System.out.println(manager.getTeams());
    }
    
    @FXML
    protected void addPlayerButtonAction (ActionEvent event) {       
        Window owner = addPlayerButton.getScene().getWindow();
        if(playerNameField.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please enter a player name");
        }
        if(selectAddTeam.getValue() == null) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please select a team");
        }
        System.out.println(selectAddTeam.getValue());
        manager.addPlayer(playerNameField.getText(), selectAddTeam.getValue());
        
    }
    @FXML
    protected void generateFixturesButtonAction (ActionEvent event) {
        manager.generateFixtures();
        
    }
    @FXML
    protected void viewFixturesButtonAction (ActionEvent event) { 
        Stage stage = new Stage();
        GridPane gridPane = new GridPane();
        Scene scene = new Scene(gridPane);
        stage.setScene(scene);
        stage.show(); 
    
    }
    
    @FXML
    protected void viewTeamStatsButtonAction (ActionEvent event) {
        statisticsOutput.setText(manager.calculateStatistics());
    }
    
    @FXML
    protected void homeTeamSelected (ActionEvent event) {
        homePlayerNames = FXCollections.observableArrayList(manager.getPlayerNames(selectHomeTeam.getValue()));
        homePlayer1.setItems(homePlayerNames);
        homePlayer2.setItems(homePlayerNames);
    }
    
    @FXML
    protected void awayTeamSelected (ActionEvent event) {
        awayPlayerNames = FXCollections.observableArrayList(manager.getPlayerNames(selectAwayTeam.getValue()));
        awayPlayer1.setItems(awayPlayerNames);
        awayPlayer2.setItems(awayPlayerNames);
    }
   
    @FXML
    protected void submitScoresButtonAction () {
        int [][] scores = new int[5][6];
//        System.out.println(getHomeScoreFromEntry(set1game1.getText()));
//        System.out.println(getAwayScoreFromEntry(set1game1.getText()));
        scores[0][0] = getHomeScoreFromEntry(set1game1.getText());
        scores[0][1] = getAwayScoreFromEntry(set1game1.getText());
        scores[0][2] = getHomeScoreFromEntry(set1game2.getText());
        scores[0][3] = getAwayScoreFromEntry(set1game2.getText());
        scores[0][4] = getHomeScoreFromEntry(set1game3.getText());
        scores[0][5] = getAwayScoreFromEntry(set1game3.getText());
        
        scores[1][0] = getHomeScoreFromEntry(set2game1.getText());
        scores[1][1] = getAwayScoreFromEntry(set2game1.getText());
        scores[1][2] = getHomeScoreFromEntry(set2game2.getText());
        scores[1][3] = getAwayScoreFromEntry(set2game2.getText());
        scores[1][4] = getHomeScoreFromEntry(set2game3.getText());
        scores[1][5] = getAwayScoreFromEntry(set2game3.getText());
        
        scores[2][0] = getHomeScoreFromEntry(set3game1.getText());
        scores[2][1] = getAwayScoreFromEntry(set3game1.getText());
        scores[2][2] = getHomeScoreFromEntry(set3game2.getText());
        scores[2][3] = getAwayScoreFromEntry(set3game2.getText());
        scores[2][4] = getHomeScoreFromEntry(set3game3.getText());
        scores[2][5] = getAwayScoreFromEntry(set3game3.getText());
        
        scores[3][0] = getHomeScoreFromEntry(set4game1.getText());
        scores[3][1] = getAwayScoreFromEntry(set4game1.getText());
        scores[3][2] = getHomeScoreFromEntry(set4game2.getText());
        scores[3][3] = getAwayScoreFromEntry(set4game2.getText());
        scores[3][4] = getHomeScoreFromEntry(set4game3.getText());
        scores[3][5] = getAwayScoreFromEntry(set4game3.getText());
        
        scores[4][0] = getHomeScoreFromEntry(dsetgame1.getText());
        scores[4][1] = getAwayScoreFromEntry(dsetgame1.getText());
        scores[4][2] = getHomeScoreFromEntry(dsetgame2.getText());
        scores[4][3] = getAwayScoreFromEntry(dsetgame2.getText());
        scores[4][4] = getHomeScoreFromEntry(dsetgame3.getText());
        scores[4][5] = getAwayScoreFromEntry(dsetgame3.getText());
        
        
        
        
        
    }
    
    public int getHomeScoreFromEntry (String score) {
        String res = "";
        for (int i = 0; i < 2; i++) {
            Character character = score.charAt(i);
            if (Character.isDigit(character)) {
                res += character;
            }
        }
        return Integer.parseInt(res);
    }
    
     public int getAwayScoreFromEntry (String score) {
        String res = "";
        for (int i = 2; i < 4; i++) {
            Character character = score.charAt(i);
            if (Character.isDigit(character)) {
                res += character; 
            }
        }
        return Integer.parseInt(res);
    }
    
}
