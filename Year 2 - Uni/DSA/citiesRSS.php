<?php 
header("Content-Type: application/rss+xml; charset=ISO-8859-1");
require("connect.php");    
//create array for cities
$cities = array();
//get cities data from database    
if($results = $db->query("SELECT * FROM City")) {   
    if($results->num_rows) {
        //populate array
        while($row = $results->fetch_object()) {
            $cities[] = $row; 
        }
        $results->free();
    }
}
//start rss string
$rssfeed = '<?xml version="1.0" encoding="ISO-8859-1"?>';
$rssfeed .= '<rss version="2.0">';
$rssfeed .= '<channel>';
$rssfeed .= '<title>Twin Cities</title>';
$rssfeed .= '<link>/www.cems.uwe.ac.uk/~ad2-biggins/Assignment/citiesRSS.php</link>';
$rssfeed .= '<description>Twin cities and their points of interest</description>';


foreach($cities as $city) {
    //add each city to the rss
    $rssfeed .= '<item>';
    $rssfeed .= '<title>' . $city->city_name . '</title>';
    $rssfeed .= '<description><![CDATA[ woeID: '. $city->woeID .'<br /> Population: '. $city->population .'<br /> Country: '
                . $city->country .'<br /> Currency: '. $city->currency .'<br /> Language: '. $city->language .'<br />Area (sqm): '. $city->area
                . '<br /> Time-zone: '. $city->time_zone . '<br /> Dial Code: ' . $city->dial_code.']]> </description>';
    $rssfeed .= '</item>';
    //create array for points of interests
    $POIs = array();
    //get points of interest data for current city from database
    if($results = $db->query("SELECT * FROM POI WHERE city_name = '$city->city_name'")) {
        if($results->num_rows) {
            //populate
            while($row = $results->fetch_object()) {
                $POIs[] = $row;
            }
            $results->free();
        }
    }
    foreach($POIs as $poi) {
        //add each point of interest to the rss
        $rssfeed .= '<item>';
        $rssfeed .= '<title>' . $poi->name . '</title>';
        $rssfeed .= '<description><![CDATA[ Address: '. $poi->address .'<br /> Population: '. $poi->description .'<br /> Year constructed: '
                    . $poi->construction .'<br /> Type: '. $poi->type .'<br /> Contact number: '. $poi->contact .'<br />Altitude: '
                    . $poi->altitude .'<br />Lat: '. $poi->lat. '<br />Long: '. $poi->long. '<br />Website: '. $poi->uri .']]> </description>';
        $rssfeed .= '</item>';
    }

}
//finish the rss
$rssfeed .= '</channel>';
$rssfeed .= '</rss>';
//output the rss
echo $rssfeed;

?>
