<?php
#requiring the config file and function file to be included else the file will not run
require('../lib/config.php');
require_once('../lib/functions.php');
#running the live function from the function file
live($up=1);
if (!file_exists(CURRENCIES1)) {
	update();
}
#setting format to xml
$format = 'xml';
#$_GET params into PHP variables
extract($_GET);
#creating variables from the params2 within the config file and pulling data from the url
$get = array_intersect(PARAM2, array_keys($_GET)); 
#checking if the $cur value is empty, if so an error message is returned
if(empty($cur)){
	error_validation(2100, $format);
	exit;
}
#checking if the $action value is empty, if so an error message is returned
if(empty($action)){
	error_validation(2000, $format);
	exit;
}
#making the entered cur into uppercases
$cur = strtoupper($cur);
#making the entered action into lowercases 
$action = strtolower($action);
#checking if the cur value is a value which fixer has no rates for, if so an error message is returned
if (in_array($cur, NA)) {
	error_validation(2300, $format);
	exit;
}
#checking if the action value is within the action declared within the config file, if not an error message is returned
if(!in_array($action, ACTION)){
    error_validation(2000, $format);
	exit;
} 
#if the action is a post or del, checking that the cur value is a live currencies within my currencies file, if not an error message is returned
if ($action == 'post' || $action == 'del'){
	if (!in_array($cur, LIVE)) {
		error_validation(2200, $format);
		exit;
	}
}
#if the action is put, checking that the cur value is not a live currencies within my currencies file, if it not a not live currencies an error message is returned
if ($action == 'put'){
	if (!in_array($cur, INACTIVE)) {
		error_validation(2200, $format);
		exit;
	}
}
#not allowing the base of GBP to be changed
if ($cur == 'GBP'){
	error_validation(2400, $format);
	exit;
}
#using the action input function for printing the xml from the function file and sending the cur and action variables
action_input($cur, $action);
?>