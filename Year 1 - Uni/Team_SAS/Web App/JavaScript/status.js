var express = require("express");
var conf = require('./config/dev.json')
var ws = require('./wslib');

// start a new instance 
var app = express();

var options = conf.options;

function path(p) {
    options.path = p;
    return options;
}

app.get("/page3", function (request, response) {
    ws.invoke(path("/api/2.0/rti/resources/status?stopIDs=UK_BRIS_EV%3Accae1e20674a45329a83a304a52f873d"), (err, data) => {
        if (err) response.status(404).send("Oops!");
        else response.send(data);
    });
});

// Listen for requests 
app.listen(8080);
console.log("Listening on port 8080"); 