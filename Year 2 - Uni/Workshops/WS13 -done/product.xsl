<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="4.0" indent="yes"/>
    <xsl:variable name="space">
        <xsl:text>  </xsl:text>
    </xsl:variable>
    <xsl:template match="product">
        <html>
            <head>
                <title>Products XSLT</title>
            </head>
            <body>
                <h3>Products</h3>
                <table border="1" cellpadding="4" cellspacing="0" bordercolor="#cccccc" width="400">
                    <tr>
                        <td width="100">Category</td>
                        <td>
                            <xsl:value-of select="category"/>
                        </td>
                    </tr>
                    <xsl:apply-templates/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="product/@id">
        <tr>
            <td align="left" valign="top">ID</td>
            <td>
                <xsl:value-of select="@id"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="name">
        <tr>
            <td align="left" valign="top">Product</td>
            <td>
                <xsl:value-of select="name"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="description">
        <tr>
            <td align="left" valign="top">Description</td>
            <td>
                <xsl:for-each select="description"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="rrp">
        <tr>
            <td align="left" valign="top">RRP</td>
            <td>
                <xsl:for-each select="rrp"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="price">
        <tr>
            <td align="left" valign="top">Price</td>
            <td>
                <xsl:for-each select="price"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="manufacturer">
        <tr>
            <td align="left" valign="top">Image</td>
            <td>
                <xsl:for-each select="manufacturer"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="image">
        <tr>
            <td align="left" valign="top">Image</td>
            <td>
                <xsl:for-each select="image"/>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="keywords">
        <tr>
            <td align="left" valign="top">Keywords</td>
            <td>
                <xsl:for-each select="keywords"/>
            </td>
        </tr>
    </xsl:template>
    <!--<xsl:template match="text()"/>-->
</xsl:stylesheet>
