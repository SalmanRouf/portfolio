<?php
/**
The following code makes API call and displays the result. Relavent attributes are 
used to send url to Flickr API. A simple caching mechanism is used to demostrate its 
efficiency. 

Initially, the first call checks if there is any cached file, if not it 
creates one and on second call onwards until 2 hours, it will use the data from cached 
file unless necessary for any modification. 

This file depends upon get_image.php file.The document respons 
according to the url. 
	1) If "Liverpool" is passed as city (?city=Liverpool)
		images related to Liverpool will be displayed and
		cached_Liv.json file will be created. 
	2) If "Cologne" is passed as city (?city=Cologne)
		images related to Cologne will be displayed and
		cached_Col.json file will be created.  
	3) If nothing is passed as city or wrong city name is passed
		user will be alerted and city will be set to Liverpool and 
		cached_Liv.json file will be created.   

NOTE: Caching mechanism was inspired by: https://www.youtube.com/watch?v=fdpnT2iPYS0
*/
?>

<?php
	require "get_image.php";
	//checks if query is passed 
	if(isset($_GET["city"])){
		//gets value of city from url 
		$city = $_GET["city"];
	}
	// if not passed,mannualy sets $city to Liverpool
	else{
		// alerts user city is set of Liverpool since no query is passed 
		echo('<script type="text/javascript">
			alert("No query passed!!! City set to Liverpool!");
			</script>');
		$city = "liverpool";
	}
	$keys = json_decode(file_get_contents("../config.json"), true);
    $api_key = $keys["flickrKey"];

	// tags, weoID,lat & long of Liverpool
	$tagsL= 'liverpool,Anfield';
	$woeIDL="26734";
	$latL='51.454514';
	$lonL='-2.587910';

	// tags, weoID,lat & long of Cologne
	$tagsC= 'Cologne';
	$woeIDC = "2345487";
	$latC= "50.936389";
	$lonC= "6.952778";
	//limits result per page
	$perPage = 250;
	//search url 
	$url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search';
	// adding api key to url
	$url.= '&api_key='.$api_key;
	//condition to add item to url based on city
	if(strtolower($city) == "liverpool"){
		echo("<h1 style='text-align:center;'>LIVERPOOL</h1>");
		$url .= '&tags='.$tagsL;
		$url.= '&woe_id='.$woeIDL; 
		$url.= '&lat='.$latL;
		$url.= '&lon='.$lonL;

		$url.= '&per_page='.$perPage;
		$url.= '&format=json';
		// returns raw JSON without any wrapper function
		$url.= '&nojsoncallback=1';

		//decodes received JSON file from API call
		$flickr_response = json_decode(file_get_contents($url));


		// cache images for 2 hour and refreshes 
		$cache_time = 7200; 
		//stores cached images in cache folder
		$file_Liv= "cache/cached_Liv.json"; 
		 
		// check the file, if not found, creates file and write in it
		if ($file_Liv == false){
			//opens files to write image info in it. 
			$fh = fopen($file_Liv, 'w');
		    fwrite($fh, json_encode($flickr_response));
		    //closes the file
		    fclose($fh);
		  }
		  /* if file is not found, or if time since 
		     modification is longer than specified,
		  	the file is filled with new response
		  */
		  	//date("U") & filemtime returns Unix timestamp,
		 if (!is_file($file_Liv) || (date("U") - filemtime($file_Liv)) > $cache_time){
		    file_put_contents($file_Liv,json_encode($flickr_response));

		    // photo_array is passed as param in get_image.php
			$photo_array = $flickr_response->photos->photo;
			//calling function from get_image.php
		    get_photo($photo_array);
		    echo('<script type="text/javascript">
			console.log("Liv Direct API Call");
			</script>');
		  }

		  //  displays images from cached file
		  else{
		    $cached_images = json_decode(file_get_contents($file_Liv));
		    $photo_array_cache = $flickr_response->photos->photo;
		    get_photo($photo_array_cache);
		   	echo('<script type="text/javascript">
			console.log("Liv Cached File used");
			</script>');
		  } 
	}

	// if city is passed as cologne
	elseif (strtolower($city) == "cologne"){
		echo("<h1 style='text-align:center;'>COLOGNE</h1>");
		$url.= '&tags='.$tagsC;
		$url.= '&woe_id='.$woeIDC; 
		$url.= '&lat='.$latC;
		$url.= '&lon='.$lonC; 

		$url.= '&per_page='.$perPage;
		$url.= '&format=json';
		// returns raw JSON without any wrapper function
		$url.= '&nojsoncallback=1';
		
		//decodes received JSON file from API call
		$flickr_response = json_decode(file_get_contents($url));
		

		// cache images for 2 hour and refreshes 
		$cache_time = 7200; 
		//stores cached images in cache folder
		$file_Col = "cache/cached_Col.json"; 
		 
		// check the file_Col, if not found, creates file and write in it
		if ($file_Col == false){
			//opens files to write image info in it. 
			$fh = fopen($file_Col, 'w');
		    fwrite($fh, json_encode($flickr_response));
		    //closes the file
		    fclose($fh);
		  }
		  /* if file is missing or if time since last update is longer than specified,
		  	the file is updated with new response
		  */
		 if (!is_file($file_Col) || (date("U") - filemtime($file_Col)) > $cache_time){
		    file_put_contents($file_Col,json_encode($flickr_response));

		    // photo_array is passed as param in get_image.php
			$photo_array = $flickr_response->photos->photo;
			//calling function from get_image.php
		    get_photo($photo_array);
		    echo('<script type="text/javascript">
			console.log("Col Direct API Call");
			</script>');
		  }

		  //  displays images from cached file 
		  else{
		    // displaying images from cache
		    $cached_images = json_decode(file_get_contents($file_Col));
		    $photo_array = $flickr_response->photos->photo;
		    get_photo($photo_array);
		    //var_dump($photo_array);
		    echo('<script type="text/javascript">
			console.log("Col cached file used.");
			</script>');
		  }
	}
	else{
		// prevents query not passed warning to display and 
		// alerts user, City must be Liverpool and Cologne
		error_reporting(0);
		ini_set('display_errors', 0);
		echo('<script type="text/javascript">
			alert("City must be Liverpool or Cologne!");
			</script>');
	}
?>