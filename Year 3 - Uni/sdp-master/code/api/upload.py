########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'x-ms-client-request-id': '',
    'Content-Type': 'multipart/form-data',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    'privacy': 'Private',
    'language': 'English',
    'videoUrl': 'https://gitlab.uwe.ac.uk/s2-rouf/sdp/blob/master/trim.BA439E98-A9DB-4839-B2D4-FC4F9DC59156.MOV',
    'accessToken': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJBbGxvd0VkaXQiOiJUcnVlIiwiRXh0ZXJuYWxVc2VySWQiOiJhMjM3ZTQyMWI3YjQ1YTIyIiwiVXNlclR5cGUiOiJNaWNyb3NvZnQiLCJpc3MiOiJodHRwczovL3d3dy52aWRlb2luZGV4ZXIuYWkvIiwiYXVkIjoiaHR0cHM6Ly93d3cudmlkZW9pbmRleGVyLmFpLyIsImV4cCI6MTU3NTg5MDc2MiwibmJmIjoxNTc1ODg2ODYyfQ.0eVysbS4famtiGZ2UgsZZwVoyyEQhUOAEt0kEpfD3h8',
})

try:
    conn = http.client.HTTPSConnection('api.videoindexer.ai')
    conn.request("POST", "/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos?name=chri&%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
