<?php

error_reporting(E_ALL);

include('classes/BracketsScheduler.php');

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>League Brackets Scheduler</title>
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <h1>League Brackets Scheduler</h1>
        <form method="post" action="index.php">
            <label for="teams">Number of teams:</label>
            <input type="text" name="teams" id="teams" value="<?php echo isset($_POST['teams']) ? $_POST['teams'] : '' ?>">
            <div id="or">OR</div>
            <label for="names">Team names (one per line):</label>
            <textarea name="names" id="names"><?php echo isset($_POST['names']) ? $_POST['names'] : '' ?></textarea>
            <label for="rounds">Number of rounds:</label><input type="text" name="rounds" id="rounds" value="<?php echo isset($_POST['rounds']) ? $_POST['rounds'] : '' ?>">
            <label for="byes">Show byes:</label><input type="checkbox" name="byes" id="byes" value="1" <?php echo (isset($_POST['byes']) || !isset($_POST['submit'])) ? 'checked' : '' ?>>
            <input type="submit" name="submit" value="Generate Schedule">
        </form>
        <?php

            if(isset($_POST['submit'])){

                $brackets = new BracketsScheduler($_POST['names'], $_POST['teams'], $_POST['rounds'], isset($_POST['byes']));

                $brackets->printBrackets();

            }

        ?>
        <div id="version">Version: 1.0</div>
    </body>
</html>