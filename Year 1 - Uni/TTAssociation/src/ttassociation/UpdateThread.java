/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TTAssociation;

import static java.lang.Thread.sleep;

/**
 *
 * @author andrew
 */
public class UpdateThread implements Runnable {
    private final int interval;
    private TableTennisManager manager;
    
    public UpdateThread (int intervalTime, TableTennisManager ttManager) {
        interval = intervalTime;
        manager = ttManager;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                manager.calculateStatistics();
                sleep(interval); 
            }
        } catch (InterruptedException e) {
            return; 
       
        }
    }
}