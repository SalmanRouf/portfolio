<?php 
header("Content-Type: application/rss+xml; charset=ISO-8859-15");
require("connect.php");


if (isset($_GET['city_name'])) {
    $city_name = $_GET['city_name'];
} 

//create array for pois
$POIs = array();
//get cities data from database    
if($results = $db->query("SELECT * FROM POI WHERE city_name = '$city_name'")) {   
    if($results->num_rows) {
        //populate array
        while($row = $results->fetch_object()) {
            $POIs[] = $row; 
        }
        $results->free();
    }
}
//Generate the XML
$poiXML = '<?xml version="1.0" encoding="ISO-8859-15"?>';
$poiXML .= '<markers>';
$count = 0;
foreach($POIs as $poi) {
    //add each point of interest to the XML
    $poiXML .= '<marker id ="'.++$count.'" name ="'. $poi->name .'" address ="'.$poi->address.'" 
    lat ="'.$poi->lat.'" lng ="'.$poi->long.'" type ="'.$poi->type.'"/>';
}
$poiXML .= '</markers>';
echo $poiXML;

?>