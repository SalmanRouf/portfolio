import csv
import operator
import sys #allows me to exit the program without having to ask the user twice

class Items:
    def __init__(self, itemNumber, fromWarehouse, toWarehouse):
        self.itemNumber = int (itemNumber)
        self.fromWarehouse = fromWarehouse
        self.toWarehouse = toWarehouse



class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    #add items function
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)

    #appending the items i'm adding into one of the warehouses
    def addSomething(self, item):
        self.items.append(item)
        
    #remove function
    def removeItem(self, item):
        self.items.remove(item)

    
def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse
    


#Reading CSV file from each warehouse
##A = readingCSV(r'Warehouse A.csv', 'A')
##B = readingCSV(r'Warehouse B.csv', 'B')
##C = readingCSV(r'Warehouse C.csv', 'C')
##D = readingCSV(r'Warehouse D.csv', 'D')
##E = readingCSV(r'TASK 1.csv', '1')
F = readingCSV(r'TASK 2.csv', '2')
##G = readingCSV(r'TASK 3 AND 4.csv', '3')



class move:       
    def movingItems(functions):
        items = (F.items)
        for item in F.items:
            moving = (item.fromWarehouse, item.toWarehouse)
            lengthTask = len(items)
            if item.fromWarehouse == ('A'):
                print(item.itemNumber, item.fromWarehouse, item.toWarehouse)
            


first = move()
    
while True: 
    print("Warehouse Menu\n1.View Warehouse A\n2.View Warehouse B\n3.View Warehouse C\n4.View Warehouse D\n5.Task 2\n6.View Task 2")
    selection = input("Enter: ")
    itemNo = 'Item'
    fromWarehouse = 'From Warehouse'
    toWarehouse = 'To Warehouse'
    print(f'{itemNo:10}', f'{fromWarehouse:20}', f'{toWarehouse:15}')
    if selection == '5':
        for item in F.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<20}'.format(item.fromWarehouse)), ('{:<15}'.format(item.toWarehouse)))
        first.movingItems()
    elif selection == '6':
        for item in F.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<20}'.format(item.fromWarehouse)), ('{:<15}'.format(item.toWarehouse)))
    else:
        print("Nothing")
