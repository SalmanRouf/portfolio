/*
This document creates players. 
 */
package tt_association;

/**
 * @author CP2/02 Team 3
 */
public class Player { //start of class Player

    private String playerName;

    public Player(String playerName) {
        this.playerName = playerName;

    }

    public String getName() {
        return playerName;
    }
} // end of class Player
