<?php
    #adding the header path
    require "header.php";
?>
<!DOCTYPE html>
<head>
    <!-- path to css file -->
    <link href="css/layout.css" type="text/css" rel="stylesheet">
    <script src="js/table.js"></script>
</head>
<body>
    <section>
    <!-- LEFT-CONTAINER -->
    <div class="middle-container container">
        <img src="images/Once-Upon-a-Time-in-Hollywood-2.jpeg" alt="Once Upon a Time in Hollywood movie cover" style="width:500px;height:500px;">
    </div>
</section>
<section>
      <!-- MIDDLE-CONTAINER -->
    <div class="middle-container container">
        <div class="p block"> <!-- PROFILE (MIDDLE-CONTAINER) -->
            <h1 class="page-title">Once Upon a Time in Hollywood (2019)</h1>
            <h2>Storyline Description</h2>
                <p class="font-color">A faded television actor and his stunt double strive to achieve fame and success in the film industry during the final years of Hollywood's Golden Age in 1969 Los Angeles.</p>
            <div class="p-description">
            <h2>Rating</h2>
                <img class="preview" src="images/ratings.jpeg" alt="Rating star icon">
                <h2>71%</h2>
            </div>
        </div>
    </div>
</section>
<div class="space"></div>
<!-- Cast and Crew table-->
    <section>
        <h1>Cast and Crew</h1>
        <div class="tbl-header">
            <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                <th>Actor</th>
                <th>...</th>
                <th>Character</th>
                </tr>
            </thead>
            </table>
        </div>
        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td><a href="actor.php?actor=leonardodicaprio">Leonardo DiCaprio</a></td>
                    <td>....</td>
                    <td><a href="">Rick Dalton</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=bradpitt">Brad Pitt</a></td>
                    <td>....</td>
                    <td><a href="">Cliff Booth</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=margotrobbie">Margot Robbie</a></td>
                    <td>....</td>
                    <td><a href="">Sharon Tate</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=emilehirsch">Emile Hirsch</a></td>
                    <td>....</td>
                    <td><a href="">Jay Sebring</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=margaretqualley">Margaret Qualley</a></td>
                    <td>....</td>
                    <td><a href="">Pussycat</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=timothyolyphant">Timothy Olyphant</a></td>
                    <td>....</td>
                    <td><a href="">James Stacy</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=juliabutters">Julia Butters</a></td>
                    <td>....</td>
                    <td><a href="">Trudi Fraser</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=austinbutler">Austin Butler</a></td>
                    <td>....</td>
                    <td><a href="">Tex Watson</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=dakotafanning">Dakota Fanning</a></td>
                    <td>....</td>
                    <td><a href="">Squeaky Fromme</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=brucedern">Bruce Dern</a></td>
                    <td>....</td>
                    <td><a href="">George Spahn</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=mikemoh">Mike Moh</a></td>
                    <td>....</td>
                    <td><a href="">Bruce Lee</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=lukeperry">Luke Perry</a></td>
                    <td>....</td>
                    <td><a href="">Wayne Maunder</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=damianlewis">Damian Lewis</a></td>
                    <td>....</td>
                    <td><a href="">Steve McQueen</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=alpacino">Al Pacino</a></td>
                    <td>....</td>
                    <td><a href="">Marvin Schwarz</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=nicholashammond">Nicholas Hammond</a></td>
                    <td>....</td>
                    <td><a href="">Sam Wanamaker</a></td>
                </tr>
            </tbody>
            </table>
        </div>
    </section>
</body>
<?php
    #adding the footer path
    require "footer.php";
?>