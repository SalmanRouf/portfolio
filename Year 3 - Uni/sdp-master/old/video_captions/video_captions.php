<!DOCTYPE html>
<html>
<head>
    <title>JSSample</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
</head>
<body>

<script type="text/javascript">
    $(function() {
        var params = {
            // Request parameters
            'accessToken':'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJWaWRlb0lkIjoiNmY4ZGE1ZDZjNSIsIkFsbG93RWRpdCI6IlRydWUiLCJFeHRlcm5hbFVzZXJJZCI6ImEyMzdlNDIxYjdiNDVhMjIiLCJVc2VyVHlwZSI6Ik1pY3Jvc29mdCIsIm5iZiI6MTU4MjkwMzE4MSwiZXhwIjoxNTgyOTA3MDgxLCJpc3MiOiJodHRwczovL3d3dy52aWRlb2luZGV4ZXIuYWkvIiwiYXVkIjoiaHR0cHM6Ly93d3cudmlkZW9pbmRleGVyLmFpLyJ9.DHGiSKbZA6JG3xZV86NUzWcgFzHX_XYuREBw14_dYxY',
        };
      
        $.ajax({
            url: "https://api.videoindexer.ai/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos/6f8da5d6c5/Captions?" + $.param(params),
            beforeSend: function(xhrObj){
                // Request headers
                xhrObj.setRequestHeader("x-ms-client-request-id","");
                xhrObj.setRequestHeader("Content-Type","multipart/form-data");
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","79634650c39241a8af5030c89c325bfd");
            },
            type: "GET",
            // Request body
            data: "{body}",
        })
        .done(function(data) {
            //alert("success");
            document.write(data);
        })
        .fail(function() {
            //alert("error");
            document.write('error');
        });
    });
</script>
</body>
</html>