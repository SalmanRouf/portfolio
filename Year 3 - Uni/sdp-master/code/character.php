<?php
#adding the header path
require "header.php";
#path to the characters xml file
$dir = "xml/characters.xml";

if (file_exists($dir)) {
    #loading the currencies file with the current file path
    $xml_currencies=simplexml_load_file($dir) or die("Error: Service Failed");
    #checking if there is an character
    if (isset($_GET['character'])) {
        #stores character name
        $character = $_GET['character'];
        #finds the information within the xml file on the character required
        $xml_print = $xml_currencies->xpath("//id[.='$character']/parent::*");
        #stores the information on the character within seperate variables
        $individual = (string) $xml_print[0]->individual;
        $stripped = str_replace(' ', '', $individual);
        $appeared = (string) $xml_print[0]->appeared;
        $actor = (string) $xml_print[0]->actor;
        $desc = (string) $xml_print[0]->desc;
        $trivia = (string) $xml_print[0]->trivia;
        $img1 = (string) $xml_print[0]->images->img1;
        $img2 = (string) $xml_print[0]->images->img2;
        $img3 = (string) $xml_print[0]->images->img3;
        $link = (string) $xml_print[0]->link;
        #prints the the character information within a HTML format
        echo'
        <body>
        <style>
        .responsive {
            width: 100%;
            max-width: 300px;
            height: auto;
        }
    
    
        </style>
        <div style="text-align:center;overflow: scroll;">
            <h1>Character: '.$individual.'</h1>
            <img src="'.$img1.'" alt="'.$individual.' image" class="responsive">
            <span><p>Appeared in: '.$appeared.'</p></span>
            <span><p>Actor: '.$actor.'</p></span>
            <div>'.$desc.'</div>
            <h2>Trivia</h2>
            <a>'.$trivia.'</a>
            <h2>Images</h2>
            <img class="mySlides" src="'.$img3.'">
            <img class="mySlides" src="'.$img2.'">
            <img class="mySlides" src="'.$img1.'">
            <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
            <button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button>
            <h2>Additional Resources</h2>
            <a href="'.$link.'">'.$link.'</a>
        </div>
        <script>
        var slideIndex = 1;
        showDivs(slideIndex);
    
        function plusDivs(n) {
        showDivs(slideIndex += n);
        }
    
        function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {slideIndex = 1}
        if (n < 1) {slideIndex = x.length} ;
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex-1].style.display = "block";
        }
        </script>
        </body>';
    } else{
        #if the character doesn't exist within the xml file then a no match message is returned.
        echo "no character match";
    }
} else{
    #if the xml file is not found then a service error is returned
    echo "service error";
}
#adding the footer path
require "footer.php";
?>