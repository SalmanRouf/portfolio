########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'x-ms-client-request-id': '',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    # Request parameters
    'accessToken': 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJWaWRlb0lkIjoiNmY4ZGE1ZDZjNSIsIkFsbG93RWRpdCI6IlRydWUiLCJFeHRlcm5hbFVzZXJJZCI6ImEyMzdlNDIxYjdiNDVhMjIiLCJVc2VyVHlwZSI6Ik1pY3Jvc29mdCIsIkxvY2F0aW9uIjoiVHJpYWwiLCJuYmYiOjE1ODc2MTY3NTIsImV4cCI6MTU4NzYyMDY1MiwiaXNzIjoiaHR0cHM6Ly93d3cudmlkZW9pbmRleGVyLmFpLyIsImF1ZCI6Imh0dHBzOi8vd3d3LnZpZGVvaW5kZXhlci5haS8ifQ.jYtcHAY5Jr3XjyHg2MHj-bcmfwK-RzjoHguqOAGAVzA',
})

try:
    conn = http.client.HTTPSConnection('api.videoindexer.ai')
    conn.request("GET", "/tril/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos/6f8da5d6c5/PlayerWidget?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
