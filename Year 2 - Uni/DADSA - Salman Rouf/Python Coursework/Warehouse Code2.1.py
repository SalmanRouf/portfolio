import csv

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = itemNumber
        self.itemDescription = itemDescription
        self.itemValue = itemValue


class Warehouse:
    def __init__(self, name):
        self.items = []

        
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)
           # print(item)

        
def readingCSV(file, name):
    with open(file, encoding = 'utf-8', errors = 'ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse
    
def menu():
    menu = {}
    menu['1']="Add Item." 
    menu['2']="View Warehouse."
    menu['3']="Exit"

print(menu)
while True: 
    selection=input("Please Select: ") 
    if selection =='1': 
        print ("Add item")
        break
    elif selection == '2': 
        print ("View Warehouse")
        print ("1. View All Warehouses"
               "2. Select to view warehouse A, B, C or D")
    elif selection == '3': 
        break
    else: 
        print ("Unknown Option Selected!") 


#Asking the use to enter the item number
itemNumber = int(input('Enter the item number: ')) 

#Getting the item descrpition
itemDescription = str(input('Enter the item description: '))

#Item value entered & checked the value to make sure it doesn't exceed the limit.
itemValue = int(input('Enter the item value:£ '))

while itemValue > 2000000000:
    print('The item value has been rejected as the value has exceeded the limit of £2 billion')
    itemValue = int(input('Enter the item value:£ '))
    
#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')

#Asking the user to pick a warehouse to place the item within
four = str(input("Please select a warehouse to place the item within A, B, C or D: "))
if A:
    for item in A.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
elif B:
    for item in B.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
elif C:
    for item in C.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
elif D:
    for item in D.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)

   


