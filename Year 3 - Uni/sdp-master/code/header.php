<?php
  #First we start a session which allow for us to store information as SESSION variables.
  session_start();
  #default setting variables.
  $_SESSION['message'] = '';
  $_SESSION['sucessfull'] = '';
  #linking database path
  require "includes/dbh.inc.php";
  #"require" creates an error message and stops the script. "include" creates an error and continues the script.
  #if a user is not signed in then they are included within the below HTML header.
  if (!isset($_SESSION['id'])) {
    echo '<!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="css/main.css" type="text/css">
      </head>
      <body>
        <header class="header">
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
            <a href="index.php" class="title">Web-based review application</a>
            <nav class="main-nav"> 
                <ul class="nav-links">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="movies.php">Movies</a></li>
                    <li><a href="television.php">Television</a></li>
                    <li><a href="login.php">Login</a></li>
                    <li><a href="signup.php">Sign Up</a></li>
                </ul>
            </nav>
    
            <script src="js/nav.js"></script>
            <div class="clearfix"></div>
        </header>
      </body>
    </html>';
  } #if a user is signed in then they are included within the below HTML header.
  else if (isset($_SESSION['id'])) {
    echo '<!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="css/login.css" type="text/css">
      </head>
      <body>
        <header class="header">
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div> 
            <a href="index.php" class="title">Web-based review application</a>
            <div class="dropdown">
            <img src="'.$_SESSION["avatar"].'" onclick="myFunction()" class="dropbtn" alt="Avatar" />
                <div id="myDropdown" class="dropdown-content">
                <a href="#">User Access API</a>
                <a href="#">Video Access API</a>
                <a href="#">Video Captions API</a>
                <a href="#">Video Insights API</a>
                <a href="#">List Videos API</a>
                <a href="#">Player Widget API</a>
                <a href="#">Upload API</a>
                <a href="#">Delete API</a>
                <form action="includes/logout.inc.php" method="post">
                    <button type="submit" name="login-submit">Logout</button>
                </form>
                </div>
            </div>

            <nav class="main-nav"> 
                <ul class="nav-links">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="movies.php">Movies</a></li>
                    <li><a href="television.php">Television</a></li>
                </ul>
            </nav>
    
            <script src="js/nav.js"></script>

            <script> 
            /* When the user clicks on the button, 
            toggle between hiding and showing the dropdown content */
            function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
            }
            
            // Close the dropdown if the user clicks outside of it
            window.onclick = function(event) {
                if (!event.target.matches(".dropbtn")) {
                var dropdowns = document.getElementsByClassName("dropdown-content");
                var i;
                for (i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (openDropdown.classList.contains("show")) {
                    openDropdown.classList.remove("show");
                    }
                }
                }
            }
            
            </script>
        </nav>
        
        <div class="clearfix"></div>
        </header>
        </body>
      </html>'; 
    }
?>


