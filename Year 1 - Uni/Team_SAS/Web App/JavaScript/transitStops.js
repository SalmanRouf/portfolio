var express = require("express");
var conf = require('./config/dev.json')
var ws = require('./wslib');

// start a new instance 
var app = express();

var options = conf.options;

function path(p) {
    options.path = p;
    return options;
}

app.get("/page2", function (request, response) {
    ws.invoke(path("/api/2.0/static/transitstops?centerLat=51.50021068&centerLng=-2.54749088&radius=10000&maxResults=1&stopModes=14"), (err, data) => {
        if (err) response.status(404).send("Oops!");
        else response.send(data);
    });
});

// Listen for requests 
app.listen(8080);
console.log("Listening on port 8080"); 