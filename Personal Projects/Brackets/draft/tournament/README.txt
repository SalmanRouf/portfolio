Tournament Brackets Generator 2.0 by RMK147

Usage

To start using the Brackets Generator first open the index.php file in a browser.

If you wish to generate brackets with numeric values, then you can enter the number of teams in the first field, and click
the Generate Brackets button.

If you wish to enter custom team names, then you may skip the number of teams field, and enter your teams in the team names
field. Each team name must be on a new line. Any duplicate team names will automatically be removed. The team names field
takes precedence over the number of teams field, if both are filled in. If you are planning on using seeded brackets, then
you must list your players in seeded order, starting with the highest.

In the event there are isn't a perfect number of teams entered (such as 8, 16, 32 etc), teams will automatically be given
BYEs into the second round.

You may select the winner of each match, by first clicking on the appropriate match bracket, and choosing a player from the
drop down menu. You can also enter a player's score by clicking on the score field next to the selected player's name.

Customising

You may customise the look and feel of the generator by editing the CSS file located at assets/css/style.css. For example,
to change the default team name colour from black to red you could do the following:

.player-wrapper{
    color: red;
}

To customise the HTML that is outputted by the brackets generator, you can edit the printBrackets function in the
BracketsGenerator class located at classes/BracketsGenerator.php