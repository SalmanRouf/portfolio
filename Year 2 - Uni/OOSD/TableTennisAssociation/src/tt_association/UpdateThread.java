/*
This class controls the thread. It helps to auto update the team stat every 
100 seconds. This class uses the concept of Concurrency. 
 */
package tt_association;

import static java.lang.Thread.sleep;

/**
 * @author CP2/02 Team 3
 */
public class UpdateThread implements Runnable { // start of class UpdateThread

    private final int interval;
    private TableTennisManager manager;

    public UpdateThread(int intervalTime, TableTennisManager ttManager) {
        interval = intervalTime;
        manager = ttManager;
    }

    @Override
    public void run() {
        try {
            while (true) {
                manager.calculateStatistics();
                sleep(interval);
            }
        } catch (InterruptedException e) {
            return;

        }
    }
} // end of class Update Thread
