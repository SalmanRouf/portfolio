/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practical6Part2;
import java.util.Scanner;
import java.io.*;
import java.util.Arrays;

/**
 *
 * @author ad2-biggins
 */
public class RepeatedOffender {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        InputStream stream =
        RepeatedOffender.class.getResourceAsStream("dna_input.txt");
        Scanner data = new Scanner(stream); 
        Scanner input = new Scanner(System.in);
        
        double [] suspectDNA = new double [10];
        double [] criminalDNA = new double [10];
        int criminalNumber = 0;
        int numberOfCriminals;
        
        System.out.println("Enter suspect DNA: ");
        for (int i = 0; i < suspectDNA.length; i++)
            suspectDNA[i] = input.nextDouble();
        
        numberOfCriminals = data.nextInt();
        
        do
        {
            if (criminalNumber >= numberOfCriminals)
            {
                System.out.println("No matches");
                System.exit(0);
            }
            
            for (int i = 0; i < criminalDNA.length; i++)
                criminalDNA[i] = data.nextDouble();
            
            criminalNumber++;
            
            
        }
        while (!matchingProfiles(suspectDNA, criminalDNA));
      
        System.out.println("The suspect's DNA matches with criminal number " +
                criminalNumber);
    }
    
   public static boolean matchingProfiles(double[] suspectDNA,
    double[] criminalDNA) {
       return Arrays.equals(suspectDNA, criminalDNA);
       
   }
}
