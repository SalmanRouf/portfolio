<!-- footer -->
<head>
  <!-- path to css file -->
    <link rel="stylesheet" href="css/footer.css" type="text/css">
</head>
<body>
<footer>
  <div class="footer-content">
  <nav class="footer-menu">
    <div class="footer-col">  <!-- Web-based applications -->
      <div class="item">
        <input type="checkbox" id="products">
        <div class="col-section">
          <label for="products">
          <h3>Web-based Application</h3>  
        </label>
          <ul class="product-list">
            <li><a href="index.php">Home</a></li>
            <li><a href="movies.php">Movie</a></li>
            <li><a href="television.php">Television</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- Actors -->
    <div class="footer-col">  
      <div class="item">
        <input type="checkbox" id="store">
        <div class="col-section">
          <label for="store">
          <h3>Actors</h3>  
        </label>
          <ul class="product-list">
            <li><a href="actor.php?actor=joaquinphoenix">Joaquin Phoenix</a></li>
            <li><a href="actor.php?actor=robertdeniro">Robert De Niro</a></li>
            <li><a href="actor.php?actor=zaziebeetz">Zazie Beetz</a></li>
            <li><a href="actor.php?actor=francesconroy">Frances Conroy</a></li>
            <li><a href="actor.php?actor=brettcullen">Brett Cullen</a></li>
            <li><a href="actor.php?actor=sheawhigham">Shea Whigham</a></li>
            <li><a href="actor.php?actor=leonardodicaprio">Leonardo DiCaprio</a></li>
            <li><a href="actor.php?actor=bradpitt">Brad Pitt</a></li>
            <li><a href="actor.php?actor=margotrobbie">Margot Robbie</a></li>
            <li><a href="actor.php?actor=emilehirsch">Emile Hirsch</a></li>
            <li><a href="actor.php?actor=margaretqualley">Margaret Qualley</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- Characters -->
    <div class="footer-col"> 
      <div class="item">
        <input type="checkbox" id="business">
        <div class="col-section">
          <label for="business">
          <h3>Characters</h3>  
        </label>
          <ul class="product-list">
            <li><a href="character.php?character=arthurfleck">Arthur Fleck</a></li>
            <li><a href="character.php?character=murrayfranklin">Murray Franklin</a></li>
            <li><a href="character.php?character=sophiedumond">Sophie Dumond</a></li>
            <li><a href="character.php?character=pennyfleck">Penny Fleck</a></li>
            <li><a href="character.php?character=thomaswayne">Thomas Wayne</a></li>
            <li><a href="#">Rick Dalton</a></li>
            <li><a href="#">Cliff Booth</a></li>
            <li><a href="#">Sharon Tate</a></li>
            <li><a href="#">Jay Sebring</a></li>
            <li><a href="#">Alfred Pennyworth</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- Account -->
    <div class="footer-col">
      <div class="item">
        <input type="checkbox" id="acccount">
        <div class="col-section">
          <label for="acccount">
          <h3>Account</h3>  
        </label>
          <ul class="product-list">
            <li><a href="#">Manage Your Account</a></li>
            <li><a href="#">Find A Movie</a></li>
            <li><a href="#">Find A TV Show</a></li>
          </ul>
        </div>
      </div>
      <div class="item item-padding-top"> <!-- Review System -->
        <input type="checkbox" id="values">
        <div class="col-section">
          <label for="values">
          <h3>Review System</h3>   
        </label>
          <ul class="product-list">
            <li><a href="#">Review a movie/tv show</a></li>
            <li><a href="#">Upload a review</a></li>
            <li><a href="#">Privacy</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- About Application -->
    <div class="footer-col">
      <div class="item">
        <input type="checkbox" id="about">
        <div class="col-section">
          <label for="about">
          <h3>About Application</h3>  
        </label>
          <ul class="product-list">
            <li><a href="#">Lastest</a></li>
            <li><a href="#">New Movies</a></li>
            <li><a href="#">New TV shows</a></li>
            <li><a href="#">Popular</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <section class="footer-base"> <!-- Bottom Footer -->
    <div class="footer-legal">
		  <div class="footer-legal-copyright">Copyright © 2020 Web-based Application.</div>
		  <div class="footer-legal-links">
			  <a href="#">Privacy Policy</a>
			  <a href="#">Terms of Use</a>
			  <a href="#">Ratings</a>
			  <a href="#">Legal</a>
			  <a href="#">Verified</a>
		  </div>
	  </div>
  </section>
    </div>
</footer>
</body>
