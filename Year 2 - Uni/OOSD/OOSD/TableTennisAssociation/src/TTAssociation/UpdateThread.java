/*
This class controls the thread. It helps to auto update the team stat every 
100 seconds. 
 */
package TTAssociation;

import static java.lang.Thread.sleep;

/**
 *
 * @author andrew
 */
public class UpdateThread implements Runnable {
    private final int interval;
    private TableTennisManager manager;
    
    public UpdateThread (int intervalTime, TableTennisManager ttManager) {
        interval = intervalTime;
        manager = ttManager;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                manager.calculateStatistics();
                sleep(interval); 
            }
        } catch (InterruptedException e) {
            return; 
       
        }
    }
}