<?php
#requiring the config file and function file to be included else the file will not run
require('../../lib/config.php');
require_once('../../lib/functions.php');
#running the live function from the function file
live($up=2);
#checking if core data files are missing 
if (!file_exists(CURRENCIES2)) {
	update();
}
?>
<!DOCTYPE HTML>
<html>
<head>
<head>
      <title>Part 2 - POST, PUT, DELETE</title>
      <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
      <script>
         //post function which is only called upon when the submit button is clicked
        function post(){
         //storing the action and code values from the html below
         var action = $('input[type="radio"]:checked').val();
         var code = $("#codes").val();
         //Sending the variables to a php file called form.inc.php 
         $.post("form.inc.php",{
               codes:code, action:action
         }, function(data){
               //getting the data back from the form.inc.php file and storing the data within the xml_tag id
               $("#xml_text").html(data);
               //unchecking the radio box that was selected
               $('input[type="radio"]:checked').prop('checked', false);
               //unsetting the selected currency code
               $("#codes").val('');
         });
        };
        //function called upon once the user selects a radio button on the html
        function change(arr) {
         //getting the currenct <select> tag by id of codes
         var codes = document.getElementById('codes');
         //setting the default length
         codes.options.length = 0;
         //loops through until every code is enetered into the <select> tag
         for(var i = 0; i< arr.length; i++ ) {
         codes.options[codes.options.length] = new Option( arr[i], arr[i]);
         }
        }
      </script>
   </head>
   <body>
      <div> 
         <h1>Form Interface for POST, PUT, DELETE</h1>
         <p id="options">Actions:
         <label id="radio-label">POST</label>
         <input type="radio" id="post" name="action" value='post' onclick='change(<?php echo json_encode(LIVE);?>)'> 
         <label id="radio-label">PUT</label>
         <input type="radio" id="put"  name="action" value='put' onclick='change(<?php echo json_encode(INACTIVE);?>)'>
         <label id="radio-label">DELETE</label>
         <input type="radio" id="del"  name="action" value='del' onclick='change(<?php echo json_encode(LIVE);?>)'></p>
         <p>Choose a currency code: <select id="codes" name='codes'><option>Select a action</option></select></p>
         <input type="button" id="submit" value="Submit" onclick="post();">
      </div>
      <p id="xml_text"></p>
   </body>
</html>