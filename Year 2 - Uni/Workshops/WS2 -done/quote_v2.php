<!DOCTYPE html> 
<html lang="en"> 
<head>
    <title>Quote of the day : Version 2</title> 
</head> 
<body> 
<h4>Quote of the day ... [version 2]</h4> 

<?php 
echo "<h4>".date("D M d Y")."</h4>"; 

$content = file('quotes.dat'); 

// count how many lines in the file  
$numLines = count($content); 

// generate a random number between 1 and $numLines 
$line = rand(0, ($numLines-1)); 

$data = explode("|", $content[$line]); 
    
// print out the quote 
echo "<p style=\width:600px>"; 
echo htmlentities($data[0]); 
echo "</p>"; 
echo "<p style=\"text-align:left\"><a href=\"$data[3]\">$data[1]</a> ($data[2])</p>"; 

?> 
</body> 
</html> 
