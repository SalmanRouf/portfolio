/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;

/**
 *
 * @author s2-rouf
 */
public class DoubleSet {

	private String homePlayer, awayPlayer;

	private Game [] games;
        

	DoubleSet() {
            games = new Game[3];
            for (int i = 0; i < 3; i++) {
                games[i] = new Game();
            }
	}

    
        public void setHomePlayer(String homePlayer) {
            this.homePlayer = null;
        }
        
        public void setAwayPlayer(String awayPlayer) {
            this.awayPlayer = null;
        }
        
        
        public Game [] getGames() {
            return games;
        }
        
        public void setGameScores (int [] scores) {
             for (int i = 0 ; i < games.length; i++) {
                games[i].setScores(scores[i*2], scores [i*2+1]);
                
            }
        }
        
        public String calculateWinner (int [] scores) {
            int homeScore = 0;
            int awayScore = 0;        
            for (int i = 0; i< scores.length; i+=2) {
                if (scores[i]==11) {
                    homeScore ++;
                }
                if (scores[i+1]==11) {
                    awayScore ++;
                }
            }
            if (homeScore > awayScore) {
                return "home";
            }
            else {
                return "away";
            }
        }
        
        @Override
        
        public String toString() {
            String doubleSet = homePlayer + " vs " + awayPlayer + " ";
            for (int i = 0; i < games.length; i++) {
                doubleSet += games[i].getHomeScore() + ":" + 
                        games[i].getAwayScore(); 
                if (i<games.length-1) {
                    doubleSet += ",";
                }
            }
            return doubleSet;
        }

}
