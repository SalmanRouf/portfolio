import csv
import sys

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = int (itemNumber)
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)


class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)

    def addSomething(self, item):
        self.items.append(item)

    def totalValue(self):
        totalValue = 0
        for i in self.items:
            totalValue = totalValue + i.itemValue
        return totalValue

    def removeItem(self, item):
        self.items.remove(item)


class Function:
    def addFunction(functions):
        # Asking the use to enter the item number
        itemNumber = int(input("Please Enter The Item Number\n"))

        # Getting the item descrpition
        itemDescription = str(input("Please Enter The Item Description\n"))

        # Item value entered & checked the value to make sure it doesn't exceed the limit.
        itemValue = int(input("Please Enter The Item Value(£)\n"))

        while itemValue > capacity:
            print("Error: The Item Value Has Been Rejected As The Value Has Exceeded The Limit Of (£)", capacity)
            sys.exit()
            break

        newItem = Items(itemNumber, itemDescription, itemValue)

        # Asking the user to pick a warehouse to place the item within
        while True:
            warehouseSelection = str(input("Please Select A Warehouse To Place The Item Within A, B, C or D\n"))
            if warehouseSelection == 'A' or warehouseSelection == 'a':
                A.addSomething(newItem)
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print("The Total Value For Warehouse A is", A.totalValue())
                if Warehouse.totalValue(A) > capacity:
                    print("Warehouse Limit is £2000000000\nThe Item Cannot Be Added To The Warehouse\nThe Item Would Make The Warehouse Exceed The Limit By", Warehouse.totalValue(A) - capacity)
                    Warehouse.removeItem(A, newItem)
                break
            elif warehouseSelection == 'B' or warehouseSelection == 'b':
                B.addSomething(newItem)
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(B.totalValue())
                if Warehouse.totalValue(B) > capacity:
                    print("Warehouse Limit is £2000000000\nThe Item Cannot Be Added To The Warehouse\nThe Item Would Make The Warehouse Exceed The Limit By", Warehouse.totalValue(B) - capacity)
                    Warehouse.removeItem(B, newItem)
                break
            elif warehouseSelection == 'C' or warehouseSelection == 'c':
                C.addSomething(newItem)
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(C.totalValue())
                if Warehouse.totalValue(C) > capacity:
                    print("Warehouse Limit is £2000000000\nThe Item Cannot Be Added To The Warehouse\nThe Item Would Make The Warehouse Exceed The Limit By", Warehouse.totalValue(C) - capacity)
                    Warehouse.removeItem(C, newItem)
                break
            elif warehouseSelection == 'D' or warehouseSelection == 'd':
                D.addSomething(newItem)
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(D.totalValue())
                if Warehouse.totalValue(D) > capacity:
                    print("Warehouse Limit is £2000000000\nThe Item Cannot Be Added To The Warehouse\nThe Item Would Make The Warehouse Exceed The Limit By", Warehouse.totalValue(D) - capacity)
                    Warehouse.removeItem(D, newItem)
                break
            else:
                print("Error: Unknown Variable Selected")


class Function2(Function):
    def viewSummaryFunction(functions):
        # Asking the user to pick a warehouse to place the item within
        while True:
            choice = input("Select To View Warehouse A, B, C or D\n")
            if choice == 'A' or choice == 'a':
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'B' or choice == 'b':
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'C' or choice == 'c':
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'D' or choice == 'd':
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            else:
                print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D")


def movingItems():
    itemNumber = None
    movingOption = str(input("Please Enter The Warehouse You Want To Take An Item From [A,B,C or D]\n"))
    if movingOption == "A" or movingOption == "a":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))
        for item in A.items:
            if itemNumber == item.itemNumber:
                A.removeItem(item)
    elif movingOption == "B" or movingOption == "b":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))
        for item in B.items:
            if itemNumber == item.itemNumber:
                B.removeItem(item)
    elif movingOption == "C" or movingOption == "c":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))
        for item in C.items:
            if  itemNumber == item.itemNumber:
                C.removeItem(item)
    elif movingOption == "D" or movingOption == "d":
        itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))
        for item in D.items:
            if itemNumber == item.itemNumber:
                D.removeItem(item)
    else:
        print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D")

    itemDescription = str(input("Please Enter The Item Description For The Item You Are Moving\n"))

    itemValue = int(input("Please Enter The Item Value For The Item You Are Moving (£)\n"))

    nextWarehouse = str(input("Please Enter The Warehouse That You Would Like To Move This Item To [A,B,C or D]\n"))

    newItem = Items(itemNumber, itemDescription, itemValue)

    if nextWarehouse == 'A' or nextWarehouse == 'a':
        if(itemValue > capacity - Warehouse.totalValue(A)):
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(A)):
            print("The Item Has Been Added to Warehouse A")
            A.addSomething(newItem)
    elif nextWarehouse == 'B' or nextWarehouse == 'b':
        if (itemValue > capacity - Warehouse.totalValue(B)):
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(B)):
            print("The Item Has Been Added to Warehouse B")
            B.addSomething(newItem)
    elif nextWarehouse == 'C' or nextWarehouse == 'c':
        if (itemValue > capacity - Warehouse.totalValue(C)):
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(C)):
            print("The Item Has Been Added to Warehouse C")
            C.addSomething(newItem)
    elif nextWarehouse == 'D' or nextWarehouse == 'd':
        if (itemValue > capacity - Warehouse.totalValue(D)):
            print("Error: There Isn't Enough Room Within The Warehouse\nWarehouse Limit £2000000000")
        elif (itemValue < capacity - Warehouse.totalValue(D)):
            print("The Item Has Been Added to Warehouse D")
            D.addSomething(newItem)
    else:
        print("Error: Unknown Variable Selected\nMake Sure That You Are Selecting A, B, C or D")


def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse


#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')


# Declaring objects
first = Function()
second = Function2()
capacity = 2000000000

# Asking the user to pick a warehouse to place the item within
while True:
    print("Warehouse Menu\n1.Add Item\n2.View Warehouse\n3.Move Items\n4.Exit")
    selection = input("Please Select 1, 2, 3 or 4: ")
    if selection == '1':
        print("You Can Now Add The Item")
        first.addFunction()
    elif selection == '2':
        print("You Can Now View A Warehouse")
        second.viewSummaryFunction()
    elif selection == '3':
        print("You Can Now Move An Item")
        movingItems()
    elif selection == '4':
        print("Thank You\nGoodbye")
        sys.exit()
    else:
        print("Error: Unknown Option Selected(Must Select 1, 2 3 or 4)")
