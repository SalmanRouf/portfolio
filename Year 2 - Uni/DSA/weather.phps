<?php 
//Get weather API key from config file
$data = json_decode(file_get_contents("config.json"), true);
$weatherKey = $data["weatherKey"];

$city = $_GET["city"]; 
$forecastURL = 'https://api.openweathermap.org/data/2.5/find?q='. $city .'&appid='. $weatherKey;
$currentURL = 'https://api.openweathermap.org/data/2.5/weather?q='. $city .'&appid='. $weatherKey;
// Read the JSON into a PHP objects 
$forecastData = json_decode(file_get_contents($forecastURL), true);
$currentData = json_decode(file_get_contents($currentURL), true);

//Convert temperature to celcius
$temp = round($currentData['main']['temp'] - 273.15);

//Generate dates
$date = strtotime(date("m/d"));
$dates = array();
for ($x = 0; $x <= 5; $x++) {
    $date = strtotime("+1 day", $date);
    $dates[] = date("d/m/Y", $date);
}

$forecastTemps = array();
for ($x = 0; $x <= 5; $x++) {
 // echo ($forecastData['list'][$x]['main']['temp']);
  $forecastTemps[] = round(($forecastData['list'][$x]['main']['temp_max'])  - 273.15);
}

print <<<EOT

<!DOCTYPE html>
<html>
<body>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
  }
td {
    text-align: center;
    }
</style>
<h3>Current weather</h3>
<img src="icons/{$currentData['weather'][0]['icon']}.png">
<br>
{$currentData['weather'][0]['main']}
{$temp}&#8451;

<h3>Forecast</h3>
<table style="width:50%">
  <tr>
    <th>{$dates[0]}</th>
    <th>{$dates[1]}</th>
    <th>{$dates[2]}</th>
    <th>{$dates[3]}</th>
    <th>{$dates[4]}</th>
  </tr>
  <tr>
    <td>{$forecastData['list'][0]['weather'][0]['main']}</td>
    <td>{$forecastData['list'][1]['weather'][0]['main']}</td>
    <td>{$forecastData['list'][2]['weather'][0]['main']}</td>
    <td>{$forecastData['list'][3]['weather'][0]['main']}</td>
    <td>{$forecastData['list'][4]['weather'][0]['main']}</td>
  </tr>
  <tr>
    <td><img src="icons/{$forecastData['list'][0]['weather'][0]['icon']}.png"></td>
    <td><img src="icons/{$forecastData['list'][1]['weather'][0]['icon']}.png"></td>
    <td><img src="icons/{$forecastData['list'][2]['weather'][0]['icon']}.png"></td>
    <td><img src="icons/{$forecastData['list'][3]['weather'][0]['icon']}.png"></td>
    <td><img src="icons/{$forecastData['list'][4]['weather'][0]['icon']}.png"></td>
  </tr>
  
</table>

</body>
</html>

EOT;

?>
