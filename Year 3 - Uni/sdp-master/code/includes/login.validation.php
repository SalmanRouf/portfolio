<?php
$mailuid_error = $pwd_error = "";
$mailuid = "";
$fail = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // We grab all the data which we passed from the signup form so we can use it later.
  $mailuid = $_POST['mailuid'];
  $password = $_POST['pwd'];

  if (empty($mailuid)) {
    $mailuid_error = "Email/Username is required";
  }

  if (empty($password)) {
    $pwd_error = "Password is required";
  }
  
  if ($mailuid_error == "" and $pwd_error == ""){
    $sql = "SELECT * FROM users WHERE uidUsers=? OR emailUsers=?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      header("Location: ../index.php");
      exit();
    }
    else {
      mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
      // Then we execute the prepared statement and send it to the database!
      mysqli_stmt_execute($stmt);
      // And we get the result from the statement.
      $result = mysqli_stmt_get_result($stmt);
      // Then we store the result into a variable.
      if ($row = mysqli_fetch_assoc($result)) {
        // Then we match the password from the database with the password the user submitted. The result is returned as a boolean.
        $pwd_check = password_verify($password, $row['pwdUsers']);
        // If they don't match then we create an error message!
        if ($pwd_check == false) {
          // If there is an error we send the user back to the login page.
          $fail = "Email/Username and Password don't match";
        }
        // Then if they DO match, then we know it is the correct user that is trying to log in!
        else if ($pwd_check == true) {
          $_SESSION['id'] = $row['idUsers'];
          $_SESSION['uid'] = $row['uidUsers'];
          $_SESSION['email'] = $row['emailUsers'];
          $_SESSION['avatar'] = $row['avatar'];
          // Now the user is registered as logged in and we can now take them back to the front page! :)
          header("Location: index.php");
          exit();
        }
      }
      else {
        $fail = "Invalid Email/Username";
      }
    }
  }
  // Then we close the prepared statement and the database connection!
  mysqli_stmt_close($stmt);
  mysqli_close($conn);
}
?>