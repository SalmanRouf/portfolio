<?php
#requiring the config file and function file to be included else the file will not run
require('../../lib/config.php');
require_once('../../lib/functions.php');
#running the live function from the function file
live($up=2);
#checking if core data files are missing 
if (!file_exists(CURRENCIES2)) {
	update();
}
#checking if the $cur value is empty, if so an error message is returned
if(!isset($_POST['codes'])){
    error_validation(2100);
    exit;
}
#checking if the $action value is empty, if so an error message is returned
if (!isset($_POST['action'])){
    error_validation(2000);
    exit;
}
#storing the post variables that were sent from the jquery on the index.php file under the form directory
$cur = $_POST['codes'];
$action = $_POST['action'];
#making the entered cur into uppercases - if the user changes at the front-end backend still formats correctly
$cur = strtoupper($cur);
#making the entered action into lowercases - if the user changes at the front-end backend still formats correctly
$action = strtolower($action);
#checking if the cur value is a value which fixer has no rates for, if so an error message is returned
if (in_array($cur, NA)) {
    error_validation(2300);
    exit;
}
#if value is already changed and is no longer a live value - error message will appear on the form - this is for the form to make sure that you can't continue to update the same code with the same action such as code - AUD being deleted 3 times in a row
if ($action == 'post' || $action == 'del'){
	if (!in_array($cur, LIVE)) {
		error_validation(2200);
		exit;
	}
}
#if value is already changed and is now a live value - error message will appear on the form - this is for the form to make sure that you can't continue to update the same code with the same action such as code - AUD being deleted 3 times in a row
if ($action == 'put'){
	if (!in_array($cur, INACTIVE)) {
        error_validation(2200);
		exit;
	}
}
#not allowing the base of GBP to be changed
if ($cur == 'GBP'){
    error_validation(2400);
    exit;
}
#action input function from the function file to print the xml, sending the cur and action variables
action_input($cur, $action);
?>