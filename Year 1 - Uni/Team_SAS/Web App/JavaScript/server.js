var express = require("express");
var conf = require('./config/dev.json')
var ws = require('./wslib');

// start a new instance of express  
var app = express();
exports.app = app;

//set options
var options = conf.options;
exports.options = options;

//set options
function path(p) {
    options.path = p;
    return options;
}

//exporting path object for testing
exports.path = path;

app.set('view engine', 'ejs');

// displays index.ejs
app.get("/page1", function (request, response) {
    response.render('index');
});
app.get("/display", function (request, response) {
    //get postcode from URL
    var postcode = request.query.postcode;
    //call placepoints API with postcode query
    ws.invoke(path("/api/2.0/static/placepoints?centerlat=51.454514&centerlng=%20-2.587910&radius=10000&name=" + postcode),
        (err, data) => {
            var lat = data.data.placePoints[0].lat; //assign current co-ordinates from placepoints API call to lat and lat variables
            var lng = data.data.placePoints[0].lng;
            if (err) response.status(404).send("Oops!");   //display error message
            else setTimeout(transitstops(lat, lng), 1000); //invoke transitstops function after 1 second delay
        });

    function transitstops(lat, lng) {
        return function () {
            //call transitstops API with lat and long from obtained from placepoints call
            ws.invoke(path("/api/2.0/static/transitstops?centerLat=" + lat + "&centerLng=" + lng + "&radius=10000&stopModes=14&maxResults=10"),
                (err, data) => {

                    data.lat = lat; //put current co-ordinates into data
                    data.lng = lng;
                    if (err) response.status(404).send("Oops!"); //display error message
                    else response.render('display', data);    //show data in browser, on display page
                });
        }
    }
});

app.get("/livedata", function (request, response) {
    //get primarycode from URL
    var primaryCode = request.query.primaryCode;
    //call status API with primarycode query
    ws.invoke(path("/api/2.0/rti/resources/status?stopIDs=" + primaryCode),
        (err, data) => {
            if (err) response.status(404).send("Oops!");   //display error message
            else response.render('status', data);   //show data in browser, on status page
        });
});

// Listen for requests 
if (process.env.NODE_ENV != 'test') {
    app.listen(8080);
    console.log("Listening on port 8080");
}