/*
This class calculates creates sets, adds games to them, and calculates the 
winning
*/
package TTAssociation;

public class Set {

private String homePlayer, awayPlayer;

    private Game [] games;
    
    private boolean doubles;

    Set() {
        games = new Game[3];
        for (int i = 0; i < 3; i++) {
            games[i] = new Game();
        }
        doubles = false;
    }

    public void setHomePlayer(String homePlayer) {
        this.homePlayer = homePlayer;
    }

    public void setAwayPlayer(String awayPlayer) {
        this.awayPlayer = awayPlayer;
    }

    public String getHomePlayer() {
        return homePlayer;
    }

    public String getAwayPlayer() {
        return awayPlayer;
    }
    
    public void setDoubles () {
        doubles = true;
    }

    public void setGameScores (int [] scores) {
        for (int i = 0 ; i < games.length; i++) {
            games[i].setScores(scores[i*2], scores [i*2+1]);               
        }
    }

    public String calculateWinner (int [] scores) {
        //Initialise or reset final set scores
        int homeScore = 0;
        int awayScore = 0;
        //Calculate each game winner
        for (int i = 0; i< scores.length; i+=2) {
            //Highest score wins
            if (scores[i] > scores[i+1]) {
                homeScore ++;
            } else {
                awayScore++;
            }           
        }
        if (homeScore > awayScore) {
            return "home";
        }
        else {
            return "away";
        }
    }
    
    public String getGameScore (int gameNumber) {
        return games[gameNumber].getGameScore();
    }

    public int getGameHomeScore (int gameNumber) {
        return games[gameNumber].getGameHomeScore();
    }

    public int getGameAwayScore (int gameNumber) {
        return games[gameNumber].getGameAwayScore();
    }

    @Override
    public String toString() {
        String set = null;
        if (doubles == true) {
            set = "Doubles:- ";
        }
        if (doubles == false) {
            set = homePlayer + " vs " + awayPlayer + ":- ";
        }
        for (int i = 0; i < games.length; i++) {
            set += games[i].getGameHomeScore() + ":" + 
                    games[i].getGameAwayScore(); 
            if (i<games.length-1) {
                set += ", ";
            }
        }       
        return set;
    }
}