<?php
$username_error = $email_error = $password_error = $repeatpassword_error = $avatar_error = "";
$username_check = $email_check = $password_check = $repeatpassword_check = $avatar_check = "";
$success = $fail = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //set all the post variables
  $username = $conn->real_escape_string($_POST['username']);
  $email = $conn->real_escape_string($_POST['email']);
  $password = ($_POST['password']); //md5 has password for security
  $password_repeat = ($_POST['confirmpassword']); //md5 has password for security
  $avatar_path = $conn->real_escape_string('avatars/'.$_FILES['avatar']['name']);


  // We check for an invalid username. In this case ONLY letters and numbers.
  if (empty($username)) {
    $username_error = "Username is required";
  } else {
    $username_check = test_input($username);
    if (!preg_match("/^[a-zA-Z0-9]*$/", $username_check)) {
      $username_error = "Username must only contain letters and numbers";
    }
  }

  if (empty($email)) {
    $email_error = "Email is required";
  } else {
    $email_check = test_input($email);
    // check if e-mail address is well-formed
    if (!filter_var($email_check, FILTER_VALIDATE_EMAIL)) {
      $email_error = "Invalid email format"; 
    }
  }

  if (empty($password)) {
    $password_error = "Password is required";
  } else {
    $password_check = test_input($password);
  }

  if (empty($password_repeat)) {
    $repeatpassword_error = "Password is required";
  } else {
    $repeatpassword_check  = test_input($password_repeat);
  }

  if ($password_check !== $repeatpassword_check) {
    $password_error  = "Passwords must match";
  }

  if (empty($avatar_path)) {
    $avatar_path = $conn->real_escape_string('avatars/ysera.png');
    $avatar_check  = test_input($avatar_path);
  } else {
    $avatar_check  = test_input($avatar_path);
    
    if (!preg_match("!image!",$_FILES['avatar']['type'])) {
      $avatar_error = "Please only upload GIF, JPG or PNG images";
    }

    if (!copy($_FILES['avatar']['tmp_name'], $avatar_check)){
      $avatar_error = "File upload failed!";
    }
  }
  

  if ($username_error == '' and $email_error == '' and $password_error == '' and $repeatpassword_error == '' and $avatar_error == '' ) {
    $sql = "SELECT uidUsers FROM users WHERE uidUsers=?;";
    // We create a prepared statement.
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
      // If there is an error we send the user back to the signup page.
      header("Location: signup.php");
      $fail = "Database not connected";
    }
    else{
      mysqli_stmt_bind_param($stmt, "s", $username_check);
      // Then we execute the prepared statement and send it to the database!
      mysqli_stmt_execute($stmt);
      // Then we store the result from the statement.
      mysqli_stmt_store_result($stmt);
      // Then we get the number of result we received from our statement. This tells us whether the username already exists or not!
      $resultCount = mysqli_stmt_num_rows($stmt);
      // Then we close the prepared statement!
    // mysqli_stmt_close($stmt);
      // Here we check if the username exists.
      if ($resultCount > 0) {
        $fail = "Username is already taken";
      } 
      else {
      //set session variables to display on welcome page
        $_SESSION['username'] = $username_check;
        $_SESSION['avatar'] = $avatar_check;

        $hashedPwd = password_hash($password_check, PASSWORD_DEFAULT);
        //insert user data into database
        $sql = "INSERT INTO users (uidUsers, emailUsers, pwdUsers, avatar) "
        . "VALUES ('$username_check', '$email_check', '$hashedPwd', '$avatar_check')";
        
        //check if mysql query is successful
        if ($conn->query($sql) === true){
          $_SESSION['message'] = "Registration Succesful! Welcome $username!";
          //redirect the user to index.php
          header("location: login.php");
        }
        else {
          $fail = "Registration Failed! Please try again";
        }
        $conn->close(); 
      }
    }
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>