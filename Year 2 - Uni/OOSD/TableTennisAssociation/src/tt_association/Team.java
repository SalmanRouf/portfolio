/*
This document controlls the elements of a team. 
 */
package tt_association;

import java.util.ArrayList;

/**
 * @author CP2/02 Team 3
 */
public class Team { // start of class Team

    private String teamName;
    private ArrayList<Player> teamPlayers;
    private ArrayList<String> playerNames;
    private int matchesPlayed, matchesWon, setsWon;

    public Team(String teamName) {
        this.teamName = teamName;
        teamPlayers = new ArrayList<Player>();
    }

    public String getTeamName() {
        return teamName;
    }

    public void addPlayer(Player player) {
        teamPlayers.add(player);
    }

    public ArrayList<Player> getTeamPlayers() {
        return teamPlayers;
    }

    public Player findPlayer(String name) {
        Player player = null;
        for (int i = 0; i < teamPlayers.size(); i++) {
            if (teamPlayers.get(i).getName().equals(name)) {
                player = teamPlayers.get(i);
                break;
            }
        }
        return player;
    }

    public void resetStats() {
        matchesPlayed = 0;
        matchesWon = 0;
        setsWon = 0;
    }

    public void incrementMatchesPlayed() {
        matchesPlayed++;
    }

    public void incrementMatchesWon() {
        matchesWon++;
    }

    public void incrementSetsWon(int i) {
        setsWon += i;
    }

    public int getMatchesPlayed() {
        return matchesPlayed;
    }

    public int getMatchesWon() {
        return matchesWon;
    }

    public int getSetsWon() {
        return setsWon;
    }

    public ArrayList<String> getPlayerNames() {
        playerNames = new ArrayList<>();
        for (int i = 0; i < teamPlayers.size(); i++) {
            playerNames.add(teamPlayers.get(i).getName());
        }
        return playerNames;
    }
}// end of class Team 
