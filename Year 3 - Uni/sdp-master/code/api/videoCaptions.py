########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'x-ms-client-request-id': '',
    'Content-Type': 'multipart/form-data',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    # Request parameters
    'language': 'English',
    'accessToken': 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJWaWRlb0lkIjoiNWRmMDBjMDI1YiIsIkFsbG93RWRpdCI6IlRydWUiLCJFeHRlcm5hbFVzZXJJZCI6ImEyMzdlNDIxYjdiNDVhMjIiLCJVc2VyVHlwZSI6Ik1pY3Jvc29mdCIsIklzc3VlckxvY2F0aW9uIjoiVHJpYWwiLCJuYmYiOjE1OTAxMzkxMDksImV4cCI6MTU5MDE0MzAwOSwiaXNzIjoiaHR0cHM6Ly9hcGkudmlkZW9pbmRleGVyLmFpLyIsImF1ZCI6Imh0dHBzOi8vYXBpLnZpZGVvaW5kZXhlci5haS8ifQ.HpRAAloEhSu7qqOEs3YWzHHNZrjkRAFOs7keaBrrkVs',
})

try:
    conn = http.client.HTTPSConnection('api.videoindexer.ai')
    conn.request("GET", "/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos/5df00c025b/Captions?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
