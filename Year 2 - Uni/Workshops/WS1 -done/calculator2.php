<?php
/* ======================================================
PHP Calculator example using "sticky" form (Version 1)
======================================================
Author : P Chatterjee (adopted from an original example written by C J Wallace)
Purpose : To multiply 2 numbers passed from a HTML form and display the result.
input:
x, y : numbers
calc : Calculate button pressed
Date: 15 Oct 2007
*/
// grab the form values from $_HTTP_POST_VARS hash
extract($_GET);
?>
<html>
  <head>
    <title>PHP Calculator Example</title>
  </head>
  <body>
    <h3>PHP Calculator (Version 1)</h3>
    <p>Calculate two numbers and output the result</p>
    <form method="get" action="<?php print $_SERVER['PHP_SELF']; ?>">
      x = <input type="text" name="x" size="5"  placeholder="number1"/>
      y =  <input type="text" name="y" size="5"  placeholder = "number2  " />
      
      <!-- Drop down options for operators-->
      op = <select name = "operator">
        <option value = "+"> Add</option>
        <option value = "-"> Subtract</option>
        <option value = "*"> Multiply</option>
        <option value = "/"> Divide</option>
      </select>
      
      <input type="submit" name="submit" value="Calculate"/>
      <input type="reset" name="clear" value="Clear" />
    </form>
    <!-- print the result -->
    <?php
    if(isset($_GET['submit']))
      {
        //fetching the result from user input data
        $x = $_GET['x'];
        $y = $_GET['y'];
        $op = $_GET['operator'];
      
      //creating a calculation function with 3 parameter 
      function calculation($num1,$num2,$operator){
         switch($operator){
          case "+":
             $result = $num1 + $num2; 
             break;
          case "-":
             $result = $num1 - $num2; 
             break;
          case "*":
            $result = $num1 * $num2; 
             break;
          case "/":
            if($num2 == 0)
            {
              $result = "nope";  
            }
             else $result = $num1 / $num2; 
             break;
          }
          return $result; 
      }
      //calling the calculation function 
      echo "$x ".$op." $y = ".calculation($x,$y,$operator); 

    }
      
    
    
    ?>
  </body>
</html>