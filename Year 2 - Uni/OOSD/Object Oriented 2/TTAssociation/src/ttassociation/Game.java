/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;

/**
 *
 * @author s2-rouf
 */
public class Game {

	private int homeScore, awayScore;
        
	public Game() {
	}
        
        public void setScores (int homeScore, int awayScore) {
            this.homeScore = homeScore;
            this.awayScore = awayScore;
        }
        
        public int getHomeScore () {
            return homeScore;
        }
                
        public int getAwayScore () {
            return awayScore;
        }
        
        @Override
        public String toString() {
            String score = homeScore + ":" + awayScore; 
            return score;
        }

}



