########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'x-ms-client-request-id': '',
    'Content-Type': 'multipart/form-data',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    'privacy': 'Private',
    'language': 'English',
    'videoUrl': 'Joker.mp4',
    'accessToken': 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJBbGxvd0VkaXQiOiJUcnVlIiwiRXh0ZXJuYWxVc2VySWQiOiJhMjM3ZTQyMWI3YjQ1YTIyIiwiVXNlclR5cGUiOiJNaWNyb3NvZnQiLCJuYmYiOjE1ODQ4NTE3NDUsImV4cCI6MTU4NDg1NTY0NSwiaXNzIjoiaHR0cHM6Ly93d3cudmlkZW9pbmRleGVyLmFpLyIsImF1ZCI6Imh0dHBzOi8vd3d3LnZpZGVvaW5kZXhlci5haS8ifQ.47bi-Vrr_F1gH86WPOblFMhQmwUGeNgo3UCQalDBmJ0',
})

try:
    conn = http.client.HTTPSConnection('api.videoindexer.ai')
    conn.request("POST", "/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos?name=chri&%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
