import csv 
import sys #allows me to exit the program without having to ask the user twice

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue, itemShape, itemWeight):
        self.itemNumber = int (itemNumber)
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)
        self.itemShape = itemShape
        self.itemWeight = int(itemWeight)



class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    #add items function
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2], row[3], row[4])
            self.items.append(item)

    #appending the items i'm adding into one of the warehouses
    def addSomething(self, item):
        self.items.append(item)
        
    #works out the total value of one warehouse with the items we might be adding.
    def totalValue(self):
        totalValue = 0
        for i in self.items:
            totalValue = totalValue + i.itemValue
        return totalValue

    #remove function
    def removeItem(self, item):
        self.items.remove(item)

    def get_value(self):
        return self.item_value



def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        print ("success")
        return warehouse

#Reading CSV file from each warehouse
A = readingCSV(r'H:\DADSA\Warehouse A.csv', 'A')
B = readingCSV(r'H:\DADSA\Warehouse B.csv', 'B')
C = readingCSV(r'H:\DADSA\Warehouse C.csv', 'C')
D = readingCSV(r'H:\DADSA\Warehouse D.csv', 'D')


while True: 
    print("Warehouse Menu\n1.View Warehouse A\n2.View Warehouse B\n3.View Warehouse C\n4.View Warehouse D")
    selection = input("Please Select A, B, C or D: ")
    if selection == 'A':
        for item in A.items:
            print(item.itemNumber, item.itemDescription, item.itemValue, item.itemShape, item.itemWeight)
    elif selection == 'B':
        for item in B.items:
            print(item.itemNumber, item.itemDescription, item.itemValue, item.itemShape, item.itemWeight)
    elif selection == 'C':
        for item in C.items:
            print(item.itemNumber, item.itemDescription, item.itemValue, item.itemShape, item.itemWeight)
    elif selection == 'D':
        for item in D.items:
            print(item.itemNumber, item.itemDescription, item.itemValue, item.itemShape, item.itemWeight)
    else:
        print("Error: Unknown Option Selected(Must Select 1, 2 3 or 4)")

