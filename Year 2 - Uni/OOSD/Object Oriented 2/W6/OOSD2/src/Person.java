/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s2-rouf
 */
public class Person {
    private String firstName;
    private String lastName;
    private String address;

    public Person() {
    }

    public Person(String firstName, String lastName, String address)    
   {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
                return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
    @Override
    public String toString() {
        return "Person's name is " + getFirstName() +" "+ getLastName() +"\n Address is "+ getAddress();
    }
    
    public static void main(String args[]) 
   { 
       
       Person p = new Person();
       System.out.println(p);
       p.setFirstName("James");
       p.setLastName("Lear");
       p.setAddress("FET-CSCT");
       System.out.println(p);
       Person p1 = new Person("Kun", "Wei", "FET-CSCT");
       System.out.println(p1);
       
   }
}

    
