<?php
  // First we start a session which allow for us to store information as SESSION variables.
  session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="main.css" type="text/css" rel="stylesheet">
  </head>
  <body>

    <!-- Here is the header where I decided to include the login form for this tutorial. -->
    <header>
    <?php
          if (!isset($_SESSION['id'])) {
            echo '  <div class="burger">
                        <div class="line1"></div>
                        <div class="line2"></div>
                        <div class="line3"></div>
                    </div>
                    <a href="#" class="logo">Salman Rouf</a>
                    <nav class="main-nav"> 
                        <ul class="nav-links">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="league.php">Upload</a></li>
                            <li><a href="login.php">Link 2</a></li>
                            <li><a href="signup.php">Link 3</a></li>
                        </ul>
                    </nav>
            
                  <script src="app.js"></script>';
          }
          else if (isset($_SESSION['id'])) {
            echo '
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
            <a href="#" class="logo">TRX</a>
            <nav class="main-nav"> 
                <ul class="nav-links">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="league.php">Leagues</a></li>
                  <li><a href="login.php">Login</a></li>
                  <li><a href="signup.php">Sign Up</a></li>
                  <li><a href="createteam.php">Create a team</a></li>
                </ul>
                <div class="dropdown">
                <img src="'.$_SESSION["avatar"].'" onclick="myFunction()" class="dropbtn" alt="Avatar" />
                    <div id="myDropdown" class="dropdown-content">
                    <a href="profile.php">Profile</a>
                    <a href="myteam.php">My Teams</a>
                    <a href="personalsettings.php">Personal Details</a>
                    <a href="accountsconnected.php">Accounts Connected</a>
                    <a href="settings.php">Settings</a>
                    <a href="rules.php">Rules</a>
                    <form action="includes/logout.inc.php" method="post">
                        <button type="submit" name="login-submit">Logout</button>
                    </form>
                    </div>
                </div>

                <script> 
                /* When the user clicks on the button, 
                toggle between hiding and showing the dropdown content */
                function myFunction() {
                    document.getElementById("myDropdown").classList.toggle("show");
                }
                
                // Close the dropdown if the user clicks outside of it
                window.onclick = function(event) {
                    if (!event.target.matches(".dropbtn")) {
                    var dropdowns = document.getElementsByClassName("dropdown-content");
                    var i;
                    for (i = 0; i < dropdowns.length; i++) {
                        var openDropdown = dropdowns[i];
                        if (openDropdown.classList.contains("show")) {
                        openDropdown.classList.remove("show");
                        }
                    }
                    }
                }
                
                </script>
            </nav>
            
            <div class="clearfix"></div>

            <script src="app.js"></script>'; 
              }
      ?>
    </header>
  </body>
</html>


