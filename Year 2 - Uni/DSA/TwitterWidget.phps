<?php 
ini_set('display_errors', 1); 
require_once('TwitterAPIExchange.php'); 
require 'connect.php';  
header('Content-Type: text/html; charset="ISO-8859-15"'); 
//Start a session
session_start();

//Get required tag from URL
if (isset($_GET['tag'])) {
    $tag = $_GET['tag'];
}
else {
    die('Error, tag not found');
}
//Insert user comment into database
if (isset($_POST['submit'])) {
    $username = $_POST['username'];
    $comment = $_POST['comment'];
    //Generate random number for ID 
    $id = rand(100000000,999999999);
    $likes = 5; //Give some likes to prioritise over low quality tweets later
    $sql = "INSERT INTO Comments (ID, Username, Comment, Tag, Likes, Created)
     VALUES ('".$id."','".$username."','".$comment."','".$tag."','".$likes."',NOW())"; 
    $db->query($sql);
    //Add comment to session comments array
    if (isset($_SESSION[$tag])) {
        $_SESSION[$tag][] = array("Username" => $username, "Comment" => $comment);;
    }
}

/* Use session comments if present */
/* Faster loading and reduces API calls */
if (isset($_SESSION[$tag])) {
    displayTweets($_SESSION[$tag]);
}
//Otherwise get comments from Twitter API or database 
else {  
    //Construct search terms for desired tag from library for Twitter API call
    $lib = require_once('twitterlib.php');
    $getfield = '?q=';
    foreach($lib[$tag]['SearchTerms'] as $q) {
        $getfield .= $q;
        if ($q != end($lib[$tag]['SearchTerms'])) { // Don't include +OR+ on final iteration
            $getfield .= '+OR+';
        }  
    }
    /** Add tag coordinates and range to search query */
    /** Include only English tweets and filter any offensive content */
    $getfield .=
    '&geocode='.$lib[$tag]['Coordinates'].',50km&result_type=recent&lang=en&count=50+filter:safe';

    //Check for recent tweets first, before topping up with database comments

    
    
    //Get Twitter API keys from config file
    $keys = json_decode(file_get_contents("config.json"), true);
    $twitterKeys = array('oauth_access_token' => $keys["twitterKeys"]['oauth_access_token'], 
                        'oauth_access_token_secret' => $keys["twitterKeys"]['oauth_access_token_secret'], 
                        'consumer_key' => $keys["twitterKeys"]['consumer_key'], 
                        'consumer_secret' => $keys["twitterKeys"]['consumer_secret'] 
                        );
    

    //Start of code by James Mallison, see https://github.com/J7mbo/twitter-api-php 
    
    /** Perform a GET request and echo the response **/ 
    /** Note: Set the GET field BEFORE calling buildOauth(); **/ 
    $url = 'https://api.twitter.com/1.1/search/tweets.json'; 
    $requestMethod = 'GET'; 
    $twitter = new TwitterAPIExchange($twitterKeys); 
    $data=$twitter->setGetfield($getfield) 
                ->buildOauth($url, $requestMethod) 
                ->performRequest(); 
    
    // Read the JSON into a PHP object 
    $phpdata = json_decode($data, true); 
    //James Mallison based code ends 


    /** Array for tracking already added tweet IDs returned from the API search*/
    /** Used for querying the database to topup later */
    $added = array();
    /** Array for comments to be displayed */
    $comments = array();
    //Insert each tweet into the database
    foreach ($phpdata["statuses"] as $status){
        //Ensure special characters can be inserted
        $tweet = mysqli_real_escape_string($db, $status["text"]);
        //Insert tweet data including number of likes
        $sql = "INSERT IGNORE INTO Comments (ID, Username, Comment, Tag, Likes, Created)
                VALUES ('".$status["id_str"]."','".$status["user"]["name"]."',
                '".$tweet."','$tag','".$status["favorite_count"]."',NOW())"; 
        $db->query($sql);
        //Add to Tweets array and keep record of Tweet ID
        $comments[] = array("Username" => $status["user"]["name"], "Comment" => $status["text"]);
        $added[] = $status["id_str"];
    }
       

    /**  Top up with comments from the database if the API search returns less than 40 results, **/
    /**  exclude already added comments and tweets with under 5 likes **/
    if (count($phpdata["statuses"]) < 40) {
        $needed = 40-(count($phpdata["statuses"]));
        $topups = array();
        //Exlude already added Tweets and filter from query if no Tweets returned
        if (empty($added)) {
            $results = $db->query("SELECT * FROM Comments WHERE Tag =
            '$tag' AND Likes > 4 ORDER BY `Created` ASC LIMIT $needed");
        }
        else {
            $results = $db->query("SELECT * FROM Comments WHERE Tag =
            '$tag' AND ID NOT IN (".implode(",",$added).") AND Likes > 4 
            ORDER BY `Created` ASC LIMIT $needed");
        }
        //Add query results to comments array
        if($results->num_rows) {
            while($row = $results->fetch_object()) {
                $comments[] = array("Username" => $row->Username, "Comment" => $row->Comment);
            }
            $results->free();
        }
    };
    // Store tweets in session variable and display tweets
    $_SESSION[$tag] = $comments;
    displayTweets($comments);
}

function displayTweets (array $comments) {
    echo ("<body style='background-color:#38A1F3; color:white;'>");
    //Display tweets and usernames
    foreach($comments as $c) {
        echo("<p>" . $c["Username"] . "</p>");
        echo("<p>" . $c["Comment"] . "</p>");
        echo("<hr>");
    }
}
//Display user comment form
?> 
<!DOCTYPE html>
<html>
<style>
    input[type=submit]:hover {
    background-color: #0084b4;
}
</style>
<form action="<?php print $_SERVER['PHP_SELF']."?tag=". $tag; ?>" method="POST">
  Name:<br>
  <input type="text" name="username" required><br>
  Comment:<br>
  <textarea type="text" name="comment" required></textarea>
  <input type="submit" name="submit" value="Submit">
</form>

<script>
    //Scroll to bottom of page on load
    var scrollingElement = (document.scrollingElement || document.body);
    scrollingElement.scrollTop = scrollingElement.scrollHeight;
</script>
  
</html>

