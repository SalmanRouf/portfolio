<?php
#adding the header path
require "header.php";
#path to include the validation php file for the sign up
include('includes/signup.validation.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/placeholders.css" type="text/css">
  </head>
  <body>
    <div class="box">
      <h2>Sign Up</h2>
      <form class="form" action="signup.php" method="post" enctype="multipart/form-data" autocomplete="off">
        <div class="success"><?= $success ?></div>
        <div class="error"><?= $fail ?></div>
        <div>
          <span class="error"><?= $username_error ?></span>
          <input type="text" name="username" value="<?= $username_check ?>" required/>
          <label>Username</label>
        </div>
        <div>
          <span class="error"><?= $email_error ?></span>
          <input type="text" name="email" value="<?= $email_check ?>" required/>
          <label>Email</label>
        </div> 
        <div>
          <span class="error"><?= $password_error ?></span>
          <input type="password" name="password" autocomplete="new-password" required/>
          <label>Password</label>
        </div>
        <div>
          <span class="error"><?= $repeatpassword_error ?></span>
          <input type="password" name="confirmpassword" autocomplete="new-password" required/>
          <label>Repeat Password</label>
        </div>
        <div>
          <span class="error"><?= $avatar_error ?></span>
          <div class="avatar"><span>Select your avatar: </span><input type="file" name="avatar" accept="image/*" /></div>
        </div>
        <input type="submit" class="submit" name="register" value="Register">
      </form>
    </div>
  </body>
</html>

