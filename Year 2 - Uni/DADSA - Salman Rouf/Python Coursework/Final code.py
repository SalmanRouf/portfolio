#TODO
#allow movement


# importing csv
import csv

# Warehouses
warehouse_file = []
warehouse_limit = 2000000000


class Warehouse:
    def __init__(self, name) :
        self.items = []
        self.name = name


    # add item function
    def add_items(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Item(row[0], row[1], row[2])
            self.items.append(item)

    def addItem(self, item):
        self.items.append(item)

    # get items
    def get_items(self):
        return self.items

    # get total
    def get_total(self):
        total = 0
        for item in self.items:
            total += item.get_value()

        return total

    # removes item
    def remove_item(self, item):
        self.items.remove(item)

class Item:
    def __init__(self, item_number, item_description, value):
        self.item_number = int(item_number)
        self.item_description = item_description
        self.item_value = int(value)

    def get_value(self):
        return self.item_value


def select_warehouse():
    # asks for warehouse
    warehouse = str(input("\nPlease select a warehouse to view:"))
    # opens appropriate warehouse
    if warehouse.capitalize() == "A":
        for item in A.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse A selected")
        print("Warehouse Total: £")
        print(Warehouse.get_total(A))
    elif warehouse.capitalize() == "B":
        for item in B.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse B selected")
        print("Warehouse Total: £")
        print(Warehouse.get_total(B))
    elif warehouse.capitalize() == "C":
        for item in C.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse C selected")
        print("Warehouse Total: £")
        print(Warehouse.get_total(C))
    elif warehouse.capitalize() == "D":
        for item in D.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse D selected")
        print("Warehouse Total: £")
        print(Warehouse.get_total(D))
    else:
        print("This is not a warehouse")

                             
def enter_details():
    # asks for input of item no
    item_number = int(input("\nPlease enter the items number:"))

    # asks for user to input item description
    item_description = str(input("\nPlease enter a description of the item:"))

    # asks for user to input item price
    item_value = int(input("\nPlease enter the items price: £"))
    # if value exceeds 2 Billion asks once again
    while warehouse_limit < item_value:
        print("Number exceeded 2 billion as a result value was rejected")
        item_value = int(input("\nPlease enter the items price: £"))      

    new_item = Item(item_number, item_description, item_value)
    # asks for warehouse
    warehouse = str(input("\nPlease select a warehouse:"))
    if warehouse.capitalize() == "A":
        # adds new item using user input data to A
        A.addItem(new_item)
        for item in A.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse A selected")
        # shows warehouse total with new item
        print("Warehouse Total (in Pounds): ")
        print(Warehouse.get_total(A))
        # if the warehouse now exceeds limit new value will be removed
        if Warehouse.get_total(A) > warehouse_limit:
            print("Warehouse exceeds limit by (in Pounds)")
            print(Warehouse.get_total(A) - warehouse_limit)
            # a menu opens asking to make room for new item
            move_list()
            # removes too large item after moving around is complete 
            Warehouse.remove_item(A, new_item)
    elif warehouse.capitalize() == "B":
        # adds new item using user input data to B
        B.addItem(new_item)
        for item in B.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse B selected")
        # shows warehouse total with new item
        print("Warehouse Total: £")
        print(Warehouse.get_total(B))
        # if the warehouse now exceeds limit new value will be removed
        if Warehouse.get_total(B) > warehouse_limit:
            print("Warehouse exceeds limit by (in Pounds)")
            print(Warehouse.get_total(B) - warehouse_limit)
            # a menu opens asking to make room for new item
            move_list()
            # removes too large item after moving around is complet
            Warehouse.remove_item(B, new_item)
    elif warehouse.capitalize() == "C":
        # adds new item using user input data to C
        C.addItem(new_item)
        for item in C.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse C selected")
        # shows warehouse total with new item
        print("Warehouse Total: £")
        print(Warehouse.get_total(C))
        # if the warehouse now exceeds limit new value will be removed
        if Warehouse.get_total(C) > warehouse_limit:
            print("Warehouse exceeds limit by (in Pounds)")
            print(Warehouse.get_total(C) - warehouse_limit)
            # a menu opens asking to make room for new item
            move_list()
            # removes too large item after moving around is complet
            Warehouse.remove_item(C, new_item)
    elif warehouse.capitalize() == "D":
        # adds new item using user input data to D
        D.addItem(new_item)
        for item in D.items:
            print(item.item_number, item.item_description, item.item_value)
        print("Warehouse D selected")
        # shows warehouse total with new item
        print("Warehouse Total: £")
        print(Warehouse.get_total(D))
        # if the warehouse now exceeds limit new value will be removed
        if Warehouse.get_total(D) > warehouse_limit:
            print("Warehouse exceeds limit by (in Pounds)")
            print(Warehouse.get_total(D) - warehouse_limit)
            # a menu opens asking to make room for new item
            move_list()
            # removes too large item after moving around is complet
            Warehouse.remove_item(D, new_item)
    else:
        print("This is not a warehouse")

# 4.removing item
def remove_item():
    #selects warehouse to show user items to be removed
    select_warehouse()
    #asks for input of warehouse again so that if function can be carried out
    warehouse = str(input("Input warehouse again please:"))
     
    if warehouse.capitalize() == "A":
        #asks for user to input selected items number
        item_number = int(input("\nPlease enter the items number:"))
        #removes selected item
        for item in A.items:
            if item_number == item.item_number:
                A.remove_item(item)

    elif warehouse.capitalize() == "B":
         #asks for user to input selected items number
        item_number = int(input("\nPlease enter the items number:"))
        #removes selected item
        for item in B.items:
            if item_number == item.item_number:
                B.remove_item(item)

    elif warehouse.capitalize() == "C":
         #asks for user to input selected items number
        item_number = int(input("\nPlease enter the items number:"))
        #removes selected item
        for item in C.items:
            if item_number == item.item_number:
                C.remove_item(item)

    elif warehouse.capitalize() == "D":
         #asks for user to input selected items number
        item_number = int(input("\nPlease enter the items number:"))
        #removes selected item
        for item in D.items:
            if item_number == item.item_number:
                D.remove_item(item)
    else:
        print("This is not a warehouse")        

#3. moving items from one warehouse to another
def move_item():
       #removes item then adds it to another warehouse

        select_warehouse()
        warehouse = str(input("Input warehouse again please:"))
        #removes from warehouse A
        if warehouse.capitalize() == "A":

            item_number = int(input("\nPlease enter the items number:"))
            for item in A.items:
                if item_number == item.item_number:
                    A.remove_item(item)
        #removes from warehouse B
        elif warehouse.capitalize() == "B":
    
            item_number = int(input("\nPlease enter the items number:"))
            for item in B.items:
                if item_number == item.item_number:
                    B.remove_item(item)
        #removes from warehouse A
        elif warehouse.capitalize() == "C":

            item_number = int(input("\nPlease enter the items number:"))
            for item in C.items:
                if item_number == item.item_number:
                    C.remove_item(item)
        #removes from warehouse D
        elif warehouse.capitalize() == "D":

            item_number = int(input("\nPlease enter the items number:"))
            for item in D.items:
                if item_number == item.item_number:
                    D.remove_item(item)
        else:
            print("This is not a warehouse")     


        item_description = str(input("\nPlease enter a description of the item:"))
        
        item_value = int(input("\nPlease enter the items price: £"))
        
        move_warehouse = str(input("\nPlease select warehouse to move to:"))

        new_item = Item(item_number, item_description, item_value)
        
        if move_warehouse.capitalize() == "B":
            if(item_value > warehouse_limit - Warehouse.get_total(B) ):
                print("Not enough room")
            elif(item_value < warehouse_limit - Warehouse.get_total(B) ):
                print("Adding to warehouse")
                B.addItem(new_item)
        elif move_warehouse.capitalize() == "C":
            if(item_value > warehouse_limit - Warehouse.get_total(C) ):
                print("Not enough room")
            elif(item_value < warehouse_limit - Warehouse.get_total(C) ):
                print("Adding to warehouse")
                C.addItem(new_item)
        elif move_warehouse.capitalize() == "D":
            if(item_value > warehouse_limit - Warehouse.get_total(D) ):
                print("Not enough room")
            elif(item_value < warehouse_limit - Warehouse.get_total(D) ):
                print("Adding to warehouse")
                D.addItem(new_item)
        elif move_warehouse.capitalize() == "A":
            if(item_value > warehouse_limit - Warehouse.get_total(A) ):
                print("Not enough room")
            elif(item_value < warehouse_limit - Warehouse.get_total(A) ):
               print("Adding to warehouse")
               A.addItem(new_item)
        else:
            print("invalid warehouse")


def move_list():
    while True:
        print("Would you like to make room for this item?\n1.Yes\n2.No")
        selection = input("\nPlease select:")
        if selection == "1":
            move_item()
            enter_details()
            break
        elif selection == "2":
            print("returning to Menu")
            break
            

def read_csv(file, name):
    with open(file) as file:
        reader = csv.reader(file)
        warehouse = Warehouse(name)
        warehouse.add_items(reader)
        return warehouse
    

A = read_csv("DADSA Assignment 2018-19 Warehouse A.csv", "A")
B = read_csv("DADSA Assignment 2018-19 Warehouse B.csv", "B")
C = read_csv("DADSA Assignment 2018-19 Warehouse C.csv", "C")
D = read_csv("DADSA Assignment 2018-19 Warehouse D.csv", "D")

while True:
    print("Warehouse Menu\n1.Add Item\n2.View Details\n3.Move Item\n4.Remove Item\n5.Exit")
    selection = input("\nPlease select:")
    if selection == "1":
        print("ADD ITEM")
        enter_details()
    elif selection == "2":
        print("VIEW WAREHOUSE DETAILS")
        select_warehouse()
    elif selection == "3":
        print("MOVE ITEM")
        move_item()
    elif selection == "4":
        print("REMOVE ITEM")
        remove_item()
    elif selection == "5":
        print("GOODBYE")
        selection = None
        break
    else:
        print("Enter a valid value")
