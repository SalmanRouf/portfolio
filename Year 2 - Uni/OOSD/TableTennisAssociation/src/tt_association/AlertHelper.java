/*
This class helps to display alert to the user. 
 */
package tt_association;

import javafx.scene.control.Alert;
import javafx.stage.Window;

/**
 *
 * @author CP2/02 Team 3
 */
public class AlertHelper { // start of class AlertHelper

    //a method to display alerts 
    public static void showAlert(Alert.AlertType alertType,
            Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

}// end of class AlertHelper
