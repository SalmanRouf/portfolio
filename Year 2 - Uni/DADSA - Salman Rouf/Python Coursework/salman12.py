import csv
import sys


class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = itemNumber
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)


class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)
        # print(item)

    def addSomething(self, item):
        self.items.append(item)

    def totalValue(self):
        totalValue = 0
        for i in self.items:
            totalValue = totalValue + i.itemValue
        return totalValue

    def removeItem(self, item):
        self.items.remove(item)


class Function:
    def addFunction(functions):
        # Asking the use to enter the item number
        itemNumber = int(input('Please enter the item number: '))

        # Getting the item descrpition
        itemDescription = str(input('Please enter the item description: '))

        # Item value entered & checked the value to make sure it doesn't exceed the limit.
        itemValue = int(input('Please enter the item value:£ '))

        while itemValue > capacity:
            print('The item value has been rejected as the value has exceeded the limit of £2 billion')
            itemValue = int(input('Please enter the item value:£ '))

        newItem = Items(itemNumber, itemDescription, itemValue)

        # Asking the user to pick a warehouse to place the item within
        while True:
            four = str(input("Please select a warehouse to place the item within A, B, C or D: "))
            if four == 'A' or four == 'a':
                A.addSomething(newItem)
                # itemSpace = i
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print("The total value for warehouse A is", A.totalValue())
                if Warehouse.totalValue(A) > capacity:
                    print("Warehouse exceeds limit by (in Pounds)")
                    print(Warehouse.totalValue(A) - capacity)
                    move_list()
                    Warehouse.removeItem(A, newItem)
                break
            elif four == 'B' or four == 'b':
                B.addSomething(newItem)
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(B.totalValue())
                if Warehouse.totalValue(B) > capacity:
                    print("Warehouse exceeds limit by (in Pounds)")
                    print(Warehouse.totalValue(B) - capacity)
                    move_list()
                    Warehouse.removeItem(B, newItem)
                break
            elif four == 'C' or four == 'c':
                C.addSomething(newItem)
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(C.totalValue())
                if Warehouse.totalValue(C) > capacity:
                    print("Warehouse exceeds limit by (in Pounds)")
                    print(Warehouse.totalValue(C) - capacity)
                    move_list()
                    Warehouse.removeItem(C, newItem)
                break
            elif four == 'D' or four == 'd':
                D.addSomething(newItem)
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                print(D.totalValue())
                if Warehouse.totalValue(D) > capacity:
                    print("Warehouse exceeds limit by (in Pounds)")
                    print(Warehouse.totalValue(D) - capacity)
                    move_list()
                    Warehouse.removeItem(D, newItem)
                break
            else:
                print("Unknown Variable Selected")

        while True:
            option = str(input("Would you like to add to another warehouse? [y/n] "))
            if option == 'y' or option == 'Y':
                first.addFunction()
            elif option == 'n' or option == 'N':
                print("Goodbye")
                sys.exit()
            else:
                print("Unknown Option Selected. Select y or n")


class Function2(Function):
    def viewSummaryFunction(functions):
        # Asking the user to pick a warehouse to place the item within
        while True:
            choice = input("Select to view warehouse A, B, C or D: ")
            if choice == 'A' or choice == 'a':
                for item in A.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'B' or choice == 'b':
                for item in B.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'C' or choice == 'c':
                for item in C.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            elif choice == 'D' or choice == 'd':
                for item in D.items:
                    print(item.itemNumber, item.itemDescription, item.itemValue)
                break
            else:
                print("Error: Unknown Variable Selected\nMake sure that you are selecting A, B, C or D")


def movingItems():
    # removes item then adds it to another warehouse

    second.viewSummaryFunction()
    mate = str(input("Input warehouse again please:"))
   # removes from warehouse A
    if mate == "A":
        itemValue = int(input("\nPlease enter the item value: "))
        for item in A.items:
            if itemValue == item.itemValue:
                A.removeItem(item)

    # removes from warehouse B
    elif mate == "B":
        itemValue = int(input("\nPlease enter the items value:"))
        for item in B.items:
            if itemValue == item.itemValue:
                B.removeItem(item)

    # removes from warehouse A
    elif mate == "C":
        itemValue = int(input("\nPlease enter the items value:"))
        for item in C.items:
            if  itemValue == item.itemValue:
                C.removeItem(item)

    # removes from warehouse D
    elif mate == "D":
        itemValue = int(input("\nPlease enter the items value:"))
        for item in D.items:
            if itemValue == item.itemValue:
                D.removeItem(item)
    else:
        print("This is not a warehouse")

    #itemDescription = str(input("\nPlease enter a description of the item:"))

  #  itemValue = int(input("\nPlease enter the items price: £"))

    move_warehouse = str(input("\nPlease select warehouse to move to:"))

    #newItem = Items(itemValue)

    if move_warehouse.capitalize() == "B":
        if(itemValue > capacity - Warehouse.totalValue(B)):
            print("Not enough room")
        elif (itemValue < capacity - Warehouse.totalValue(B)):
            print("Adding to warehouse")
            B.addSomething(itemValue)
    elif move_warehouse.capitalize() == "C":
        if (itemValue > capacity - Warehouse.totalValue(C)):
            print("Not enough room")
        elif (itemValue < capacity - Warehouse.totalValue(C)):
            print("Adding to warehouse")
            C.addSomething(itemValue)
    elif move_warehouse.capitalize() == "D":
        if (itemValue > capacity - Warehouse.totalValue(D)):
            print("Not enough room")
        elif (itemValue < capacity - Warehouse.totalValue(D)):
            print("Adding to warehouse")
            D.addSomething(itemValue)
    elif move_warehouse.capitalize() == "A":
        if (itemValue > capacity - Warehouse.totalValue(A)):
            print("Not enough room")
        elif (itemValue < capacity - Warehouse.totalValue(A)):
            print("Adding to warehouse")
            A.addSomething(itemValue)
    else:
        print("invalid warehouse")


def move_list():
    while True:
        print("Would you like to make room for this item?\n1.Yes\n2.No")
        selection = input("\nPlease select:")
        if selection == "1":
            movingItems()
            first.addFunction()
            break
        elif selection == "2":
            print("returning to Menu")
            break


def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse


# def firstFit:
#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')


# Declaring objects
first = Function()
second = Function2()
capacity = 2000000000

#print(A.sorted())
# Asking the user to pick a warehouse to place the item within
while True:
    print("Warehouse Menu\n1.Add Item\n2.View Warehouse\n3.Move Items\n4.Exit")
    selection = input("Please Select 1, 2, 3 or 4: ")
    if selection == '1':
        print("You Can Now Add The Item")
        first.addFunction()
    elif selection == '2':
        print("You Can Now View A Warehouse")
        second.viewSummaryFunction()
    elif selection == '3':
        print("You Can Now Move An Item")
        movingItems()
    elif selection == '4':
        print("Thank You\nGoodbye")
        sys.exit()
    else:
        print("Unknown Option Selected(Please Select 1, 2 or 3)")
