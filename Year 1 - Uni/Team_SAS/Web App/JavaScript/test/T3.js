//setting up test environment
process.env.NODE_ENV = "test";
var test = require('unit.js');
//import jsdom
var jsdom = require('jsdom');
var assert = require('assert');
//creates a constructor
var { JSDOM } = jsdom;
//importing server code for testing 
var server = require('../server');

describe("test options", function () {
    // waits for 5 seconds before time out
    before(function (done) {
        this.timeout(5000);
        done();
    });
    it('T3', function (done) {
        test.httpAgent(server.app)
            .get('/display?postcode=BS161QY')
            .set('Accept', 'text/html')
            .expect(200)
            .end(function (error, response) {
                if (error) {
                    return done(error.message);
                }
                else {
                    var dom = new JSDOM(response.text);
                    //console.log(dom.serialize());
                    test.object(dom.window.document);
                    var placePoints = dom.window.document.querySelector("tbody");

                    //checks that there is some response in display.ejs
                    //Specify the length of children.
                    assert.ok(placePoints.children.length > 0);
                    done();
                }

            })
    });
});