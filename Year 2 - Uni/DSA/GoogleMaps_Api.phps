<?php
$data = json_decode(file_get_contents("config.json"), true);
$googleMapsKey = $data["googleMapsKey"];


$city =$_GET['city'];
$lat =$_GET['lat'];
$lng =$_GET['lng'];
?>
<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-15"/>
    <title>Using MySQL and PHP with Google Maps</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

<html>
  <body>
    <div id="map"></div>

    <script>
      var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        },
      };


        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(<?php echo $lat?>,<?php echo $lng ?> ),
          zoom: 12
        });
            
        var iconBase = 'http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/icons/';
        var icons = {
          Museum: {
            icon: iconBase + 'historical_museum.png'
          },
        
          Zoo: {
            icon: iconBase + 'zoo.png'
          },
        
          Religious_structure: {
            icon: iconBase + 'prayer.png'
          },
          
          Football_Stadium: {
            icon: iconBase + 'soccerfield.png'
          },
          
          Park: {
            icon: iconBase + 'urbanpark.png'
          },
          
          Theatre: {
            icon: iconBase + 'cinema.png'
          },
            
          University: {
            icon: iconBase + 'university.png'
          },
          Historical_Structure: {
            icon: iconBase + 'ruins.png'
          },      
            
        };
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
          downloadUrl('http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/markers.php?city_name=<?php echo $city?>', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label,
                icon: icons[type].icon  
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
                window.open (href = 'http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/POI.php?name=' + name);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $googleMapsKey?>&callback=initMap">
    </script>
  </body>
</html>