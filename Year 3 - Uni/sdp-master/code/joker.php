<?php
    #adding the header path
    require "header.php";
?>
<!DOCTYPE html>
<head>
    <!-- path to css file and javascript file -->
    <link href="css/layout.css" type="text/css" rel="stylesheet">
    <script src="js/table.js"></script>
</head>
<body>
    <section>
    <!-- LEFT-CONTAINER -->
    <div class="middle-container container">
        <img src="images/Joker.jpeg" alt="Joker movie cover" style="width:500px;height:500px;">
    </div>
</section>
<section>
      <!-- MIDDLE-CONTAINER -->
    <div class="middle-container container">
        <div class="p block"> <!-- PROFILE (MIDDLE-CONTAINER) -->
            <h1 class="page-title">Joker</h1> 
            <h2>Storyline Description</h2>
                <p class="font-color">Joker is a 2019 American psychological thriller film directed and produced by Todd Phillips, who co-wrote the screenplay with Scott Silver. The film, based on DC Comics characters, stars Joaquin Phoenix as the Joker and provides a possible origin story for the character.</p>
            <div class="p-description">
            <h2>Rating</h2>
                <img class="preview" src="images/ratings.jpeg" alt="Rating star icon">
                <h2>85%</h2>
            </div>
        </div>
    </div>
</section>
<div class="space"></div>
<!-- Cast and Crew table-->
    <section>
        <h1>Cast and Crew</h1>
        <div class="tbl-header">
            <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                <th>Actor</th>
                <th>...</th>
                <th>Character</th>
                </tr>
            </thead>
            </table>
        </div>
        <div class="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td><a href="actor.php?actor=joaquinphoenix">Joaquin Phoenix</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=arthurfleck">Arthur Fleck</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=robertdeniro">Robert De Niro</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=murrayfranklin">Murray Franklin</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=zaziebeetz">Zazie Beetz</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=sophiedumond">Sophie Dumond</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=francesconroy">Frances Conroy</a></td>
                    <td >....</td>
                    <td><a href="character.php?character=pennyfleck">Penny Fleck</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=brettcullen">Brett Cullen</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=thomaswayne">Thomas Wayne</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=sheawhigham">Shea Whigham</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=detectiveburke">Detective Burke</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=billcamp">Bill Camp</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=detectivegarrity">Detective Garrity</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=glennfleshler">Glenn Fleshler</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=randall">Randall</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=leighgill">Leigh Gill</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=gary">Gary</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=joshpais">Josh Pais</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=hoytvaughn">Hoyt Vaughn</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=roccoluna">Rocco Luna</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=gigidumond">GiGi Dumond</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=marcmaron">Marc Maron</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=geneufland">Gene Ufland</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=sondrajames">Sondra James</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=drsally">Dr. Sally</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=murphyguyer">Murphy Guyer</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=barrydonnell">Barry O'Donnell</a></td>
                </tr>
                <tr>
                    <td><a href="actor.php?actor=douglashodge">Douglas Hodge</a></td>
                    <td>....</td>
                    <td><a href="character.php?character=alfredpennyworth">Alfred Pennyworth</a></td>
                </tr>
            </tbody>
            </table>
        </div>
    </section>
</body>
<?php
    #adding the footer path
    require "footer.php";
?>