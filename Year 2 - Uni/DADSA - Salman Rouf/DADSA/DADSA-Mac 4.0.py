import csv
import operator
import sys #allows me to exit the program without having to ask the user twice

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue, itemShape, itemWeight):
        self.itemNumber = int (itemNumber)
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)
        self.itemShape = itemShape
        self.itemWeight = int(itemWeight)


class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    #add items function
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2], row[3], row[4])
            self.items.append(item)

    #appending the items i'm adding into one of the warehouses
    def addSomething(self, item):
        self.items.append(item)
        
    #works out the total value of one warehouse with the items we might be adding.
    def totalValue(self):
        totalValue = 0
        for i in self.items:
            totalValue = totalValue + i.itemValue
        return totalValue

    #remove function
    def removeItem(self, item):
        self.items.remove(item)

    def getValue(self):
        return self.item_value

    


def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse
    


#Reading CSV file from each warehouse
A = readingCSV(r'Warehouse A.csv', 'A')
B = readingCSV(r'Warehouse B.csv', 'B')
C = readingCSV(r'Warehouse C.csv', 'C')
D = readingCSV(r'Warehouse D.csv', 'D')
E = readingCSV(r'TASK 1.csv', '1')
##F = readingCSV(r'TASK 2.csv', '2')
##G = readingCSV(r'TASK 3 AND 4.csv', '3')
aRectangle = 1016
aSphere = 0
aPyramid = 2032
aSquare = 2032
bRectangle = 500
bSphere = 2032
bPyramid = 250
bSquare = 0
cRectangle = 0
cSphere = 250
cPyramid = 500
cSquare = 0
dRectangle = 500
dSphere = 750
dPyramid = 3048
dSquare = 750
rectangleA = []
sphereA = []
pyramidA = []
squareA = []
capacity = 2000000000 

checkA =(capacity - Warehouse.totalValue(A))
checkB = (capacity - Warehouse.totalValue(B))
checkC = (capacity - Warehouse.totalValue(C))
checkD = (capacity - Warehouse.totalValue(D))

class move:     
    def warehouseA(self, item, shape):
            for i in A.items:
                getA = (i.itemShape, i.itemWeight)
                if getA[0] == ('Rectangle'):
                    rectangleA.append(i.itemShape)
                elif getA[0] == ('Sphere'):
                    sphereA.append(i.itemShape)
                elif getA[0] == ('Pyramid'):
                    pyramidA.append(i.itemShape)
                elif getA[0] == ('Square'):
                    squareA.append(i.itemShape)
                            
            lengthRectangle = len(rectangleA)
            lengthSphere = len(sphereA)
            lengthPyramid = len(pyramidA)
            lengthSquare = len(squareA)
            
            
            if shape == ('Rectangle'):
                if lengthRectangle < 5:
                    print('good')
                    A.addSomething(item)
                    E.removeItem(item)
                else:
                    print('Rectangle cant fit')
            elif shape == ('Sphere'):
                if lengthSphere < 0:
                    print('Sphere can not be added to warehouse A')
            elif shape == ('Pyramid'):
                if lengthPyramid < 10:
                    print('Pyramid is being added')
                    A.addSomething(item)
                    E.removeItem(item)
                else:
                    ('Pyramid cant fit')
            elif shape == ('Square'):
                if lengthSquare < 5:
                    print('Square can fit')
                    A.addSomething(item)
                    E.removeItem(item)
                else:
                    print('Square cant fit')    
            else:
                print('not today')
                
            print(lengthRectangle, lengthSphere, lengthPyramid, lengthSquare)
            

            rectangleA.clear()
            sphereA.clear()
            pyramidA.clear()
            squareA.clear()


    def checkingShape(self, item, shape, weight):
        for i in item.itemShape:
            if shape == ('Rectangle'):
                if weight < aRectangle:
                    first.warehouseA(item, shape)
                    break
                else:
                    print('Rectangle item is too big')
                    break
            elif shape == ('Sphere'):
                if weight < aSphere:
                    first.warehouseA(item, shape)
                    break
                else:
                    print('Item cannot be stored within warehouse A')
                    break
            elif shape ==('Pyramid'):
                if weight < aPyramid:
                    first.warehouseA(item, shape)
                    break
                else:
                    print('pyramid is too big')
                    break
            elif shape == ('Square'):
                if weight < aSquare:
                    first.warehouseA(item, shape)
                    break
                else:
                    print('square too big')
                    break
            else:
                print('failed')

        
            
    def movingItems(functions):
        #itemNumber = None #Defining the item number within the move function
       # itemNumber = int(input("Please Enter The Item Number You Would Like To Move\n"))#redefining the item number
        E.items.sort(key=lambda x: x.itemWeight, reverse=True)
        item = (E.items[0])
        get = (item)
        value = (get.itemValue)
        shape = (get.itemShape)
        weight = (get.itemWeight)
        print(value, shape, weight)
        if value < checkA:
            first.checkingShape(item, shape, weight)
        elif value < checkB:
            print('B')
        elif value < checkC:
            print('C')
        elif value < checkD:
            print('D')
        else:
            print(item.itemNumber +'Can not fit')


first = move()
    
while True: 
    print("Warehouse Menu\n1.View Warehouse A\n2.View Warehouse B\n3.View Warehouse C\n4.View Warehouse D\n5.Task 1")
    selection = input("Enter: ")
    itemNo = 'Item No.'
    description = 'Description'
    value = 'Value in £'
    shape = 'Shape'
    weight = 'Weight (KG)'
    print(f'{itemNo:10}', f'{description:50}', f'{value:15}', f'{shape:10}', f'{weight:10}')
    if selection == '1':
        for item in A.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
    elif selection == '2':
        for item in B.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
    elif selection == '3':
        for item in C.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
    elif selection == '4':
        for item in D.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
    elif selection == '5':
        for item in E.items:
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
        first.movingItems()
    else:
        print("Nothing")

