import csv

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = itemNumber
        self.itemDescription = itemDescription
        self.itemValue = itemValue


class Warehouse:
    def __init__(self, name):
        self.items = []

        
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)
           # print(item)

        
def readingCSV(file, name):
    with open(file, encoding = 'utf-8', errors = 'ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse
    
#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')

#Asking the user to pick a warehouse to place the item within
decision =("Select to view warehouse A, B, C or D")
print(decision)

choice=input("Please Select: ")
if choice=='A':
    for item in A.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
        #break
elif choice == 'B':
    for item in B.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
        #break
elif choice =='C':
    for item in C.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
        #break
elif choice =='D':
    for item in D.items:
        print(item.itemNumber,item.itemDescription,item.itemValue)
        #break
else:
    print("error: Unknown Variable Selected")
    print("Make sure that you are selecting A, B, C or D")

again =("Exit?")
print(again)
if again == '1':
    exit()

