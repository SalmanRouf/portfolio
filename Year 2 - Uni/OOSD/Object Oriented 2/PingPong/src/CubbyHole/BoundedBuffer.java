/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CubbyHole;

import static java.lang.Thread.sleep;

/**
 *
 * @author s2-rouf
 */
public class BoundedBuffer {

    private int[] buffer;
    private int lastIndex;
    private boolean isFull;

    /*creating a buffer with given size */
    public BoundedBuffer(int size) {
        isFull = false;
        lastIndex = 0;
        buffer = new int[size];
    }

    public synchronized void produce(int data) {
        while (isFull == true) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }
        buffer[lastIndex] = data;
        lastIndex++;
        if (lastIndex >= buffer.length) {
            isFull = true;
            notifyAll();
        }
    }

    public synchronized int consume() {
        while (isFull == false) {
            try {
                wait();
            } catch (InterruptedException ex) {
            }
        }
        lastIndex--;
        if (lastIndex == 0) {
            isFull = false;
            notifyAll();
        }
        return buffer[lastIndex];
    }
}

class Producer1 extends Thread {
    private int number;
    private BoundedBuffer boundedbuffer;

    public Producer1(BoundedBuffer boundedbuffer, int number) {
        this.boundedbuffer = boundedbuffer;
        this.number = number;

    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            boundedbuffer.produce(i);
            System.out.println("Producer #" + this.number + " produced: " + i);
            try {
                sleep((int) (Math.random() * 100));
            } catch (InterruptedException e) {
            }
        }
    }
}

class Consumer1 extends Thread {

    private BoundedBuffer boundedbuffer;
    private int number;

    public Consumer1(BoundedBuffer boundedbuffer, int number) {
        this.boundedbuffer = boundedbuffer;
        this.number = number;
    }

    public void run() {
        int value = 0;
        for (int i = 0; i < 10; i++) {
            value = boundedbuffer.consume();
            System.out.println("Consumer #" + this.number
                    + " consumed: " + value);
        }
    }
}

class TestProducerConsumer {

    public static void main(String args[]) {
        BoundedBuffer buffer = new BoundedBuffer(5);
        Producer1 p1 = new Producer1(buffer, 1);
        Producer1 p2 = new Producer1(buffer, 2);
        Producer1 p3 = new Producer1(buffer, 3);
        Consumer1 c1 = new Consumer1(buffer, 1);
        Consumer1 c2 = new Consumer1(buffer, 2);
        Consumer1 c3 = new Consumer1(buffer, 3);        
        p1.start();
        p2.start();
        p3.start();
        c1.start();
        c2.start();
        c3.start();
    }
}
