<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="4.0" indent="yes"/>
    <xsl:variable name="space">
        <xsl:text>  </xsl:text>
    </xsl:variable>
    <xsl:template match="product">
    <html>
        <head>
            <title>Products XSLT</title>
        </head>
        <body>
            <h3>Products</h3>
            <table border="1" cellpadding="4" cellspacing="0" bordercolor="#cccccc" width="400">
                        <tr>
                            <td width="100">Category</td>
                            <td>
                                <xsl:value-of select="category"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">ID</td>
                            <td>
                                <xsl:value-of select="@id"/>
                            </td>
                        </tr>
                         <tr>
                             <td align="left" valign="top">Product</td>
                             <td>
                                 <xsl:value-of select="name"/>
                             </td>
                         </tr>
                          <tr>
                              <td align="left" valign="top">Description</td>
                              <td>
                                  <xsl:value-of select="description"/>
                              </td>
                          </tr>
                            <tr>
                                <td align="left" valign="top">RRP</td>
                                <td>
                                    <xsl:value-of select="rrp"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">Price</td>
                                <td>
                                    <xsl:value-of select="price"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">Manufacturer</td>
                                <td>
                                    <xsl:value-of select="manufacturer"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">Image</td>
                                <td>
                                     <img src="H:\{image}" style="width:128px;height:128"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">Keywords</td>
                                <td>
                                    <xsl:value-of select="keywords"/>
                                </td>
                            </tr>
        
            </table>
        </body>
    </html>
    </xsl:template>
        <!--<xsl:template match="text()"/>-->    
</xsl:stylesheet>