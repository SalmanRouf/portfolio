<?php
function error_validation($error_number, $format="xml"){ 
    #storing the error message required within this variable 
    $error_message = ERROR_MESSAGE[$error_number];
    #if the format is json, this will print the json in the correct format
    if ($format=='json'){
        $json = array('conv'=> array('error' => array("code" => "$error_number", "msg" => "$error_message")));
		header('Content-Type: application/json');
        echo json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        exit;
    }
    #if the format is xml, this will print the xml in the correct format
    if ($format=='xml'){
        #creating a new dom document with pretty formatting
        $doc = new DomDocument(VERSION, ENCODING);
        $doc->formatOutput=true;
        #<conv> as the root tag
        $root =$doc->createElement("conv");
        $doc->appendChild($root);
        #<error>
        $errortag =$doc->createElement("error");
        $root->appendChild($errortag);
        #<code> with code number
        $codetag =$doc->createElement("code", $error_number);
        $errortag->appendChild($codetag);
        #<msg> with the error message
        $messagetag =$doc->createElement("msg", $error_message);
        $errortag->appendChild($messagetag);
        #checking if the file path is the same as currencies2 declared in config. Everything printing under form directory doesn't need "<xmp>" tags.
        #printing the document to the page and exiting
        if (FILE_PATH == CURRENCIES2){
            echo "<xmp>".$doc->SaveXML()."</xmp>";
            exit;
        } else{
            #every directory but the form dir requires the "<xmp>" tags, ensuring that these tags are added 
            echo "<xmp>".$doc->SaveXML()."</xmp>";
            exit;
        }
    }
}

function live($up=0){
    #finding the current file path for every file.
    if ($up == 0){
        #getting the currencies declared in the config and adding the path to a variable - if function is called from index.php under assignment foler. it will end up using this file path
        $dir = CURRENCIES;
    }
    if ($up == 1){
        #getting the currencies1 declared in the config and adding the path to a variable - if function is called from index.php under update folder. it will use this file path
        $dir = CURRENCIES1;
    }
    if ($up == 2){
        #getting the currencies2 declared in the config and adding the path to a variable - if function is called from index.php under form folder. it will use this file path
        $dir = CURRENCIES2;
    }
    if (file_exists($dir)) {
        #loading the currencies file with the current file path
        $xml_currencies=simplexml_load_file($dir) or die("Error: Cannot load currencies file");
        #storing the timestamp from the currencies file
        $date = date('j F Y H:i', (string) $xml_currencies['timestamp']);
        #using xpath to store each of the codes that match 1
        $live_path = $xml_currencies->xpath("//currency[@live=1]/code");
        #using xpath to store each of the codes that match 0
        $not_path = $xml_currencies->xpath("//currency[@live=0]/code");
        #using xpath to store each of the codes that match 2
        $na_path = $xml_currencies->xpath("//currency[@live=2]/code");
        #creating empty arrays for each variable
        $live = $not = $not_live = $na = [];
        #array for each live codes, not live codes and the codes which don't have a fixer rate 
        foreach ($live_path as $code) {$live[] = (string) $code;}
        foreach ($not_path as $code) {$not[] = (string) $code;}
        foreach ($na_path as $code) {$na[] = (string) $code;}
        #merging the inactive codes with the codes that have no rates - Used to check errors and for the form(PUT)
        $not_live = array_merge($not,$na);
        #sorting the $not_live into alphabetical order
        sort($not_live);
    }
    else {
        #if unable to find the currencies file - default live codes, not live codes and codes that don't have a fixer rate are set.
        $live = array(
            'AUD', 'BRL', 'CAD','CHF', 'CNY', 'DKK', 'EUR','GBP', 'HKD', 'HUF', 'INR','JPY', 'MXN', 'MYR', 'NOK','NZD',
            'PHP', 'RUB', 'SEK','SGD', 'THB', 'TRY', 'USD','ZAR');
        $not_live = array(
            'AED','AFN','ALL','AMD','ANG','AOA','ARS','AWG','AZN','BAM','BBD','BDT','BGN','BHD','BIF','BMD','BND','BOB', 'BOV','BSD',
            'BTN','BWP','BYN','BZD','CDF','CHE','CHW','CLF','CLP','COP','COU','CRC','CUC','CUP','CVE','CZK','DJF','DOP','DZD','EGP',
            'ERN', 'ETB','FJD','FKP','GEL','GHS','GIP','GMD','GNF','GTQ','GYD','HNL','HRK','HTG','IDR','ILS','IQD','IRR','ISK','JMD',
            'JOD','KES');
        $na = array(
            'BOV', 'CHE', 'CHW','COU', 'MRU', 'MXV', 'SSP','STN', 'USN', 'UYI', 'UYW','VES', 'XBA', 'XBB', 'XBC','XBD', 'XPD', 
            'XPT', 'XSU','XTS', 'XUA', 'XXX');
        $date = 'N/A'; #default date is set
    }
    #defining variables to use throughout my files
    define('LIVE', $live);
    define('INACTIVE', $not_live);
    define('NA', $na);
    define('DATE', $date);
    define('FILE_PATH', $dir);
}


function update(){
    #loading the rates from fixer with the rates url declared in the config
    $rates = file_get_contents(RATES_URL) or die ("Error: Unable to get rates content");
    #storing the data as an object
    $decode = json_decode($rates);
    #calculating 1 divided by GBP from fixer to work out the difference -  use this later to set GBP to the base rate 
    $gbp = 1/ $decode->rates->GBP; 
    #creating the method
    $writer = new XMLWriter();
    #opening the currencies file with the current file path declared in the live function - open for editing
    $writer->openURI(FILE_PATH);
    #indenting the file - pretty formatting
    $writer->setIndent(true);
    #creating the document and setting the version and encoding of the xml file using the declared version and encoding from the config
    $writer->startDocument(VERSION, ENCODING);
    #starting <currencies> tag with attributes of base GBP and timestamp from fixer
    $writer->startElement("currencies");
    $writer->writeAttribute('base', 'GBP'); 
    $writer->writeAttribute('timestamp', $decode->timestamp); 
    #<date> tag with value - this is just the date/time that the file was created on
    $writer->startElement("date");
    $writer->text(date("j F Y H:i"));
    $writer->endElement();
    #loading the iso codes with the iso url declared within the config file
    $iso_currencies=simplexml_load_file(ISO) or die ("Error: Unable to get ISO content");
    #getting the currency codes using xpath 
    $codes_path = $iso_currencies->xpath("//CcyNtry/Ccy");
    #creating an empty array
    $iso_codes = [];
    #array with unique currency codes - loops through an adds every code into $iso_codes until every code is added
    foreach ($codes_path as $code) {
        if (!in_array($code, $iso_codes)) {
            $iso_codes[] = (string) $code;
        }
    }
    #sorting the $iso_codes into alphabetical order
    sort($iso_codes);
    #loops through every iso code stored within the $iso_code array
    foreach ($iso_codes as $values) { 
        #using xpath to find the codes nodes that match the code and its parents
        $nodes = $iso_currencies->xpath("//Ccy[.='$values']/parent::*");
        #getting the currency
        $cname =  $nodes[0]->CcyNm;
        #starting the <currency> tag with an attribute of live and rate
        $writer->startElement("currency");
        #if the code does exist within the fixer then continue 
        if (isset($decode->rates->$values)){
            #if value is GBP then rate is set to 1.00 else value is calculated to make GBP the base
            if ($decode->rates->$values == 'GBP'){
                $writer->writeAttribute('rate', '1.00');
            } else{
                $writer->writeAttribute('rate', $decode->rates->$values * $gbp);
            }
            #if value is within the live array declared within the live function - then the value will be set to a live attribute of 1
            if (in_array($values, LIVE)){
                $writer->writeAttribute('live', '1'); 
            } 
            else{
                #if not a live code - the value is set to an attribute of 0
                $writer->writeAttribute('live', '0'); 
            }
        } else {
            #if the code doesn't exist within fixer - the rate and live attributes are set to defaults
            $writer->writeAttribute('rate', 'n/a');
            $writer->writeAttribute('live', '2');
        }
        #<code> tag with code value
        $writer->startElement("code"); 
        $writer->text($values);
        $writer->endElement();
        #<curr> tag with currency value
        $writer->startElement("curr");
        $writer->text($cname);
        $writer->endElement();
        #<loc> tag with with each loc value
        $writer->startElement("loc");
            $last = count($nodes) - 1;
            #grouping countries together & lowercase first letter in name - cleaning up the loc tag whilst storing the data
            foreach ($nodes as $index=>$node) {
                $writer->text(mb_convert_case($node->CtryNm, MB_CASE_TITLE, ENCODING));
                $cntry = trim(preg_replace('/[\t\n\r\s]+/', ' ', $node->CtryNm));
                $wrong = array("Of", "And", "U.s.", "(The)", " , ");
                $right = array("of", "and", "U.S.", "", ", ");
                $cn = str_replace($wrong, $right, $cntry);
                if ($index!=$last) {$writer->text(', ');}
            }
        #closing tags - <currencies> and <currency>
        $writer->endElement();
        $writer->endElement();
    }
    #ending the document 
    $writer->endDocument();
    $writer->flush();
    #finding the correct path to the data file to save a copy of the currencies xml file - backup version with timestamp within file name
    if (FILE_PATH == CURRENCIES){
        $path = "data/currencies_$decode->timestamp.xml";
    }
    if (FILE_PATH == CURRENCIES1){
        $path = "../data/currencies_$decode->timestamp.xml";
    }
    if (FILE_PATH == CURRENCIES2){
        $path = "../../data/currencies_$decode->timestamp.xml";
    }
    #copies the currencies xml file into the data folder under assignment - currencies_timestamp.xml is the format of every copied file
    copy(FILE_PATH, $path);
};

function print_format($from, $to, $amnt, $format){
    #loading the currencies file with the current file path
    $xml_print=simplexml_load_file(FILE_PATH) or die("Error: Cannot load currencies file");
    #storing the timestamp from the currencies file
    $date = date('j F Y H:i', (string) $xml_print['timestamp']);
    #storing variables for from rate, curr and loc using xpath
    $from_path = $xml_print->xpath("//code[.='$from']/parent::*");
    $from_rate = (string) $from_path[0]['rate'];
    $from_curr = (string) $from_path[0]->curr;
    $from_loc = (string) $from_path[0]->loc;
    #storing variables for to rate, curr and loc using xpath
    $to_path = $xml_print->xpath("//code[.='$to']/parent::*");
    $to_rate = (string) $to_path[0]['rate'];
    $to_curr = (string) $to_path[0]->curr;
    $to_loc = (string) $to_path[0]->loc;
    #if from and to rate are the same, rate is set to 1
    if ($from == $to){
        $rate = 1.00;
    } else{
        #calculating the rate
        $rate = floatval($to_rate/$from_rate);
    }
    #calculating the rate with the amnt
    $rate_cal = ($rate * $amnt);
    #if the format is json, this will print the json in the correct format
    if ($format == 'json'){
        $json = array('conv' => array("at" => "$date", "rate" => "$rate", "from" => array("code" => "$from", "curr" => "$from_curr", "loc" => "$from_loc", "amnt" => "$amnt"), "to" => array("code" => "$to", "curr" => "$to_curr", "loc" => "$to_loc", "amnt" => "$rate_cal")));
        header('Content-Type: application/json');
        echo json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        exit;
    }
    #if the format is xml, this will print the xml in the correct format
    if ($format == 'xml'){
    #create a new dom document with pretty formatting
    $doc = new DomDocument(VERSION, ENCODING);
    $doc->formatOutput=true;
    #setting the tags with the correct variables
    $root =$doc->createElement("cov");
    $doc->appendChild($root);
    $date = $doc->createElement("at", $date);
    $root->appendChild($date);
    $rate = $doc->createElement("rate", $rate);
    $root->appendChild($rate);
    $fromtag = $doc->createElement("from");
    $root->appendChild($fromtag);
    $fromcode = $doc->createElement("code", $from);
    $fromtag->appendChild($fromcode);
    $fromcurr = $doc->createElement("curr", $from_curr);
    $fromtag->appendChild($fromcurr);
    $fromloc = $doc->createElement("loc", $from_loc);
    $fromtag->appendChild($fromloc);
    $fromamount = $doc->createElement("amnt", $amnt);
    $fromtag->appendChild($fromamount);
    $totag = $doc->createElement("to");
    $root->appendChild($totag); 
    $tocode = $doc->createElement("code", $to);
    $totag->appendChild($tocode);
    $tocurr = $doc->createElement("curr", $to_curr);
    $totag->appendChild($tocurr);
    $toloc = $doc->createElement("loc", $to_loc);
    $totag->appendChild($toloc);
    $toamount = $doc->createElement("amnt", $rate_cal);
    $totag->appendChild($toamount);
    #printing the document to the page and exiting
    echo "<xmp>".$doc->SaveXML()."</xmp>";
    exit;
    }
}

function action_input($cur, $action){
    #loading the currencies file with the current file path 
    $xml_load=simplexml_load_file(FILE_PATH) or die("Error: Cannot load currencies file");
    #if action is post or put then storing variables and updating the rates in currencies file
    if ($action == 'post' || $action == 'put'){
        $old = $xml_load->xpath("//code[.='$cur']/parent::*");
        $old_rate = (string) $old[0]['rate'];
        update();
        $new = $xml_load->xpath("//code[.='$cur']/parent::*");
        $current_rate = (string) $new[0]['rate'];
        $curr_name = (string) $new[0]->curr;
        $curr_loc = (string) $new[0]->loc;
    }
    #storing the timestamp from the currencies file
    $new_date = date('j F Y H:i', (string) $xml_load['timestamp']);
    #creating a new dom document with pretty formatting
    $doc = new DomDocument(VERSION, ENCODING);
    $doc->formatOutput=true;
    $root =$doc->createElement("action");
    $root->setAttribute("type", $action);
    $doc->appendChild($root);
    $date = $doc->createElement("at", $new_date);
    $root->appendChild($date);
    #if action is del change the value to 0 in the currencies file and print the xml
    if ($action == 'del'){
        #loading the del cur in xpath
        $delete = $xml_load->xpath("//code[.='$cur']/parent::*");
        #changing the live value of cur from a 1 to a 0 and saving it into the currenices file
        $delete_change = (string) $delete[0]['live'] = '0';
        $xml_load->asXML(FILE_PATH);
        #setting the code tag with variable, printing xml and exiting
        $del_code = $doc->createElement("code", $cur);
        $root->appendChild($del_code);
        #checking if the file path is the same as currencies2 in config. Everything printing under form directory doesn't need "<xmp>" tags. 
        #printing the document to the page and exiting
        if (FILE_PATH == CURRENCIES2){
            echo "<xmp>".$doc->SaveXML()."</xmp>";
            exit;
        }else{
            #every directory but the form dir requires the "<xmp>" tags, ensuring that these tags are added
            echo "<xmp>".$doc->SaveXML()."</xmp>";
            exit;
        }
    }
    #if action is put, changing the live value of cur from a 0 to a 1 and saving it into the currenices file
    #incase the code does break - ensuring that every post value is 1. 
    if (($action == 'put') || ($action == 'post')){
        $put_change = (string) $new[0]['live'] = '1';
        $xml_load->asXML(FILE_PATH);
    }
    #setting rate tag with the current rate within the currencies file
    $rate_tag = $doc->createElement("rate", $current_rate);
    $root->appendChild($rate_tag);
    #if action is post, setting the old rate tag with the old rate variable
    if ($action == 'post'){
        $old_tag = $doc->createElement("old_rate", $old_rate);
        $root->appendChild($old_tag);
    }
    #setting the tags with the correct variables
    $curr_tag = $doc->createElement("curr");
    $root->appendChild($curr_tag);
    $code_tag = $doc->createElement("code", $cur);
    $curr_tag->appendChild($code_tag);
    $name_tag = $doc->createElement("name", $curr_name);
    $curr_tag->appendChild($name_tag);
    $loc_tag = $doc->createElement("loc", $curr_loc);
    $curr_tag->appendChild($loc_tag); 
    #checking if the file path is the same as currencies2 declared in config. Everything printing under form directory doesn't need "<xmp>" tags. 
    #printing the document to the page and exiting

    #every directory but the form dir requires the "<xmp>" tags, ensuring that these tags are added 
    echo "<xmp>".$doc->SaveXML()."</xmp>";
    exit;
}
?>