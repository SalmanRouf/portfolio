<?php

error_reporting(E_ALL);

include('classes/BracketsGenerator.php');

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Tournament Brackets Generator</title>
        <link href="assets/css/style.css" rel="stylesheet">
    </head>
    <body>
        <h1>Tournament Brackets Generator</h1>
        <form method="post" action="index.php">
            <label for="teams">Number of teams:</label>
            <input type="text" name="teams" id="teams" value="<?php echo isset($_POST['teams']) ? $_POST['teams'] : '' ?>">
            <div id="or">OR</div>
            <label for="names">Team names (one per line):</label>
            <textarea name="names" id="names"><?php echo isset($_POST['names']) ? $_POST['names'] : '' ?></textarea>
            <label class="radio-label">
                Seeded Brackets:
                <input type="radio" name="type" value="1"<?php echo !isset($_POST['type']) || $_POST['type'] == 1 ? ' checked="checked"' : '' ?>>
            </label>
            <label for="type" class="radio-label">
                Random Brackets:
                <input type="radio" name="type" value="2"<?php echo isset($_POST['type']) && $_POST['type'] == 2 ? ' checked="checked"' : '' ?>>
            </label>
            <input type="submit" name="submit" value="Generate Brackets">
        </form>
        <?php

            if(isset($_POST['submit'])) {

                $brackets = new BracketsGenerator($_POST['names'], $_POST['teams'], $_POST['type']);

                $brackets->printBrackets();

            }

        ?>
        <div id="version">Version: 2.0</div>
    </body>
</html>