/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author s2-rouf
 */
public class Lecturer extends Person{
    private int lecturerID;
    
    public Lecturer(){
    }
    
    public Lecturer(String firstName, String lastName, String address, int lecturerID){
         super(firstName, lastName, address);
         this.lecturerID = lecturerID;
    }
    
}
