########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'x-ms-client-request-id': '',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    # Request parameters
    'accessToken': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJBY2NvdW50SWQiOiJiOTE5ZjY5OC0wMDkxLTRmOGUtOWY4Zi0yZjkzMTMxYzc2NWYiLCJWaWRlb0lkIjoiZTFjOWYxZTFjYyIsIkFsbG93RWRpdCI6IlRydWUiLCJFeHRlcm5hbFVzZXJJZCI6ImEyMzdlNDIxYjdiNDVhMjIiLCJVc2VyVHlwZSI6Ik1pY3Jvc29mdCIsImlzcyI6Imh0dHBzOi8vd3d3LnZpZGVvaW5kZXhlci5haS8iLCJhdWQiOiJodHRwczovL3d3dy52aWRlb2luZGV4ZXIuYWkvIiwiZXhwIjoxNTc1ODg5OTkzLCJuYmYiOjE1NzU4ODYwOTN9._TDCmFDY2-nWi_V3mXpYtQsY-5H2YlBmBpUMqZeBPnE',
})

try:
    conn = http.client.HTTPSConnection('api.videoindexer.ai')
    conn.request("DELETE", "/trial/Accounts/b919f698-0091-4f8e-9f8f-2f93131c765f/Videos/e1c9f1e1cc?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
