<?php
  #adding the header path
  require "header.php";
  #path to include the validation php file for the login
  include('includes/login.validation.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/placeholders.css" type="text/css">
  </head>
  <body>
    <div class="body-content">
      <div class="module">
        <?php
        if (!isset($_SESSION['id'])) {
          echo '<div class="box">
                  <h2>Login In</h2>
                  <form action="login.php" method="post">
                    <div class="error">'.$fail.'</div>
                    <div>
                      <span class="error">'.$mailuid_error.'</span>
                      <input type="text" name="mailuid" value="'.$mailuid.'" required>
                      <label>Email/Username</label>
                    </div>
                    <div>
                      <span class="error">'.$pwd_error.'</span>
                      <input type="password" name="pwd" required>
                      <label>Password</label>
                    </div>
                    <input type="submit" class="submit" name="login-submit" value="Login In">
                  </form>
              </div>';
        } else {
          echo '<div class="box">
                  <h2>You Are Signed In</h2>
                </div>';
        }
        
        ?>
  <body>
</html>

