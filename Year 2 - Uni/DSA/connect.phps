<?php
$data = json_decode(file_get_contents("config.json"), true);
$server = $data["database"]["server"];
$username = $data["database"]["username"];
$password = $data["database"]["password"];
$database = $data["database"]["database"];

$db = new mysqli($server, $username, $password, $database);
if($db->connect_errno) {
    die('Error');
}

?>