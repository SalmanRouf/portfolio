League Brackets Scheduler 1.0 by RMK147

Usage

To start using the Brackets Scheduler first open the index.php file in a browser.

If you wish to generate brackets with numeric values, then you can enter the number of teams in the first field, and click
the Generate Schedule button.

If you wish to enter custom team names, then you may skip the number of teams field, and enter your teams in the team names
field. Each team name must be on a new line. Any duplicate team names will automatically be removed. The team names field
takes precedence over the number of teams field, if both are filled in.

You can also specify the number of rounds to play. The number of rounds is how many times all teams play each other. For
every alternate round, teams will swap sides depending on if they are the home or away team.

In the event there are an odd amount of teams entered, BYEs will automatically be generated. You have the option whether
or not to display these BYEs in the brackets tables.

Customising

You may customise the look and feel of the generator by editing the CSS file located at assets/css/style.css. For example,
to change the default colour of each brackets table's header from black to red you could do the following:

th{
    background-color: red;
}

To customise the HTML that is outputted by the brackets scheduler, you can edit the printBrackets function in the
BracketsScheduler class located at classes/BracketsScheduler.php