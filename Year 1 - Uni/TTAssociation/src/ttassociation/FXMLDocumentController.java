/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TTAssociation;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Window;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;


/**
 *
 * @author andrew
 */
public class FXMLDocumentController implements Initializable {
    
    private TableTennisManager manager = new TableTennisManager();
    
    private ObservableList<String> teamNames, homePlayerNames, awayPlayerNames;
    
    private ArrayList<TextField> scoreTextFields;
    
    private ArrayList<ComboBox> teamPlayerScoreComboboxes;
        
    @FXML
    private ComboBox<String> selectAddTeam, submitScoresSelectHomeTeam, 
            submitScoresSelectAwayTeam, selectAwayPlayer1, selectAwayPlayer2, 
            selectHomePlayer1, selectHomePlayer2, viewMatchSelectHomeTeam, 
            viewMatchSelectAwayTeam;
    
    @FXML
    private Button addTeamButton, addPlayerButton, modifySheetButton,
            viewMatchButton, submitScoresButton, generateStatsButton,
            generateFixturesButton;
    
    @FXML
    private TextField teamNameField, playerNameField, setH1A1Game1, setH1A1Game2,
            setH1A1Game3, setH2A1Game1, setH2A1Game2, setH2A1Game3, setH1A2Game1, 
            setH1A2Game2, setH1A2Game3, setH2A2Game1, setH2A2Game2, setH2A2Game3,
            dsetGame1, dsetGame2, dsetGame3;
    
    @FXML
    private TextArea statisticsOutput, finalScore;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        //create teams and players for test data
        manager.addTeam("Filton");
        manager.addTeam("UWE");
        manager.addTeam("KCC");
        manager.addTeam("Page"); 
        
        manager.addPlayer("Alex", manager.findTeam("Filton"));
        manager.addPlayer("Brian",manager.findTeam("Filton"));
        manager.addPlayer("Jin",manager.findTeam("UWE"));
        manager.addPlayer("Julia",manager.findTeam("UWE"));
        manager.addPlayer("Stewart",manager.findTeam("UWE"));
        manager.addPlayer("Chris",manager.findTeam("KCC"));
        manager.addPlayer("Ryan",manager.findTeam("KCC"));
        manager.addPlayer("Peter",manager.findTeam("Page"));
        manager.addPlayer("Phil", manager.findTeam("Page"));
        
        // set up comboboxes and textfields
        teamNames = FXCollections.observableArrayList(manager.getTeamNames());
        selectAddTeam.setItems(teamNames);
        submitScoresSelectHomeTeam.setItems(teamNames);
        submitScoresSelectAwayTeam.setItems(teamNames);
        viewMatchSelectHomeTeam.setItems(teamNames);
        viewMatchSelectAwayTeam.setItems(teamNames);
        
        scoreTextFields = new ArrayList<>();
        Collections.addAll(scoreTextFields, setH1A1Game1, setH1A1Game2,
            setH1A1Game3, setH2A1Game1, setH2A1Game2, setH2A1Game3, setH1A2Game1, 
            setH1A2Game2, setH1A2Game3, setH2A2Game1, setH2A2Game2, setH2A2Game3,
            dsetGame1, dsetGame2, dsetGame3);
        
        teamPlayerScoreComboboxes = new ArrayList<>();
        Collections.addAll(teamPlayerScoreComboboxes, submitScoresSelectHomeTeam, 
            submitScoresSelectAwayTeam, selectAwayPlayer1, selectAwayPlayer2, 
            selectHomePlayer1, selectHomePlayer2);
        
        // cresate matches with scores and players for initial test data
        Match match1 = new Match(manager.findTeam("Filton"),
                manager.findTeam("UWE"));
        
        match1.setHomePlayers(match1.findHomePlayer("Alex"), 
                match1.findHomePlayer("Brian"));
        match1.setAwayPlayers(match1.findAwayPlayer("Jin"),
                match1.findAwayPlayer("Julia"));
               
        int [][] scores = new int[][]{
          {11, 2, 3, 11, 11, 5},
          {1, 11, 5, 11, 11, 6},
          {11, 9, 11, 1, 11, 1},
          {11, 2, 3, 11, 11, 5}, 
          {0, 11, 1, 11, 2, 11}
        };  
    
        match1.setScores(scores);
        manager.addMatch(match1);
        
        Match match2 = new Match(manager.findTeam("UWE"),
                manager.findTeam("Page"));
        match2.setHomePlayers(match2.findHomePlayer("Jin"), 
                match2.findHomePlayer("Julia"));
        match2.setAwayPlayers(match2.findAwayPlayer("Peter"), 
                match2.findAwayPlayer("Phil"));
        
        int [][] scores2 = new int[][]{
          {11, 2, 3, 11, 11, 5},
          {11, 1, 5, 11, 11, 6},
          {11, 9, 11, 1, 11, 1},
          {11, 2, 3, 11, 11, 5}, 
          {0, 11, 1, 11, 2, 11}
        };
        
        match2.setScores(scores2);
        manager.addMatch(match2);
        
        //create thread for auto stats updating
        UpdateThread stats = new UpdateThread(100000, manager);
        Thread statsThread = new Thread(stats);
        
        //stop thread if application exits
        statsThread.setDaemon(true);
        
        //start the thread
        statsThread.start();   
    }
    
    @FXML
    protected void addTeamButtonAction (ActionEvent event) {       
        Window owner = addTeamButton.getScene().getWindow();
        //check team name has been entered
        if(teamNameField.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please enter a team name");
        }
        else {
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Success!", 
                    "Welcome " + teamNameField.getText());
            manager.addTeam(teamNameField.getText());
            //add team and update nodes
            teamNames = FXCollections.observableArrayList(manager.getTeamNames());
            selectAddTeam.setItems(teamNames);
            submitScoresSelectHomeTeam.setItems(teamNames);
            submitScoresSelectAwayTeam.setItems(teamNames);
            viewMatchSelectHomeTeam.setItems(teamNames);
            viewMatchSelectHomeTeam.setItems(teamNames);
            teamNameField.clear();
        }
    }
    
    @FXML
    protected void addPlayerButtonAction (ActionEvent event) {       
        Window owner = addPlayerButton.getScene().getWindow();
        //check player name has been entered
        if(playerNameField.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please enter a player name");
        }
        //check team has been selected
        if(selectAddTeam.getValue() == null) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please select a team");
        }
        else {
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Success!", 
                    "Welcome " + playerNameField.getText());
        //add player
        manager.addPlayer(playerNameField.getText(), 
                manager.findTeam(selectAddTeam.getValue()));
        }
    }
    
    @FXML
    protected void generateFixturesButtonAction (ActionEvent event) {
        manager.generateFixtures();
        System.out.println(manager.getFixturesMatrix()); 
        Window owner = generateFixturesButton.getScene().getWindow();
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Success!", 
                    "Fixtures generated. Check chart on viewer tab.");
    }
    
    @FXML
    protected void generateStatsButtonAction (ActionEvent event) {
        manager.calculateStatistics();
        Window owner = generateStatsButton.getScene().getWindow();
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Success!", 
                    "Statistics generated. Check viewer tab.");
    }
    
    @FXML
    protected void viewFixturesButtonAction (ActionEvent event) {   
        try {
            //link fixtures controller
            FXMLLoader fxmlLoader;
            fxmlLoader = new FXMLLoader(getClass().getResource("Fixtures.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            
            //get and send fixtures data          
            FixturesController fixController = fxmlLoader.getController();
            fixController.setFixtures(manager.getFixturesMatrix(), 
                    manager.getNumberOfTeams());
            //show new window
            Stage stage = new Stage();
            stage.setTitle("Fixtures and results");
            stage.setScene(new Scene(root1));
            stage.show();
                    
        } catch (Exception e) {
            System.out.println("Can not load fixtures window");
        }

    }
    
    @FXML
    protected void viewTeamStatsButtonAction (ActionEvent event) {
        statisticsOutput.setText(manager.getStatistics());
    }
    
    @FXML
    protected void viewRankedTeamsButtonAction (ActionEvent event) {
        manager.rankBySets();
        manager.calculateStatistics();
        statisticsOutput.setText(manager.getStatistics());
    }
    
    @FXML
    protected void viewMatchButtonAction (ActionEvent event) {
        Window owner = viewMatchButton.getScene().getWindow();
        //check teams have been selected
        if(viewMatchSelectHomeTeam.getValue() == null || 
                viewMatchSelectAwayTeam.getValue() == null) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please select teams");
            return;
        }
        //check teams are different
        if(viewMatchSelectHomeTeam.getValue().equals(viewMatchSelectAwayTeam.getValue()))
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Teams must be different");
        }
        else {
            //use selected teams to find match
            Match match = 
                    manager.findMatch(manager.findTeam(viewMatchSelectHomeTeam.getValue()),
                    manager.findTeam(viewMatchSelectAwayTeam.getValue()));
            //show error if match is not found
            if (match == null) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Match not found, generate fixtures on Admin tab first");
            }
            else {
                //display match details if found
                statisticsOutput.setText(match.toString());
            }
        }
    }
    
    @FXML
    protected void enterScoresHomeTeamSelected (ActionEvent event) {
        //update player select comboboxes according to team selected
        if (submitScoresSelectHomeTeam.getValue() == null) {
            selectHomePlayer1.valueProperty().set(null);
            selectHomePlayer2.valueProperty().set(null);
        }
        else {
            Team homeTeam = 
                    manager.findTeam(submitScoresSelectHomeTeam.getValue());       
            homePlayerNames = 
                    FXCollections.observableArrayList(homeTeam.getPlayerNames());
            selectHomePlayer1.setItems(homePlayerNames);
            selectHomePlayer2.setItems(homePlayerNames);
        }
    }
    
    @FXML
    protected void enterScoresAwayTeamSelected (ActionEvent event) {
        //update player select comboboxes according to team selected
        if (submitScoresSelectAwayTeam.getValue() == null) {
            selectAwayPlayer1.valueProperty().set(null);
            selectAwayPlayer2.valueProperty().set(null);
        }
        else {
            Team awayTeam = 
                    manager.findTeam(submitScoresSelectAwayTeam.getValue());  
            awayPlayerNames = 
                    FXCollections.observableArrayList(awayTeam.getPlayerNames());
            selectAwayPlayer1.setItems(awayPlayerNames);
            selectAwayPlayer2.setItems(awayPlayerNames);
        }
    }
   
    @FXML
    protected void submitScoresButtonAction (ActionEvent event) {
        Window owner = submitScoresButton.getScene().getWindow();
        //check all teams and player have been selected
        if(!checkComboboxes()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Please select all players and teams");
            return;
        }
        //check teams are different
        if(submitScoresSelectHomeTeam.getValue().equals(submitScoresSelectAwayTeam.getValue()))
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Teams must be different");
            return;
        }
        //check players are different
        if(selectHomePlayer1.getValue().equals(selectHomePlayer2.getValue())) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Home players must be different");
            return;
        }
        if(selectAwayPlayer1.getValue().equals(selectAwayPlayer2.getValue())) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Away players must be different");
            return;
        }
        //check scores are only numbers and a colon
        for (int i = 0; i < scoreTextFields.size(); i++) {           
            if(!validateEntries(scoreTextFields.get(i).getText())) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Invalid entries, check and try again");
            return;
            }
        } 
       //populate scores array with textfield entries
        int [][] scores = new int[5][6];
        scores[0][0] = getHomeScoreFromEntry(setH1A1Game1.getText());
        scores[0][1] = getAwayScoreFromEntry(setH1A1Game1.getText());
        scores[0][2] = getHomeScoreFromEntry(setH1A1Game2.getText());
        scores[0][3] = getAwayScoreFromEntry(setH1A1Game2.getText());
        scores[0][4] = getHomeScoreFromEntry(setH1A1Game3.getText());
        scores[0][5] = getAwayScoreFromEntry(setH1A1Game3.getText());
        
        scores[1][0] = getHomeScoreFromEntry(setH1A2Game1.getText());
        scores[1][1] = getAwayScoreFromEntry(setH1A2Game1.getText());
        scores[1][2] = getHomeScoreFromEntry(setH1A2Game2.getText());
        scores[1][3] = getAwayScoreFromEntry(setH1A2Game2.getText());
        scores[1][4] = getHomeScoreFromEntry(setH1A2Game3.getText());
        scores[1][5] = getAwayScoreFromEntry(setH1A2Game3.getText());
        
        scores[2][0] = getHomeScoreFromEntry(setH2A1Game1.getText());
        scores[2][1] = getAwayScoreFromEntry(setH2A1Game1.getText());
        scores[2][2] = getHomeScoreFromEntry(setH2A1Game2.getText());
        scores[2][3] = getAwayScoreFromEntry(setH2A1Game2.getText());
        scores[2][4] = getHomeScoreFromEntry(setH2A1Game3.getText());
        scores[2][5] = getAwayScoreFromEntry(setH2A1Game3.getText());
        
        scores[3][0] = getHomeScoreFromEntry(setH2A2Game1.getText());
        scores[3][1] = getAwayScoreFromEntry(setH2A2Game1.getText());
        scores[3][2] = getHomeScoreFromEntry(setH2A2Game2.getText());
        scores[3][3] = getAwayScoreFromEntry(setH2A2Game2.getText());
        scores[3][4] = getHomeScoreFromEntry(setH2A2Game3.getText());
        scores[3][5] = getAwayScoreFromEntry(setH2A2Game3.getText());
        
        scores[4][0] = getHomeScoreFromEntry(dsetGame1.getText());
        scores[4][1] = getAwayScoreFromEntry(dsetGame1.getText());
        scores[4][2] = getHomeScoreFromEntry(dsetGame2.getText());
        scores[4][3] = getAwayScoreFromEntry(dsetGame2.getText());
        scores[4][4] = getHomeScoreFromEntry(dsetGame3.getText());
        scores[4][5] = getAwayScoreFromEntry(dsetGame3.getText());
        
        //check scores are valid table tennis scores
        if(!validateScores(scores)) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Invalid scores, check and try again");
            return;
        }
        //find desired match
        Match match = 
                manager.findMatch(manager.findTeam(submitScoresSelectHomeTeam.getValue()),
                manager.findTeam(submitScoresSelectAwayTeam.getValue()));
        
        if (match == null) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Match not found, generate fixtures on Admin tab first");
                return;
            }
        //set or update the match scores and players, and calculate final score
        match.setScores(scores);
        
        match.setHomePlayers(match.findHomePlayer(selectHomePlayer1.getValue()),
                match.findHomePlayer(selectHomePlayer2.getValue()));
        
        match.setAwayPlayers(match.findAwayPlayer(selectAwayPlayer1.getValue()),
                match.findAwayPlayer(selectAwayPlayer2.getValue()));
        System.out.println(match);
        finalScore.setText(match.getFinalScore());
        
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Success!", 
                    "Scores submitted");
    }
    
    public boolean checkComboboxes () {
        for (int i = 0; i < teamPlayerScoreComboboxes.size(); i++) {
            if (teamPlayerScoreComboboxes.get(i).getValue() == null) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validateEntries (String entry) {
        //check entry isn't too long and contains a colon
        if (entry.length() > 5 || entry.length() < 4 || !entry.contains(":") ) { 
            return false;
        }
        //check first and last characters are numbers
        Character first = entry.charAt(0);
        Character last = entry.charAt(entry.length()-1);
        if (!Character.isDigit(first) || !Character.isDigit(last)) {
            return false;
        }
        //check second and penultimate character is a number if entry is 5 long
        //(remaining must be a colon)
        if (entry.length() == 5) {
            if (Character.isDigit(entry.charAt(1)) || 
                    Character.isDigit(entry.charAt(3))) {
                return true;
            }                        
        }
        //check either character at index 1 or 2 is a number (remaining must 
        //be a colon)
        return Character.isDigit(entry.charAt(1)) || 
                Character.isDigit(entry.charAt(2));  
    }
    
    public boolean validateScores (int [][] scores) {
        for (int i = 0; i < scores.length; i++) {
            //check all scores are within range 0-99
            for (int j = 0; j < scores[i].length; j++) {
                if (scores[i][j] < 0 || scores[i][j] > 99) {
                    return false;
                }
            }             
            for (int k = 0; k < scores[i].length; k+=2) {
                //check one score from each game is 11 or more    
                if (!(scores[i][k] >= 11 || scores[i][k+1] >= 11)) {
                    return false;
                }
                //check for tie breaker (after 11-10 or 10-11)
                if ((scores[i][k] > 11 || scores[i][k+1] > 11)) {
                    if (Math.abs((scores[i][k] - scores[i][k+1])) != 2) {
                        return false;
                    }
                }               
                //check winning margin is 2 or more
                if (Math.abs((scores[i][k] - scores[i][k+1])) < 2) {
                    return false;
                }
            }              
        }      
        return true;
    }
    
    public int getHomeScoreFromEntry (String score) {
        String res = "";
        for (int i = 0; i < 2; i++) {
            Character character = score.charAt(i);
            if (Character.isDigit(character)) {
                res += character;
            }
        }
        return Integer.parseInt(res);
    }
    
     public int getAwayScoreFromEntry (String score) {
        String res = "";
        for (int i = 2; i < score.length(); i++) {
            Character character = score.charAt(i);
            if (Character.isDigit(character)) {
                res += character; 
            }
        }
        return Integer.parseInt(res);
    }
     
    @FXML
    protected void newSheetActionButton (ActionEvent event) {
        //clear all text fields
        for (int i = 0; i < scoreTextFields.size(); i++) {
            scoreTextFields.get(i).clear();
        }
        finalScore.clear();
        //reset select team comboboxes
        submitScoresSelectHomeTeam.valueProperty().set(null);
        submitScoresSelectAwayTeam.valueProperty().set(null);       
    }
    
    @FXML
    protected void modifySheetActionButton (ActionEvent event) {
        Window owner = modifySheetButton.getScene().getWindow();
        //check teams are different
        if(submitScoresSelectHomeTeam.getValue().equals(submitScoresSelectAwayTeam.getValue()))
        {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Teams must be different");
            return;
        }
        if (manager.findMatch(manager.findTeam(submitScoresSelectHomeTeam.getValue()),
                manager.findTeam(submitScoresSelectAwayTeam.getValue())) == null)  {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                "Match not found, generate fixtures on Admin tab first");
            return;
        }
            
                        
        
        Match match = manager.findMatch(manager.findTeam(submitScoresSelectHomeTeam.getValue()),
                manager.findTeam(submitScoresSelectAwayTeam.getValue()));
        
        //check match has been played
        if (!match.isPlayed()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Error!", 
                    "Match not played yet");
        }
        else {
            //show scores
            setH1A1Game1.setText(match.getGameScore(0, 0));
            setH1A1Game2.setText(match.getGameScore(0, 1));
            setH1A1Game3.setText(match.getGameScore(0, 2));
            setH1A2Game1.setText(match.getGameScore(1, 0));
            setH1A2Game2.setText(match.getGameScore(1, 1));
            setH1A2Game3.setText(match.getGameScore(1, 2));
            setH2A1Game1.setText(match.getGameScore(2, 0));
            setH2A1Game2.setText(match.getGameScore(2, 1));
            setH2A1Game3.setText(match.getGameScore(2, 2));
            setH2A2Game1.setText(match.getGameScore(3, 0));
            setH2A2Game2.setText(match.getGameScore(3, 1));
            setH2A2Game3.setText(match.getGameScore(3, 2));
            dsetGame1.setText(match.getGameScore(4, 0));
            dsetGame2.setText(match.getGameScore(4, 1));
            dsetGame3.setText(match.getGameScore(4, 2));
            finalScore.clear();
            
            //show players
            selectHomePlayer1.valueProperty().set(match.getHomePlayer1Name());
            selectHomePlayer2.valueProperty().set(match.getHomePlayer2Name());
            selectAwayPlayer1.valueProperty().set(match.getAwayPlayer1Name());
            selectAwayPlayer2.valueProperty().set(match.getAwayPlayer2Name());
        }
    }   
}