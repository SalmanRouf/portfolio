/*
This document is the client code. 
 */
package tt_association;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author CP2/02 Team 3
 */
public class TTAssociation extends Application { // start of class TTAssociation

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        stage.setTitle("Table Tennis Association");
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) { // main method 
        launch(args);

    }// end of main method 

}// end of class TTAssociatoin
