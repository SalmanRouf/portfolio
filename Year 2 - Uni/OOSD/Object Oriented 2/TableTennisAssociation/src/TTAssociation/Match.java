/*
This document sets players for Match. 
*/
package TTAssociation;

/**
 *
 * @author andrew
 */
public class Match {
    private Set[] sets;
    private Team homeTeam, awayTeam; 
    private Player homePlayer1, homePlayer2, awayPlayer1, awayPlayer2;
//    private DoubleSet dset;
    private boolean played;
    private int homeFinalScore, awayFinalScore;
    
    public Match (Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        sets = new Set[5];
        
        for (int i = 0; i < sets.length; i++) {
           sets[i] = new Set();
        }
        sets[4].setDoubles();
        this.played = false;
    }
    
    public void setHomePlayers(Player homePlayer1, Player homePlayer2) {
        this.homePlayer1 = homePlayer1;
        this.homePlayer2 = homePlayer2;
        sets[0].setHomePlayer(homePlayer1.getName());
        sets[1].setHomePlayer(homePlayer1.getName());
        sets[2].setHomePlayer(homePlayer2.getName());
        sets[3].setHomePlayer(homePlayer2.getName());
    }
    
    public void setAwayPlayers(Player awayPlayer1, Player awayPlayer2) {
        this.awayPlayer1 = awayPlayer1;
        this.awayPlayer2 = awayPlayer2;
        sets[0].setAwayPlayer(awayPlayer1.getName());
        sets[1].setAwayPlayer(awayPlayer2.getName());
        sets[2].setAwayPlayer(awayPlayer1.getName());
        sets[3].setAwayPlayer(awayPlayer2.getName());
    }
    
    public Team getHomeTeam () {
        return homeTeam;
    }
    
    public Team getAwayTeam() {
        return awayTeam;
    }
    
    public String getHomeTeamName () {
        return homeTeam.getTeamName();
    }
    
    public String getAwayTeamName() {
        return awayTeam.getTeamName();
    }
    
    public Player getHomePlayer1() {
        return homePlayer1;
    }
    
    public Player getHomePlayer2() {
        return homePlayer2;
    }
    
    public Player getAwayPlayer1() {
        return awayPlayer1;
    }
    
    public Player getAwayPlayer2() {
        return awayPlayer2;
    }
    
    public String getHomePlayer1Name() {
        return homePlayer1.getName();
    }
    
    public String getHomePlayer2Name() {
        return homePlayer2.getName();
    }
    
    public String getAwayPlayer1Name() {
        return awayPlayer1.getName();
    }
    
    public String getAwayPlayer2Name() {
        return awayPlayer2.getName();
    }
       
    public void setScores (int [][] scores ) {
        //Initialise final scores
        this.homeFinalScore = 0;
        this.awayFinalScore = 0;
        //Add games scores for each set
        for (int i = 0; i < sets.length; i++){
            sets[i].setGameScores(scores[i]);
            // Increment final scores accordingly
            if ("home".equals(sets[i].calculateWinner(scores[i]))) {
                        this.homeFinalScore ++;                                
            }
            if ("away".equals(sets[i].calculateWinner(scores[i]))) {
                        this.awayFinalScore ++;                                
            }            
        }
        played = true;  
    }
    
    public boolean isPlayed () {
        return played;
    }
    
    public int getHomeFinalScore () {
        return this.homeFinalScore;
    }
    
    public int getAwayFinalScore () {
        return this.awayFinalScore;
    }
    
    public String getFinalScore () {
        String score = this.homeFinalScore + ":" + this.awayFinalScore;
        return score;
    }
    
    public String getGameScore (int setNumber, int gameNumber) {
        return sets[setNumber].getGameScore(gameNumber);
    }
    
    public int getGameHomeScore (int setNumber, int gameNumber) {
        return sets[setNumber].getGameHomeScore(gameNumber);
    }
    
    public int getGameAwayScore (int setNumber, int gameNumber) {
        return sets[setNumber].getGameAwayScore(gameNumber);
    }
    
    public Player findHomePlayer (String name) {
        Player player = homeTeam.findPlayer(name);
        return player;
    }
    
    public Player findAwayPlayer (String name) {
        Player player = awayTeam.findPlayer(name);
        return player;
    }
    
    @Override  
    public String toString() {
        String match = "Match: " + homeTeam.getTeamName() + " vs "
                + awayTeam.getTeamName();
        if (played == false) {
            match += "\n Not played";
            return match;      
        }
        else {
            for (Set set : sets) {
                match += "\n" + set;
            }
            match += "\nFinal Scores: " + homeFinalScore + ":" + awayFinalScore;
            return match;
        }
    }
}