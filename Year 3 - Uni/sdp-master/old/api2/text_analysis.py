########### Python 3.2 #############
import http.client, urllib.request, urllib.parse, urllib.error, base64

headers = {
    # Request headers
    'Content-Type': 'application/json',
    'Ocp-Apim-Subscription-Key': '79634650c39241a8af5030c89c325bfd',
}

params = urllib.parse.urlencode({
    # Request parameters
    'showStats': '{boolean}',
    'model-version': '{string}',
})

try:
    conn = http.client.HTTPSConnection('uksouth.api.cognitive.microsoft.com')
    conn.request("POST", "/text/analytics/v3.0-preview.1/languages?%s" % params, "{body}", headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

####################################
