
<?php
/* header("Content-Type: application/rss+xml; charset=ISO-8859-1");*/
require("connect.php");    
if(isset($_GET['name'])) {
    $name = $_GET['name'];
}

$results = $db->query("SELECT * FROM POI WHERE name = '$name'");
$poi = $results->fetch_object();
print <<<EOT

<!DOCTYPE html>
<html>
    <body>
    <h3>POI</h3>
      <table border="1" cellpadding="4" cellspacing="0" bordercolor="#cccccc" width="400">
         <tr>
            <td> Place of interest </td>
            <td> {$poi->name} </td>
         </tr>
         <tr>
            <td> Address </td>
            <td> {$poi->address} </td>
        </tr>
        <tr>
            <td> Description </td>
            <td> {$poi->description} </td>
        </tr>
        <tr>
            <td> URI </td>
            <td> {$poi->uri} </td>
        </tr>
        <tr>
            <td> Construction </td>
            <td> {$poi->construction} </td>
        </tr>
        <tr>
            <td> Type </td>
            <td> {$poi->type} </td>
        </tr>
        <tr>
            <td> Contact </td>
            <td> {$poi->contact} </td>
        </tr>
        <tr>
            <td> Altitude </td>
            <td> {$poi->altitude} </td>
        </tr>
        <tr>
            <td> City </td>
            <td> {$poi->city_name} </td>
        </tr>
        <tr>
            <td> Latitude </td>
            <td> {$poi->lat} </td>
        </tr>
        <tr>
            <td> Longitude </td>
            <td> {$poi->long} </td>
        </tr>
        <tr>
            <td> Image </td>
            <td><image src="http://www.cems.uwe.ac.uk/~ad2-biggins/Assignment/images/{$poi->image}" style="width:250px;height:250px"></td>
        </tr>
</body>
</html>

EOT;

?>