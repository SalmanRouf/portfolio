import csv 
import sys #allows me to exit the program without having to ask the user twice

class Items: 
    def __init__(self, itemNumber, itemDescription, itemValue, itemShape, itemWeight):
        self.itemNumber = int (itemNumber)
        self.itemDescription = itemDescription
        self.itemValue = int(itemValue)
        self.itemShape = itemShape
        self.itemWeight = int(itemWeight)
       

class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    #add items function
    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2], row[3], row[4])
            self.items.append(item)

    #appending the items i'm adding into one of the warehouses
    def addSomething(self, item):
        self.items.append(item)
    
    #works out the total value of one warehouse with the items we might be adding.
    def totalValue(self):
        totalValue = 0
        for i in self.items:
                totalValue = totalValue + i.itemValue
        return totalValue
        
    #remove function
    def removeItem(self, item):
        self.items.remove(item)

def readingCSV(file, name):
    with open(file, encoding='windows-1252', errors='ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse


#Reading CSV file from each warehouse
A = readingCSV(r'Warehouse A.csv', 'A')
B = readingCSV(r'Warehouse B.csv', 'B')
C = readingCSV(r'Warehouse C.csv', 'C')
D = readingCSV(r'Warehouse D.csv', 'D')

#Declaring each warehouse max weight for a shape
aRectangle = 1016
aSphere = 0
aPyramid = 2032
aSquare = 2032
bRectangle = 500
bSphere = 2032
bPyramid = 250
bSquare = 0
cRectangle = 0
cSphere = 250
cPyramid = 500
cSquare = 0
dRectangle = 500
dSphere = 750
dPyramid = 3048
dSquare = 750

#Creating empty lists
rectangleA = []
sphereA = []
pyramidA = []
squareA = []

#Setting the warehouse limit of 2 billion
capacity = 2000000000

#Working out the remaining space for each warehouse
checkA = (capacity - Warehouse.totalValue(A))
checkB = (capacity - Warehouse.totalValue(B))
checkC = (capacity - Warehouse.totalValue(C))
checkD = (capacity - Warehouse.totalValue(D))

#Setting the day countt to 0
day = 0

class move():
    #Checking how many shapes are within warehouse D to make sure the new item can fit
    def warehouseD(self, item, shape):
            #Getting the shapes from warehouse D and storing them within a list
            for i in D.items:
                getD = (i.itemShape, i.itemWeight)
                if getD[0] == ('Rectangle'):
                    rectangleA.append(i.itemShape)
                elif getD[0] == ('Sphere'):
                    sphereA.append(i.itemShape)
                elif getD[0] == ('Pyramid'):
                    pyramidA.append(i.itemShape)
                elif getD[0] == ('Square'):
                    squareA.append(i.itemShape)

            #Countting the amount of shapes within warehouse D for each shape      
            lengthRectangle = len(rectangleA)
            lengthSphere = len(sphereA)
            lengthPyramid = len(pyramidA)
            lengthSquare = len(squareA)
            
            #Checking the current length with the max length of the shape to see if there is any space for the new item
            if shape == ('Rectangle'):
                if lengthRectangle < 10:
                    D.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse D')
                else:
                    print('Theres not enough space to store:', item.itemNumber)
            elif shape == ('Sphere'):
                if lengthSphere < 2:
                    D.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse D')
                else:
                    print('Theres not enough space to store:', item.itemNumber)
            elif shape == ('Pyramid'):
                if lengthPyramid < 2:
                    D.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse D')
                else:
                    print ('Theres not enough space to store:', item.itemNumber)
            elif shape == ('Square'):
                if lengthSquare < 10:
                    D.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse D')
                else:
                    print('Theres not enough space to store:', item.itemNumber)
            else:
                print('Shape does not match the shapes within warehouse D')
            
            #Clearing lists so that the lists count from 0 for every loop
            rectangleA.clear()
            sphereA.clear()
            pyramidA.clear()
            squareA.clear()
    
    #Checking how many shapes are within warehouse C to make sure the new item can fit
    def warehouseC(self, item, shape):
            #Getting the shapes from warehouse C and storing them within a list
            for i in C.items:
                getC = (i.itemShape, i.itemWeight)
                if getC[0] == ('Rectangle'):
                    rectangleA.append(i.itemShape)
                elif getC[0] == ('Sphere'):
                    sphereA.append(i.itemShape)
                elif getC[0] == ('Pyramid'):
                    pyramidA.append(i.itemShape)
                elif getC[0] == ('Square'):
                    squareA.append(i.itemShape)
            
            #Countting the amount of shapes within warehouse C for each shape  
            lengthRectangle = len(rectangleA)
            lengthSphere = len(sphereA)
            lengthPyramid = len(pyramidA)
            lengthSquare = len(squareA)  

            #Checking the current length with the max length of the shape to see if there is any space for the new item   
            if shape == ('Rectangle'):
                if lengthRectangle < 0:
                    C.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse C')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseD(item, shape) 
            elif shape == ('Sphere'):
                if lengthSphere < 15:
                    C.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse C')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseD(item, shape) 
            elif shape == ('Pyramid'):
                if lengthPyramid < 5:
                    C.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse C')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseD(item, shape) 
            elif shape == ('Square'):
                if lengthSquare < 0:
                    C.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse C')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseD(item, shape)    
            else:
                print('Shape does not match the shapes within warehouse C')
            
            #Clearing lists so that the lists count from 0 for every loop
            rectangleA.clear()
            sphereA.clear()
            pyramidA.clear()
            squareA.clear()

    #Checking how many shapes are within warehouse B to make sure the new item can fit  
    def warehouseB(self, item, shape):
            #Getting the shapes from warehouse B and storing them within a list
            for i in B.items:
                getB = (i.itemShape, i.itemWeight)
                if getB[0] == ('Rectangle'):
                    rectangleA.append(i.itemShape)
                elif getB[0] == ('Sphere'):
                    sphereA.append(i.itemShape)
                elif getB[0] == ('Pyramid'):
                    pyramidA.append(i.itemShape)
                elif getB[0] == ('Square'):
                    squareA.append(i.itemShape)
            
            #Countting the amount of shapes within warehouse B for each shape  
            lengthRectangle = len(rectangleA)
            lengthSphere = len(sphereA)
            lengthPyramid = len(pyramidA)
            lengthSquare = len(squareA)
            
            #Checking the current length with the max length of the shape to see if there is any space for the new item
            if shape == ('Rectangle'):
                if lengthRectangle < 10:
                    B.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse B')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseC(item, shape)  
            elif shape == ('Sphere'):
                if lengthSphere < 5:
                    B.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse B')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseC(item, shape) 
            elif shape == ('Pyramid'):
                if lengthPyramid < 10:
                    B.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse B')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseC(item, shape) 
            elif shape == ('Square'):
                if lengthSquare < 0:
                    B.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse B')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseC(item, shape)    
            else:
                print('Shape does not match the shapes within warehouse B')

            #Clearing lists so that the lists count from 0 for every loop        
            rectangleA.clear()
            sphereA.clear()
            pyramidA.clear()
            squareA.clear()

    #Checking how many shapes are within warehouse A to make sure the new item can fit         
    def warehouseA(self, item, shape):
            #Getting the shapes from warehouse A and storing them within a list
            for i in A.items:
                getA = (i.itemShape, i.itemWeight)
                if getA[0] == ('Rectangle'):
                    rectangleA.append(i.itemShape)
                elif getA[0] == ('Sphere'):
                    sphereA.append(i.itemShape)
                elif getA[0] == ('Pyramid'):
                    pyramidA.append(i.itemShape)
                elif getA[0] == ('Square'):
                    squareA.append(i.itemShape)

            #Countting the amount of shapes within warehouse A for each shape              
            lengthRectangle = len(rectangleA)
            lengthSphere = len(sphereA)
            lengthPyramid = len(pyramidA)
            lengthSquare = len(squareA)
            
            #Checking the current length with the max length of the shape to see if there is any space for the new item
            if shape == ('Rectangle'):
                if lengthRectangle < 5:
                    A.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse A')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseB(item, shape)
            elif shape == ('Sphere'):
                if lengthSphere < 0:
                    print('Sphere cannot be added to warehouse A')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseB(item, shape)
            elif shape == ('Pyramid'):
                if lengthPyramid < 10:
                    A.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse A')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseB(item, shape)
            elif shape == ('Square'):
                if lengthSquare < 5:
                    A.addSomething(item)
                    print('Item:',item.itemNumber,'added to Warehouse A')
                else:
                    rectangleA.clear()
                    sphereA.clear()
                    pyramidA.clear()
                    squareA.clear()
                    first.warehouseB(item, shape)    
            else:
                print('Shape does not match the shapes within warehouse A')

            #Clearing lists so that the lists count from 0 for every loop     
            rectangleA.clear()
            sphereA.clear()
            pyramidA.clear()
            squareA.clear()
            
    
    #Function that checks the weight max for each item and compares the new item to see if it can fit within warehouse D
    def checkingShapeD(self, item, value, shape, weight):
        #Checks if the new value can fit within warehouse D
        if value < checkD:
            for i in item.itemShape:
                #Checks if the new item shape is below the warehouse limit for that shape
                if shape == ('Rectangle'):
                    if weight < dRectangle:
                        first.warehouseD(item, shape)
                        break
                    else:
                        print('The item weight is too big to fit within any warehouse')
                        break
                elif shape == ('Sphere'):
                    if weight < dSphere:
                        first.warehouseD(item, shape)
                        break
                    else:
                        print('The item weight is too big to fit within any warehouse')
                        break
                elif shape ==('Pyramid'):
                    if weight < dPyramid:
                        first.warehouseD(item, shape)
                        break
                    else:
                        print('The item weight is too big to fit within any warehouse')
                        break
                elif shape == ('Square'):
                    if weight < dSquare:
                        first.warehouseD(item, shape)
                        break
                    else:
                        print('The item weight is too big to fit within any warehouse')
                        break
                else:
                    print('Shape of the item is not located within warehouse D')
            else:
                print('The item weight is too big to fit within any warehouse')
              
    #Function that checks the weight max for each item and compares the new item to see if it can fit within warehouse C       
    def checkingShapeC(self, item, value, shape, weight):
        #Checks if the new value can fit within warehouse C
        if value < checkC:
            for i in item.itemShape:
                #Checks if the new item shape is below the warehouse limit for that shape
                if shape == ('Rectangle'):
                    if weight < cRectangle:
                        first.warehouseC(item, shape)
                        break
                    else:
                        first.checkingShapeD(item, value, shape, weight)
                        break
                elif shape == ('Sphere'):
                    if weight < cSphere:
                        first.warehouseC(item, shape)
                        break
                    else:
                        first.checkingShapeD(item, value, shape, weight)
                        break
                elif shape ==('Pyramid'):
                    if weight < cPyramid:
                        first.warehouseC(item, shape)
                        break
                    else:
                        first.checkingShapeD(item, value, shape, weight)
                        break
                elif shape == ('Square'):
                    if weight < cSquare:
                        first.warehouseC(item, shape)
                        break
                    else:
                        first.checkingShapeD(item, value, shape, weight)
                        break
                else:
                    print('Shape of the item is not located within warehouse C')
        else:
            first.checkingShapeD(item, value, shape, weight)

    #Function that checks the weight max for each item and compares the new item to see if it can fit within warehouse B                
    def checkingShapeB(self, item, value, shape, weight):
        #Checks if the new value can fit within warehouse B
        if value < checkB:
            for i in item.itemShape:
                #Checks if the new item shape is below the warehouse limit for that shape
                if shape == ('Rectangle'):
                    if weight < bRectangle:
                        first.warehouseB(item, shape)
                        break
                    else:
                        first.checkingShapeC(item, value, shape, weight)
                        break
                elif shape == ('Sphere'):
                    if weight < bSphere:
                        first.warehouseB(item, shape)
                        break
                    else:
                        first.checkingShapeC(item, value, shape, weight)
                        break
                elif shape ==('Pyramid'):
                    if weight < bPyramid:
                        first.warehouseB(item, shape)
                        break
                    else:
                        first.checkingShapeC(item, value, shape, weight)
                        break
                elif shape == ('Square'):
                    if weight < bSquare:
                        first.warehouseB(item, shape)
                        break
                    else:
                        first.checkingShapeC(item, value, shape, weight)
                        break
                else:
                    print('Shape of the item is not located within warehouse B')
        else:
            first.checkingShapeC(item, value, shape, weight)

    #Function that checks the weight max for each item and compares the new item to see if it can fit within warehouse A
    def checkingShapeA(self, item, value, shape, weight):
        #Checks if the new value can fit within warehouse A
        if value < checkA:
            for i in item.itemShape:
                #Checks if the new item shape is below the warehouse limit for that shape
                if shape == ('Rectangle'):
                    if weight < aRectangle:
                        first.warehouseA(item, shape)
                        break
                    else:
                        first.checkingShapeB(item, value, shape, weight)
                        break
                elif shape == ('Sphere'):
                    if weight < aSphere:
                        first.warehouseA(item, shape)
                        break
                    else:
                        first.checkingShapeB(item, value, shape, weight)
                        break
                elif shape ==('Pyramid'):
                    if weight < aPyramid:
                        first.warehouseA(item, shape)
                        break
                    else:
                        first.checkingShapeB(item, value, shape, weight)
                        break
                elif shape == ('Square'):
                    if weight < aSquare:
                        first.warehouseA(item, shape)
                        break
                    else:
                        first.checkingShapeB(item, value, shape, weight)
                        break
                else:
                    print('Shape of the item is not located within warehouse A')
        else:
            first.checkingShapeB(item, value, shape, weight)

#Checking the item weight and item value to see if it can fit within the warehouse that it is moving too
def vanTotalA(item, start, end):
    global day
    vanMax=2032 #Setting the max weight for the van of 2 billion
    vanTotal = (item.itemWeight)
    value = (item.itemValue)
    shape = (item.itemShape)
    weight = (item.itemWeight)
    if vanMax > vanTotal: #Comparing the max weight that the van can carry with the weight of the new item
        if start in 'A':
                if end == 'B':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeB(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    A.removeItem(item)        
        if start in 'A':
                if end == 'C':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeC(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    A.removeItem(item)      
        if start == 'A':
                if end == 'D':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeD(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    A.removeItem(item)
                    
                       
#Checking the item weight and item value to see if it can fit within the warehouse that it is moving too
def vanTotalB(item, start, end):
    global day
    vanMax=2032 #Setting the max weight for the van of 2 billion
    vanTotal = (item.itemWeight)
    value = (item.itemValue)
    shape = (item.itemShape)
    weight = (item.itemWeight)
    if vanMax > vanTotal: #Comparing the max weight that the van can carry with the weight of the new item
        if start in 'B':
                if end == 'A':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeA(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    B.removeItem(item)
        if start in 'B':
                if end == 'C':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeC(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    B.removeItem(item)      
        if start == 'B':
                if end == 'D':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeD(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    D.addSomething(item)
                    B.removeItem(item)
                        
#Checking the item weight and item value to see if it can fit within the warehouse that it is moving too
def vanTotalC(item, start, end):
    global day
    vanMax=2032 #Setting the max weight for the van of 2 billion
    vanTotal = (item.itemWeight)
    value = (item.itemValue)
    shape = (item.itemShape)
    weight = (item.itemWeight)
    if vanMax > vanTotal: #Comparing the max weight that the van can carry with the weight of the new item
        if start in 'C':
                if end == 'A':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From: B','\n','To:', end)
                    first.checkingShapeA(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    C.removeItem(item)
        if start in 'C':
                if end == 'B':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeB(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    C.removeItem(item)         
        if start == 'C':
                if end == 'D':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeD(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    C.removeItem(item)
                       
#Checking the item weight and item value to see if it can fit within the warehouse that it is moving too                      
def vanTotalD(item, start, end):
    global day
    vanMax=2032 #Setting the max weight for the van of 2 billion
    vanTotal = (item.itemWeight)
    value = (item.itemValue)
    shape = (item.itemShape)
    weight = (item.itemWeight)
    if vanMax > vanTotal: #Comparing the max weight that the van can carry with the weight of the new item
        if start in 'D':
                if end == 'A':
                    day += 1 
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeA(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    A.removeItem(item)
        if start in 'D':
                if end == 'B':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeB(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    D.removeItem(item)                               
        if start == 'D':
                if end == 'C':
                    day += 1
                    print('Day:', day)
                    print('Item:', item.itemNumber, '\n','From:', start,'\n','To:', end)
                    first.checkingShapeC(item, value, shape, weight) #Checks the item value and item weight to see if the item can fit
                    D.removeItem(item)

#Checks the items starting point and end point     
class van():
    #If the item starts from A it will be sent to vanA, it will look for the end point 
    def vanA(item, number, start, end):
        if end == 'A':
                A.addSomething(item)
                A.removeItem(item)
        elif end == 'B':
                vanTotalA(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'C':
                vanTotalA(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'D':
                vanTotalA(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too

    #If the item starts from B it will be sent to vanB, it will look for the end point       
    def vanB(item, number, start, end):
        if end == 'A':
                vanTotalB(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'B':
                B.addSomething(item)
                B.removeItem(item)
        elif end == 'C':
                vanTotalB(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'D':
                vanTotalB(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too

    #If the item starts from C it will be sent to vanC, it will look for the end point 
    def vanC(item, number, start, end):
        if end == 'A':
                vanTotalC(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'B':
                vanTotalC(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
        elif end == 'C':
                C.addSomething(item)
                C.removeItem(item)
        elif end == 'D':
                vanTotalC(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too

    #If the item starts from D it will be sent to vanD, it will look for the end point 
    def vanD(item, number, start, end):
                if end == 'A':
                    vanTotalD(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
                elif end == 'B':
                    vanTotalD(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
                elif end == 'C':
                    vanTotalD(item, start, end) #Goes to check the item weight and item value to see if it can fit within the warehouse that it is moving too
                elif end == 'D':
                    D.addSomething(item)
                    D.removeItem(item)

#Class for reading in task 2 and checking the data 
class moving(object):
    def readCSV():
        #Readings task 2 csv file
        with open(r'TASK 2.csv') as csv_file:
            reader = csv.reader(csv_file, delimiter=',')
            next(reader) #Skips the first row within the csv file
            for row in reader:
                for item in A.items:
                        if int(row[0]) == item.itemNumber: #checks if the item number is within warehouse A 
                            van.vanA(item, row[0], row[1], row[2]) #sends the item, item number, from which warehouse and to which warehouse to vanA function                  
                for item in B.items:
                        if int(row[0]) == item.itemNumber: #checks if the item number is within warehouse B 
                            van.vanB(item, row[0], row[1], row[2]) #sends the item, item number, from which warehouse and to which warehouse to vanB function   
                for item in C.items:
                        if int(row[0]) == item.itemNumber: #checks if the item number is within warehouse C
                                van.vanC(item, row[0], row[1], row[2]) #sends the item, item number, from which warehouse and to which warehouse to vanC function   
                for item in D.items:
                            if int(row[0]) == item.itemNumber: #checks if the item number is within warehouse D 
                                van.vanD(item, row[0], row[1], row[2]) #sends the item, item number, from which warehouse and to which warehouse to vanD function   
          
#Creating a variable for the move class so I can callback to functions within the class    
first = move()

#Warehouse Menu
while True: #Keeps user in a loop until they pick an option of 1, 2, 3, 4, 5 or 6
    print("Warehouse Menu\n1.View Warehouse A\n2.View Warehouse B\n3.View Warehouse C\n4.View Warehouse D\n5.Task 2b\n6.Exit")
    selection = input("Enter Number: ")
    itemNo = 'Item No.'
    description = 'Description'
    value = 'Value in £'
    shape = 'Shape'
    weight = 'Weight (KG)'
    if selection == '1':
        print(f'{itemNo:10}', f'{description:50}', f'{value:15}', f'{shape:10}', f'{weight:10}')
        for item in A.items: #Prints all the items within warehouse A
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
        print('Warehouse A Total Value in £:', Warehouse.totalValue(A), '\n') #Prints the warehouse total for warehouse A
    elif selection == '2':
        print(f'{itemNo:10}', f'{description:50}', f'{value:15}', f'{shape:10}', f'{weight:10}')
        for item in B.items: #Prints all the items within warehouse B
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
        print('Warehouse B Total Value in £:', Warehouse.totalValue(B), '\n') #Prints the warehouse total for warehouse B
    elif selection == '3':
        print(f'{itemNo:10}', f'{description:50}', f'{value:15}', f'{shape:10}', f'{weight:10}')
        for item in C.items: #Prints all the items within warehouse C
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
        print('Warehouse C Total Value in £:', Warehouse.totalValue(C), '\n') #Prints the warehouse total for warehouse C
    elif selection == '4':
        print(f'{itemNo:10}', f'{description:50}', f'{value:15}', f'{shape:10}', f'{weight:10}')
        for item in D.items: #Prints all the items within warehouse D
            print(('{:<10}'.format(item.itemNumber)), ('{:<50}'.format(item.itemDescription)), ('{:<15}'.format(item.itemValue)), ('{:<10}'.format(item.itemShape)), ('{:<10}'.format(item.itemWeight)))
        print('Warehouse D Total Value in £:', Warehouse.totalValue(D), '\n') #Prints the warehouse total for warehouse D  
    elif selection == '5':
        moving.readCSV() #Calls the readCSV function from class moving
    elif selection == '6':
        print("Thank You\nGoodbye")
        sys.exit() #directing exits the program
    else:
        print('Please select a number') #Error message appears if the user doesn't select 1, 2, 3, 4, 5 or 6
