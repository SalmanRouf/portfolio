/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;

/**
 *
 * @author s2-rouf
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TableTennisManager {

        private ArrayList<Team> teams;
        private ArrayList<Match> fixtures;
        private ArrayList<String> teamNames;

	public TableTennisManager() {
            teams = new ArrayList<Team>();
            fixtures = new ArrayList<Match>();
	}

	public void addTeam(String name) {
            Team team = new Team(name);
            teams.add(team);
	}

	public void addPlayer(String name, String teamName) {
            Player player = new Player(name);
            for (int i = 0; i < teams.size(); i++) {
                if (teams.get(i).getName().equals(teamName)) {
                    teams.get(i).addPlayer(player);
                }  
            }
	}

	public void generateFixtures() {
//            fixtures.clear();
            for (int i = 0; i<teams.size(); i++) {
                Team homeTeam = teams.get(i);
                teams.remove(i);
                for (int j = 0; j<teams.size(); j++) {
                    Match match = new Match(homeTeam, teams.get(j));
                    fixtures.add(match);
                }
                teams.add(i, homeTeam);            
            }
	}
        
        public void addMatch (Match match) {
            fixtures.add(match);
        }
        
        
        public ArrayList<Match> getFixtures () {
            return fixtures;
        }
        
        public String getFixturesMatrix () {
            String results = "";
            int s = teams.size()+1;
            String [][] matrix = new String [s][s];
            matrix[0][0] = "....";
            for (int i = 1; i < s; i++) {
                matrix[i][0] = teams.get(i-1).getName();
                matrix[0][i] = teams.get(i-1).getName();
                
            }
            for (int i = 1; i < s; i++) {
                for (int j = 1; j < s; j++) {
                    if (i == j) {
                        matrix[i][j] = "---";
                    }
                    for (int k = 0; k < fixtures.size(); k++) {
                        if (fixtures.get(k).getAwayTeam().equals(matrix[0][j])
                                && fixtures.get(k).getHomeTeam().equals(matrix[i][0])) {
                            if (fixtures.get(k).isPlayed() == false) {
                                matrix[i][j] = "np";
                            }
                            else {
                                matrix[i][j] = fixtures.get(k).getFinalScore();
                            }
                        }
                    }
                }
            }          
            for (int i = 0; i < s; i++) {
                for (int j = 0; j < s; j++) {
                    results += padRight(matrix [i][j], 10);                   
                }
                results += "\n";
            }      
            return results;
        }
        

        public ArrayList<Team> getTeams() {
            return teams;
        }
        
        public ArrayList<String> getTeamNames() {
            teamNames = new ArrayList<>();
            for (int i = 0; i < teams.size(); i++) {
                teamNames.add(teams.get(i).getName());
            }
            return teamNames;
            
        }
        
        public ArrayList<String> getPlayerNames(String team) {
            for (int i = 0; i < teams.size(); i++) {
                if (team.equals(teams.get(i).getName())){
                   return teams.get(i).getPlayerNames();
                }
            }
            return null;
        }
        
        
        public String calculateStatistics() {
            String statistics = "";
            for (int i = 0; i < teams.size(); i++) {
                teams.get(i).matchesPlayed = 0;
                teams.get(i).matchesWon = 0;
                teams.get(i).setsWon = 0;
                for (int j = 0; j < fixtures.size(); j++) {
                    if (fixtures.get(j).isPlayed() == true) { 
                        if (teams.get(i).getName().equals(fixtures.get(j).getHomeTeam())) {
                            teams.get(i).matchesPlayed ++;
                            teams.get(i).setsWon += fixtures.get(j).getHomeFinalScore();
                            if (fixtures.get(j).getHomeFinalScore() > 
                                    fixtures.get(j).getAwayFinalScore()) {
                                teams.get(i).matchesWon ++;
                            }
                        }    
                        if (teams.get(i).getName().equals(fixtures.get(j).getAwayTeam())) {
                            teams.get(i).matchesPlayed ++;
                            teams.get(i).setsWon += fixtures.get(j).getAwayFinalScore();
                            if (fixtures.get(j).getAwayFinalScore() > 
                                    fixtures.get(j).getHomeFinalScore()) {
                                teams.get(i).matchesWon ++;
                            }    
                        }
                    }   
                }
            statistics += teams.get(i).getName() + ": Matches Played=" +
                    teams.get(i).matchesPlayed + " Matches Won=" + 
                    teams.get(i).matchesWon + " Sets Won=" + 
                    teams.get(i).setsWon + "\n";     
            }
        return statistics;    
        }
        
        
        public void generateStatistics() {
            Collections.sort(teams, new Comparator<Team>() {
                @Override public int compare(Team p1, Team p2) {
            return p2.setsWon - p1.setsWon;
                }
            });
        }
        
        
        public static String padRight(String s, int n) {
            return String.format("%-" + n + "s", s);  
        }

}
