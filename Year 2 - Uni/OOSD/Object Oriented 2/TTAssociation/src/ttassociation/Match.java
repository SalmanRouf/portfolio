/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttassociation;

/**
 *
 * @author s2-rouf
 */
public class Match {
    private Set[] sets;
    
    private Team homeTeam, awayTeam; 
    private Player homePlayer1, homePlayer2, awayPlayer1, awayPlayer2;
    private DoubleSet dset;
    private boolean played;
    private int homeFinalScore, awayFinalScore;
    
    public Match (Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        sets = new Set[4];
        
        for (int i = 0; i < 4; i++) {
           sets[i] = new Set();
        }
        dset = new DoubleSet();
        this.played = false;
    }
    
//    public void setHomeTeam (Team homeTeam) {
//        this.homeTeam = homeTeam;
//    }
//    
//    public void setAwayTeam (Team awayTeam) {
//        this.awayTeam = awayTeam;
//    }
    
    public void setHomePlayers(Player homePlayer1, Player homePlayer2) {
        this.homePlayer1 = homePlayer1;
        this.homePlayer2 = homePlayer2;
        sets[0].setHomePlayer(homePlayer1.getName());
        sets[1].setHomePlayer(homePlayer1.getName());
        sets[2].setHomePlayer(homePlayer2.getName());
        sets[3].setHomePlayer(homePlayer2.getName());
    }
    
    public void setAwayPlayers(Player awayPlayer1, Player awayPlayer2) {
        this.awayPlayer1 = awayPlayer1;
        this.awayPlayer2 = awayPlayer2;
        sets[0].setAwayPlayer(awayPlayer1.getName());
        sets[1].setAwayPlayer(awayPlayer2.getName());
        sets[2].setAwayPlayer(awayPlayer1.getName());
        sets[3].setAwayPlayer(awayPlayer2.getName());
    }
    
    public String getHomeTeam () {
        return homeTeam.getName();
    }
    
    public String getAwayTeam() {
        return awayTeam.getName();
    }
    
    public Player getHomePlayer1() {
        return homePlayer1;
    }
    
    public Player getHomePlayer2() {
        return homePlayer2;
    }
    
    public Player getAwayPlayer1() {
        return awayPlayer1;
    }
    
    public Player getAwayPlayer2() {
        return awayPlayer2;
    }
    
    public void setScores (int [][] scores ) {
        this.homeFinalScore = 0;
        this.awayFinalScore = 0;
        for (int i = 0; i < sets.length; i++){
            sets[i].setGameScores(scores[i]);
            if ("home".equals(sets[i].calculateWinner(scores[i]))) {
                        this.homeFinalScore ++;                                
            }
            if ("away".equals(sets[i].calculateWinner(scores[i]))) {
                        this.awayFinalScore ++;                                
            }            
        }
        dset.setGameScores(scores[4]);
        if ("home".equals(dset.calculateWinner(scores[4]))) {
                this.homeFinalScore ++;                                
            }
        if ("away".equals(dset.calculateWinner(scores[4]))) {
                this.awayFinalScore ++;
        this.played = true;
        }
    }
    
    public Set [] getSinglesSets () {
        return sets;
    }
    
    public DoubleSet getDoublesSet () {
        return dset;
    }
    
    public boolean isPlayed () {
        return this.played;
    }
    
    public int getHomeFinalScore () {
        return this.homeFinalScore;
    }
    
    public int getAwayFinalScore () {
        return this.awayFinalScore;
    }
    
    public String getFinalScore () {
        String score = this.homeFinalScore + ":" + this.awayFinalScore;
        return score;
    }
    
    
    @Override
    
    public String toString() {
        String match = "Match: " + homeTeam.getName() + " vs "
                + awayTeam.getName();
        if (played == false) {
            match += "\n Not played";
            return match;      
        }
        else {
            for (Set set : sets) {
                match += "\n" + set;
            }
            match += "\n" + dset;
            match += "\nFinal Scores: " + homeFinalScore + ":" + awayFinalScore;
            return match;
        }
    }
}
