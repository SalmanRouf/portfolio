<?php 
	//gets photos from json received from API request
	function get_photo($photo_array){
		foreach($photo_array as $single_photo){
			$farm_id = $single_photo->farm;
			$server_id = $single_photo->server;
			$photo_id = $single_photo->id;
			$secret_id = $single_photo->secret;
			// defines size of photo to display
			$size = 's';
			 
			$title = $single_photo->title;
			 // creates link for the photos
			$photo_url = 'https://farm'.$farm_id.'.staticflickr.com/'.$server_id.'/'.$photo_id.'_'.$secret_id.'_'.$size.'.'.'jpg';
			echo("<img title='".$title."' src='".$photo_url."' />");
		}
	}
 ?>