/*
This document manages the whole system. It controls the team, fixture matrix,
statistics and matches. 
 */
package tt_association;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author CP2/02 Team 3
 */
public class TableTennisManager { // start of class TabletennisManager

    private ArrayList<Team> teams;
    private ArrayList<Match> fixtures;
    private ArrayList<String> teamNames;
    private String statistics;

    public TableTennisManager() {
        teams = new ArrayList<>();
        fixtures = new ArrayList<>();
    }

    public void addTeam(String name) {
        Team team = new Team(name);
        teams.add(team);
    }

    public void addPlayer(String name, Team team) {
        Player player = new Player(name);
        team.addPlayer(player);
    }

    // generates fixture 
    public void generateFixtures() {
        //clear existing fixtures
        fixtures.clear();
        for (int i = 0; i < teams.size(); i++) { //
            Team homeTeam = teams.get(i);
            teams.remove(i);
            for (int j = 0; j < teams.size(); j++) {
                Match match = new Match(homeTeam, teams.get(j));
                fixtures.add(match);
            }
            teams.add(i, homeTeam);
        }
    }

    public void addMatch(Match match) {
        fixtures.add(match);
    }

    // creates fixture matrix 
    public String getFixturesMatrix() {
        //clear existing
        String results = "";
        int s = teams.size() + 1;
        //create 2D array size of number of teams +1 squared
        String[][] matrix = new String[s][s];
        //set team names across top and left-hand column
        matrix[0][0] = "....";
        for (int i = 1; i < s; i++) {
            matrix[i][0] = teams.get(i - 1).getTeamName();
            matrix[0][i] = teams.get(i - 1).getTeamName();
        }
        //find corresponding matches and assign scores or not played
        for (int i = 1; i < s; i++) { // Fill out rest of array
            for (int j = 1; j < s; j++) {
                if (i == j) {  // Team can't play itself; no entry
                    matrix[i][j] = "---";
                }
                // Go through fixtres and match them with matrix headings
                for (int k = 0; k < fixtures.size(); k++) {
                    if (fixtures.get(k).getAwayTeamName().equals(matrix[0][j])
                            && fixtures.get(k).getHomeTeamName().equals(matrix[i][0])) {
                        if (fixtures.get(k).isPlayed() == false) {
                            matrix[i][j] = "np";
                        } else {
                            matrix[i][j] = fixtures.get(k).getFinalScore();
                        }
                    }
                }
            }
        }
        //assemble printable matrix
        for (int i = 0; i < s; i++) {
            for (int j = 0; j < s; j++) {
                if (matrix[i][j] == null) {
                    matrix[i][j] = "---";
                }
                String f;
                int l = matrix[i][j].length();
                f = matrix[i][j].substring(0, Math.min(l, 6));
                results += padRight(f, 10) + "\t";
            }
            results += "\n";
        }
        return results;
    }

    public Team findTeam(String name) {
        Team team = null;
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getTeamName().equals(name)) {
                team = teams.get(i);
                break;
            }
        }
        return team;
    }

    public Match findMatch(Team homeTeam, Team awayTeam) {
        Match match = null;
        for (int i = 0; i < fixtures.size(); i++) {
            if (fixtures.get(i).getHomeTeam() == homeTeam
                    && fixtures.get(i).getAwayTeam() == awayTeam) {
                match = fixtures.get(i);
                break;
            }
        }
        return match;
    }

    // puts team names into array list 
    public ArrayList<String> getTeamNames() {
        teamNames = new ArrayList<>();
        for (int i = 0; i < teams.size(); i++) {
            teamNames.add(teams.get(i).getTeamName());
        }
        return teamNames;
    }

    public int getNumberOfTeams() {
        return teams.size();
    }

    public String getStatistics() {
        return statistics;
    }

    public void calculateStatistics() {
        //clear existing stats
        statistics = "";
        for (int i = 0; i < teams.size(); i++) {
            teams.get(i).resetStats();
            //go through matches played and increment respective counts accordingly
            for (int j = 0; j < fixtures.size(); j++) {
                if (fixtures.get(j).isPlayed()) {
                    if (teams.get(i) == (fixtures.get(j).getHomeTeam())) {
                        teams.get(i).incrementMatchesPlayed();
                        teams.get(i).incrementSetsWon(fixtures.get(j).getHomeFinalScore());
                        if (fixtures.get(j).getHomeFinalScore()
                                > fixtures.get(j).getAwayFinalScore()) {
                            teams.get(i).incrementMatchesWon();
                        }
                    }
                    if (teams.get(i) == (fixtures.get(j).getAwayTeam())) {
                        teams.get(i).incrementMatchesPlayed();
                        teams.get(i).incrementSetsWon(fixtures.get(j).getAwayFinalScore());
                        if (fixtures.get(j).getAwayFinalScore()
                                > fixtures.get(j).getHomeFinalScore()) {
                            teams.get(i).incrementMatchesWon();
                        }
                    }
                }
            }
            // generate string from stats    
            statistics += teams.get(i).getTeamName() + ": Matches Played="
                    + teams.get(i).getMatchesPlayed() + " Matches Won="
                    + teams.get(i).getMatchesWon() + " Sets Won="
                    + teams.get(i).getSetsWon() + "\n";
        }
    }

    public void rankBySets() {
        //sort teams by sets won
        Collections.sort(teams, new Comparator<Team>() {
            @Override
            public int compare(Team p1, Team p2) {
                return p2.getSetsWon() - p1.getSetsWon(); //descending order
            }
        });
    }

    //formatting for fixtures matrix
    public static String padRight(String s, int n) {
        return String.format("%-" + n + ".8s", s);
    }

} // end of class TableTennisManager
