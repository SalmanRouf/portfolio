/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pingpong;

/**
 *
 * @author s2-rouf
 */
public class PingPong extends Thread{
    String word;
    public PingPong (String whatToSay) {
	word = whatToSay;
    }

    public void run() {
        for(int i=0; i< 100; i++){
            System.out.print(word+ " ");
        }
    }
}

class TestPingPong {
    public static void main(String args[]) {
    PingPong ping = new PingPong("Ping");
    PingPong pong = new PingPong("Pong");
    ping.start();
    pong.start();
    }
}

