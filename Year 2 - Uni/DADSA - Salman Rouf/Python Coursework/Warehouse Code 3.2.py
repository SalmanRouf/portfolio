import csv

class Items:
    def __init__(self, itemNumber, itemDescription, itemValue):
        self.itemNumber = itemNumber
        self.itemDescription = itemDescription
        self.itemValue = int (itemValue)


class Warehouse:
    def __init__(self, name):
        self.items = []
        self.name = name

    def addItems(self, csv_reader):
        next(csv_reader)
        for row in csv_reader:
            item = Items(row[0], row[1], row[2])
            self.items.append(item)
           # print(item)
    def addSomething(self, item):
        self.items.append(item)
        
    def totalValue(self):
        totalValue = 0
        for i in self.items:
           totalValue = totalValue + i.itemValue
        return totalValue
        

class Function:
    def addFunction(functions):
        #Asking the use to enter the item number
        itemNumber = int(input('Please enter the item number: ')) 

        #Getting the item descrpition
        itemDescription = str(input('Please enter the item description: '))

        #Item value entered & checked the value to make sure it doesn't exceed the limit.
        itemValue = int(input('Please enter the item value:£ '))

        while itemValue > 2000000000:
            print('The item value has been rejected as the value has exceeded the limit of £2 billion')
            itemValue = int(input('Please enter the item value:£ '))

        newItem = Items(itemNumber, itemDescription, itemValue)

        #Asking the user to pick a warehouse to place the item within
        while True:
            four = str(input("Please select a warehouse to place the item within A, B, C or D: "))
            if four =='A' or four =='a':
                A.addSomething(newItem)
                #itemSpace = i
                for item in A.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break    
            elif four == 'B' or four =='b':
                B.addSomething(newItem)
                for item in B.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break    
            elif four =='C' or four =='c':
                C.addSomething(newItem)
                for item in C.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            elif four =='D' or four =='d':
                D.addSomething(newItem)
                for item in D.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            else:
                print("Unknown Variable Selected")
                
        while True:
            option = str(input("Would you like to add to another warehouse? [y/n] "))
            if option == 'y' or option == 'Y':
                 first.addFunction()
            elif option == 'n' or option == 'N':
                    print("Goodbye")
                    exit()
            else:
                    print("Unknown Option Selected. Select y or n")

                     
class Function2(Function):
    def viewSummaryFunction(functions):
    #Asking the user to pick a warehouse to place the item within
        while True:
            choice=input("Select to view warehouse A, B, C or D: " )
            if choice =='A' or choice =='a':
                for item in A.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            elif choice =='B' or choice =='b':
                for item in B.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            elif choice =='C' or choice =='c':
                for item in C.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            elif choice =='D' or choice =='d':
                for item in D.items:
                    print(item.itemNumber,item.itemDescription,item.itemValue)
                break
            else:
                print("Error: Unknown Variable Selected\nMake sure that you are selecting A, B, C or D")
               
def readingCSV(file, name):
    with open(file, encoding = 'windows-1252', errors = 'ignore') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        warehouse = Warehouse(name)
        warehouse.addItems(reader)
        return warehouse

def firstFit(self, capacity):
    capacity = 2000000000
    if A.totalValue + itemValue > capacity:
        print ("The item cannot fit")
    
  
#Reading CSV file from each warehouse
A = readingCSV('DADSA Assignment 2018-19 Warehouse A.csv', 'A')
B = readingCSV('DADSA Assignment 2018-19 Warehouse B.csv', 'B')
C = readingCSV('DADSA Assignment 2018-19 Warehouse C.csv', 'C')
D = readingCSV('DADSA Assignment 2018-19 Warehouse D.csv', 'D')

#Declaring objects
first=Function()
second=Function2()

print (A.totalValue(), B.totalValue(), C.totalValue(), D.totalValue())

while True:
    print("Warehouse Menu\n1.Add Item\n2.View Warehouse\n3.Exit")
    selection=input("Please Select 1, 2, or 3: ") 
    if selection =='1': 
        print ("You Can Now Add The Item")
        first.addFunction()
    elif selection == '2': 
        print ("View Warehouse")
        second.viewSummaryFunction()
    elif selection == '3':
        print ("Goodbye")
        exit()
    else: 
        print ("Unknown Option Selected(Please Select 1, 2 or 3)") 

   


