<?php
    #adding the header path
    require "header.php";
?>
<!DOCTYPE html>
<head>
    <!-- path to css file -->
    <link rel="stylesheet" href="css/placement.css" type="text/css"/>
</head>
<body>
    <div id="table-container">
        <div class="container-placement">
            <div class="square">
                <div class="layer"></div>
                    <div class="content">
                        <a href="joker.php"><h1>Joker (2019)</h1></a>
                        <div class="image">
                            <img src="images/Joker.jpeg">
                        </div>
                        <div class="details">
                            <h2>Director: Todd Phillips</h2>
                            <h3>Writers: Todd Phillips, Scott Silver</h3>
                            <h3>Stars: Joaquin Phoenix, Robert De Niro, Zazie Beetz</h3>
                        </div>
                    </div>
            </div>
            <div class="square">
                <div class="layer"></div>
                    <div class="content">
                        <a href="once-upon-a-time-in-hollywood.php"><h1>Once Upon a Time in Hollywood (2019)</h1></a>
                        <div class="image">
                            <img src="images/Once-Upon-a-Time-in-Hollywood-2.jpeg">
                        </div>
                        <div class="details">
                            <h2>Director: Quentin Tarantino</h2>
                            <h3>Writer: Quentin Tarantino</h3>
                            <h3>Stars: Leonardo DiCaprio, Brad Pitt, Margot Robbie</h3>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</body>
